<?php 


class PatientsInfo_model extends CI_Model{

    
    public function add_patients_info($data) {
        $this->db->insert('patients_info', $data);
        if ($this->db->insert_id()) {
            $data = $this->db->insert_id();
            return $data;
        } else {
            return FALSE;
        }
    }

    public function CheckPatientsAndInfo($PatientId, $PatientInfoId){
        $sql = "SELECT * FROM patients_info WHERE patient_id = '{$PatientId}' AND patient_info_id='{$PatientInfoId}'";
        $query = $this->db->query($sql);
        if($query){
            $rs = $query->result_array();
            return $rs;
        }else{
            return FALSE;
        }
    }

    public function GetCompliantsByPatientId($Id){
        // $sql = "SELECT * FROM patients_info, compliants, patients WHERE patients_info.patient_id = patients.id AND patients_info.patient_id = '{$Id}' AND compliants.id = patients_info.patient_info_id AND compliants.type = 1";
        $sql = "SELECT patients_info.id, patients.name, compliants.compliant_name, patients_info.description FROM patients_info, patients, compliants WHERE patients_info.patient_id = patients.id AND patients_info.patient_id = '{$Id}' AND compliants.id = patients_info.patient_info_id AND compliants.type = 1";
        $query = $this->db->query($sql);
        if($query){
            $rs = $query->result_array();
            return $rs;
        }else{
            return FALSE;
        }
    }

    public function GetExaminationByPatientId($Id){
        // $sql = "SELECT * FROM patients_info, compliants, patients WHERE patients_info.patient_id = patients.id AND patients_info.patient_id = '{$Id}' AND  compliants.id = patients_info.patient_info_id AND compliants.type = 2";
        $sql = "SELECT patients_info.id, patients.name, compliants.compliant_name, patients_info.description FROM patients_info, patients, compliants WHERE patients_info.patient_id = patients.id AND patients_info.patient_id = '{$Id}' AND compliants.id = patients_info.patient_info_id AND compliants.type = 2";
        $query = $this->db->query($sql);
        if($query){
            $rs = $query->result_array();
            return $rs;
        }else{
            return FALSE;
        }
    }

    public function GetInvestigationByPatientId($Id){
        $sql = "SELECT patients_info.id, patients.name, compliants.compliant_name, patients_info.description FROM patients_info, patients, compliants WHERE patients_info.patient_id = patients.id AND patients_info.patient_id = '{$Id}' AND compliants.id = patients_info.patient_info_id AND compliants.type = 3";
        $query = $this->db->query($sql);
        if($query){
            $rs = $query->result_array();
            return $rs;
        }else{
            return FALSE;
        }
    }

    public function GetTreatmentsByPatientId($Id){
        $sql = "SELECT treatments.* FROM treatments, patients WHERE treatments.patient_id = patients.id AND treatments.patient_id = '{$Id}'";
        $query = $this->db->query($sql);
        if($query){
            $rs = $query->result_array();
            return $rs;
        }else{
            return FALSE;
        }
    }

    public function GetDiagnoticsByPatientId($Id){
        $sql = "SELECT patients_info.id, patients.name, compliants.compliant_name, patients_info.description FROM patients_info, patients, compliants WHERE patients_info.patient_id = patients.id AND patients_info.patient_id = '{$Id}' AND compliants.id = patients_info.patient_info_id AND compliants.type = 5";
        $query = $this->db->query($sql);
        if($query){
            $rs = $query->result_array();
            return $rs;
        }else{
            return FALSE;
        }
    }

    public function GetSurgeryByPatientId($Id){
        $sql = "SELECT surgery.id, surgery.surgery_name, surgery.details, surgery.date FROM patients, surgery WHERE surgery.patient_id = patients.id AND surgery.patient_id = '{$Id}'";
        $query = $this->db->query($sql);
        if($query){
            $rs = $query->result_array();
            return $rs;
        }else{
            return FALSE;
        }
    }


    public function add_surgery($data) {
        $this->db->insert('surgery', $data);
        if ($this->db->insert_id()) {
            $data = $this->db->insert_id();
            return $data;
        } else {
            return FALSE;
        }
    }

    public function CheckPatientsSurgery($PatientId, $Surgery){
        $sql = "SELECT * FROM patients, surgery WHERE patients.id = surgery.patient_id AND surgery.patient_id = '{$PatientId}' AND surgery.surgery_name='{$Surgery}'";
        $query = $this->db->query($sql);
        if($query){
            $rs = $query->result_array();
            return $rs;
        }else{
            return FALSE;
        }
    }

    public function update_surgery($SurgeryId, $PatientId, $data){
        $this->db->where('surgery_name', $SurgeryId);
        $this->db->where('patient_id', $PatientId);
        $this->db->update('surgery', $data);
        return TRUE;
    }

    public function GetSpinSurgery(){
        $sql = "SELECT * FROM surgery WHERE surgery_name = 1";
        $query = $this->db->query($sql);
        if($query){
            $rs = $query->num_rows();
            return $rs;
        }else{
            return FALSE;
        }
    }

    public function GetBrainSurgery(){
        $sql = "SELECT * FROM surgery WHERE surgery_name = 2";
        $query = $this->db->query($sql);
        if($query){
            $rs = $query->num_rows();
            return $rs;
        }else{
            return FALSE;
        }
    }


    public function SearchNyComplaintName($Name){
        $sql = "SELECT compliant_name FROM compliants WHERE compliants.type = 2 AND compliant_name LIKE '%$Name%'";
        $query = $this->db->query($sql);
        if($query){
            $rs = $query->result();
            return $rs;
        }else{
            return FALSE;
        }
	}
	

	public function add_treatments($data) {
        $this->db->insert('treatments', $data);
        if ($this->db->insert_id()) {
            $data = $this->db->insert_id();
            return $data;
        } else {
            return FALSE;
        }
	}
	
	public function UpdatePatients($id){
        $sql ="SELECT * FROM patients WHERE id='{$id}'";
        $query =$this->db->query($sql);
        if($query){
            $val = $query->row_array();
            if($val['status'] == 1){
                $updated = 0;
            }else{
                $updated = 1;
            }
            $sql ="UPDATE patients SET status = '{$updated}' WHERE id ='{$id}'";
            $this->db->query($sql);
        }
        return TRUE;
    }
    
}
