<?php 


class CheckupsModel extends CI_Model{

    public function GetCheckups(){
        $sql = "SELECT * FROM checkups";
        $query = $this->db->query($sql);
        if($query){
            $rs = $query->result_array();
            return $rs;
        }else{
            return FALSE;
        }
    }

    public function GetMyProfile(){
        $sql = "SELECT * FROM my_profile";
        $query = $this->db->query($sql);
        if($query){
            $rs = $query->result_array();
            return $rs;
        }else{
            return FALSE;
        }
    }

    public function GetMyProfileById($id){
        $sql = "SELECT * FROM my_profile WHERE id='{$id}'";
        $query = $this->db->query($sql);
        if($query){
            $rs = $query->result_array();
            return $rs;
        }else{
            return FALSE;
        }
    }

    public function GetCheckupsbyId($id){
        $sql = "SELECT * FROM checkups WHERE id='{$id}'";
        $query = $this->db->query($sql);
        if($query){
            $rs = $query->result_array();
            return $rs;
        }else{
            return FALSE;
        }
    }


    public function add_checkups($data) {
        $this->db->insert('checkups', $data);
        if ($this->db->insert_id()) {
            $data = $this->db->insert_id();
            return $data;
        } else {
            return FALSE;
        }
    }


    public function add_patient_checkups($data) {
        $this->db->insert('add_checkups', $data);
        if ($this->db->insert_id()) {
            $data = $this->db->insert_id();
            return $data;
        } else {
            return FALSE;
        }
    }


    public function updatecheckups($id, $data){
        $this->db->where('id', $id);
        $this->db->update('checkups', $data);
        return TRUE;
    }


}