<?php 


class SuperAdminModel extends CI_Model{

    public function GetCompliants(){
        $sql = "SELECT * FROM compliants WHERE type = 1";
        $query = $this->db->query($sql);
        if($query){
            $rs = $query->result_array();
            return $rs;
        }else{
            return FALSE;
        }
    }

    public function GetDiagnotics(){
        $sql = "SELECT * FROM compliants WHERE type = 5";
        $query = $this->db->query($sql);
        if($query){
            $rs = $query->result_array();
            return $rs;
        }else{
            return FALSE;
        }
    }

    public function GetExamination(){
        $sql = "SELECT * FROM compliants WHERE type = 2";
        $query = $this->db->query($sql);
        if($query){
            $rs = $query->result_array();
            return $rs;
        }else{
            return FALSE;
        }
    }

    public function GetInvestigation(){
        $sql = "SELECT * FROM compliants WHERE type = 3";
        $query = $this->db->query($sql);
        if($query){
            $rs = $query->result_array();
            return $rs;
        }else{
            return FALSE;
        }
    }


    public function GetTreatments(){
        $sql = "SELECT * FROM compliants WHERE type = 4";
        $query = $this->db->query($sql);
        if($query){
            $rs = $query->result_array();
            return $rs;
        }else{
            return FALSE;
        }
    }

    public function GetMyProfile(){
        $sql = "SELECT * FROM my_profile";
        $query = $this->db->query($sql);
        if($query){
            $rs = $query->result_array();
            return $rs;
        }else{
            return FALSE;
        }
    }

    public function GetMyProfileById($id){
        $sql = "SELECT * FROM my_profile WHERE id='{$id}'";
        $query = $this->db->query($sql);
        if($query){
            $rs = $query->result_array();
            return $rs;
        }else{
            return FALSE;
        }
    }

    public function GetCompliantsbyId($id){
        $sql = "SELECT * FROM compliants WHERE id='{$id}'";
        $query = $this->db->query($sql);
        if($query){
            $rs = $query->result_array();
            return $rs;
        }else{
            return FALSE;
        }
    }

	public function GetTreatmentsbyId($id){
        $sql = "SELECT * FROM treatments WHERE id='{$id}'";
        $query = $this->db->query($sql);
        if($query){
            $rs = $query->result_array();
            return $rs;
        }else{
            return FALSE;
        }
	}
	
    public function GetPatientsInfobyId($id){
        $sql = "SELECT patients_info.id, compliants.compliant_name, patients_info.description FROM patients_info, compliants WHERE compliants.id = patients_info.patient_info_id AND  patients_info.id='{$id}'";
        $query = $this->db->query($sql);
        if($query){
            $rs = $query->result_array();
            return $rs;
        }else{
            return FALSE;
        }
    }

    public function GetSurgerybyId($id){
        $sql = "SELECT surgery.id, surgery.surgery_name, surgery.details FROM surgery, patients WHERE patients.id = surgery.patient_id AND  surgery.id='{$id}'";
        $query = $this->db->query($sql);
        if($query){
            $rs = $query->result_array();
            return $rs;
        }else{
            return FALSE;
        }
    }


    public function add_compliants($data) {
        $this->db->insert('compliants', $data);
        if ($this->db->insert_id()) {
            $data = $this->db->insert_id();
            return $data;
        } else {
            return FALSE;
        }
    }

    public function updatecompliants($id, $data){
        $this->db->where('id', $id);
        $this->db->update('compliants', $data);
        return TRUE;
    }

    public function updatecompliantsinfo($id, $data){
        $this->db->where('id', $id);
        $this->db->update('patients_info', $data);
        return TRUE;
	}
	

	public function updatetreatmentsinfo($id, $data){
        $this->db->where('id', $id);
        $this->db->update('treatments', $data);
        return TRUE;
    }

    public function updatesurgery($id, $data){
        $this->db->where('id', $id);
        $this->db->update('surgery', $data);
        return TRUE;
    }

    public function updateprofile($id, $data){
        $this->db->where('id', $id);
        $this->db->update('my_profile', $data);
        return TRUE;
    }


    // payments
    public function add_payments($data) {
        $this->db->insert('payments', $data);
        if ($this->db->insert_id()) {
            $data = $this->db->insert_id();
            return $data;
        } else {
            return FALSE;
        }
    }

    public function GetPayments(){
        $sql = "SELECT * FROM payments";
        $query = $this->db->query($sql);
        if($query){
            $rs = $query->result_array();
            return $rs;
        }else{
            return FALSE;
        }
    }

    public function GetPaymentsbyId($id){
        $sql = "SELECT * FROM payments WHERE id = '{$id}'";
        $query = $this->db->query($sql);
        if($query){
            $rs = $query->result_array();
            return $rs;
        }else{
            return FALSE;
        }
    }

    public function updatepayments($id, $data){
        $this->db->where('id', $id);
        $this->db->update('payments', $data);
        return TRUE;
    }

    public function GetPatientsCount(){
        $sql = "SELECT * FROM patients WHERE type=2";
        $query = $this->db->query($sql);
        if($query){
            $rs = $query->num_rows();
            return $rs;
        }else{
            return FALSE;
        }
    }

    public function GetPatientsLast30Days(){
        $sql = "SELECT * FROM patients WHERE DATE(created_at) >= DATE(NOW()) - INTERVAL 30 DAY AND type=2";
        $query = $this->db->query($sql);
        if($query){
            $rs = $query->num_rows();
            return $rs;
        }else{
            return FALSE;
        }
    }

    public function GetPatientsLast7Days(){
        $sql = "SELECT * FROM patients WHERE DATE(created_at) >= DATE(NOW()) - INTERVAL 7 DAY AND type = 2 LIMIT 4";
        $query = $this->db->query($sql);
        if($query){
            $rs = $query->result_array();
            return $rs;
        }else{
            return FALSE;
        }
    }

    public function GetTotalRevenue(){
        $sql = "SELECT SUM(payments.amount) AS Amount FROM appointments, payments WHERE payments.id = appointments.location_id";
        $query = $this->db->query($sql);
        if($query){
            $rs = $query->result_array();
            return $rs;
        }else{
            return FALSE;
        }
    }

    public function GetProfitDetails(){
        $sql = "SELECT SUM(payments.amount) AS Amount, count(patients.name) AS patientscount, payments.location  AS LocationName FROM appointments, payments, patients WHERE appointments.patient_id = patients.id AND payments.id = appointments.location_id AND patients.type=2";
        $query = $this->db->query($sql);
        if($query){
            $rs = $query->result_array();
            return $rs;
        }else{
            return FALSE;
        }
    }

    public function GetProfitDetailsBylocation($LocationId){
        $sql = "SELECT SUM(payments.amount) AS Amount, count(patients.name) AS patientscount, payments.location  AS LocationName FROM appointments, payments, patients WHERE appointments.patient_id = patients.id AND payments.id = appointments.location_id AND payments.id = '{$LocationId}' AND patients.type=2";
        $query = $this->db->query($sql);
        if($query){
            $rs = $query->result_array();
            return $rs;
        }else{
            return FALSE;
        }
    }



    // Details

    public function GetTips(){
        $sql = "SELECT * FROM patient_tips";
        $query = $this->db->query($sql);
        if($query){
            $rs = $query->result_array();
            return $rs;
        }else{
            return FALSE;
        }
    }


    public function GetTipsById($id){
        $sql = "SELECT * FROM patient_tips WHERE id = '{$id}'";
        $query = $this->db->query($sql);
        if($query){
            $rs = $query->result_array();
            return $rs;
        }else{
            return FALSE;
        }
    }
    
    public function updatetips($id, $data){
        $this->db->where('id', $id);
        $this->db->update('patient_tips', $data);
        return TRUE;
    }


    // payments
    public function add_tips($data) {
        $this->db->insert('patient_tips', $data);
        if ($this->db->insert_id()) {
            $data = $this->db->insert_id();
            return $data;
        } else {
            return FALSE;
        }
    }



    // Delete Functions
    public function Delete_Complaints($id) {
        $this->db->where('id', $id);
        $this->db->delete('compliants');
        return TRUE;
    }

    public function Delete_ComplaintsInfo($id) {
        $this->db->where('id', $id);
        $this->db->delete('patients_info');
        return TRUE;
	}
	
	public function Delete_TreatmentsInfo($id) {
        $this->db->where('id', $id);
        $this->db->delete('treatments');
        return TRUE;
    }

    public function Delete_Surgery($id) {
        $this->db->where('id', $id);
        $this->db->delete('surgery');
        return TRUE;
    }

    public function Delete_Payments($id) {
        $this->db->where('id', $id);
        $this->db->delete('payments');
        return TRUE;
    }

    public function Delete_Tips($id) {
        $this->db->where('id', $id);
        $this->db->delete('patient_tips');
        return TRUE;
    }



    // Multiple images
    public function add_investigation($filename,$patient_id)
  {
    // $this->db->insert('patient_images', $data); 
    // $insert_id = $this->db->insert_id();
    if($filename!='' ){
      $filename1 = explode(',',$filename);
      foreach($filename1 as $file){
        $file_data = array(
          'images' => $file,
          'patient_id' => $patient_id,
          'type' => 1
        );
        $this->db->insert('patient_images', $file_data,$patient_id);
      }
    }
  }

  public function add_diagnotics($filename,$patient_id)
  {
    // $this->db->insert('patient_images', $data); 
    // $insert_id = $this->db->insert_id();
    if($filename!='' ){
      $filename1 = explode(',',$filename);
      foreach($filename1 as $file){
        $file_data = array(
          'images' => $file,
          'patient_id' => $patient_id,
          'type' => 2
        );
        $this->db->insert('patient_images', $file_data,$patient_id);
      }
    }
  }

  public function add_treatments($filename,$patient_id)
  {
    // $this->db->insert('patient_images', $data); 
    // $insert_id = $this->db->insert_id();
    if($filename!='' ){
      $filename1 = explode(',',$filename);
      foreach($filename1 as $file){
        $file_data = array(
          'images' => $file,
          'patient_id' => $patient_id,
          'type' => 3
        );
        $this->db->insert('patient_images', $file_data,$patient_id);
      }
    }
  }

  public function add_surgery($filename,$patient_id)
  {
    // $this->db->insert('patient_images', $data); 
    // $insert_id = $this->db->insert_id();
    if($filename!='' ){
      $filename1 = explode(',',$filename);
      foreach($filename1 as $file){
        $file_data = array(
          'images' => $file,
          'patient_id' => $patient_id,
          'type' => 4
        );
        $this->db->insert('patient_images', $file_data,$patient_id);
      }
    }
  }

  public function add_surgery_video($data)
  {
    $this->db->insert('patient_images', $data);
    if ($this->db->insert_id()) {
        $data = $this->db->insert_id();
        return $data;
    } else {
        return FALSE;
    }
  }
}
