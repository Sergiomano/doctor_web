<?php 


class AdminModel extends CI_Model{

    public function GetAdmin(){
        $sql = "SELECT * FROM patients WHERE type=1";
        $query = $this->db->query($sql);
        if($query){
            $rs = $query->result_array();
            return $rs;
        }else{
            return FALSE;
        }
    }

    public function GetPatients(){
        $sql = "SELECT * FROM patients WHERE type=2";
        $query = $this->db->query($sql);
        if($query){
            $rs = $query->result_array();
            return $rs;
        }else{
            return FALSE;
        }
    }

    public function GetAdminById($id){
        $sql = "SELECT * FROM patients WHERE id='{$id}'";
        $query = $this->db->query($sql);
        if($query){
            $rs = $query->result_array();
            return $rs;
        }else{
            return FALSE;
        }
    }

    public function GetDataByName($name, $password){
        $sql = "SELECT * FROM patients Where name = '{$name}' AND password = '{$password}'";
        $query = $this->db->query($sql);
        if($query){
            $rs = $query->result_array();
            return $rs;
        }else{
            return FALSE;
        }
    }

    public function add_admin($data) {
        $this->db->insert('patients', $data);
        if ($this->db->insert_id()) {
            $data = $this->db->insert_id();
            return $data;
        } else {
            return FALSE;
        }
    }

    public function updateadmin($id, $data){
        $this->db->where('id', $id);
        $this->db->update('patients', $data);
        return TRUE;
    }

    public function Delete_Admins($id) {
        $this->db->where('id', $id);
        $this->db->delete('patients');
        return TRUE;
    }

}