<?php 


class PatientsModel extends CI_Model{

    public function GetPatients(){
        $sql = "SELECT * FROM patients";
        $query = $this->db->query($sql);
        if($query){
            $rs = $query->result_array();
            return $rs;
        }else{
            return FALSE;
        }
    }

    public function GetPatientsByName($name){
        $sql = "SELECT * FROM patients WHERE name='{$name}'";
        $query = $this->db->query($sql);
        if($query){
            $rs = $query->result_array();
            return $rs;
        }else{
            return FALSE;
        }
    }

    public function GetPatientsById($id){
        $sql = "SELECT * FROM patients WHERE id='{$id}'";
        $query = $this->db->query($sql);
        if($query){
            $rs = $query->result_array();
            return $rs;
        }else{
            return FALSE;
        }
    }

    public function GetCompliantsbyId($id){
        $sql = "SELECT * FROM compliants WHERE id='{$id}'";
        $query = $this->db->query($sql);
        if($query){
            $rs = $query->result_array();
            return $rs;
        }else{
            return FALSE;
        }
    }


    public function add_patients($data) {
        $this->db->insert('patients', $data);
        if ($this->db->insert_id()) {
            $data = $this->db->insert_id();
            return $data;
        } else {
            return FALSE;
        }
    }

    public function add_appointments($data) {
        $this->db->insert('appointments', $data);
        if ($this->db->insert_id()) {
            $data = $this->db->insert_id();
            return $data;
        } else {
            return FALSE;
        }
    }

    public function update_patients($id, $data){
        $this->db->where('id', $id);
        $this->db->update('patients', $data);
        return TRUE;
    }

    public function updatecompliants($id, $data){
        $this->db->where('id', $id);
        $this->db->update('compliants', $data);
        return TRUE;
    }


    public function GetCheckAppointments($id){
        $sql = "SELECT * FROM patients, appointments WHERE patients.id = appointments.patient_id AND patients.id='{$id}'";
        $query = $this->db->query($sql);
        if($query){
            $rs = $query->result_array();
            return $rs;
        }else{
            return FALSE;
        }
    }

    public function GetCheckCheckups($id){
        $sql = "SELECT * FROM patients, add_checkups, checkups WHERE patients.id = add_checkups.patient_id AND checkups.id = add_checkups.checkup_id AND patients.id='{$id}'";
        $query = $this->db->query($sql);
        if($query){
            $rs = $query->result_array();
            return $rs;
        }else{
            return FALSE;
        }
    }

    public function CheckAppointments($id, $Date){
        $sql = "SELECT * FROM appointments WHERE patient_id='{$id}' AND appointment_date = '{$Date}'";
        $query = $this->db->query($sql);
        if($query){
            $rs = $query->result_array();
            return $rs;
        }else{
            return FALSE;
        }
    }

    public function CheckAppointmentsByPatientId($id){
        $sql = "SELECT * FROM appointments, patients, payments WHERE payments.id = appointments.location_id AND appointments.patient_id = patients.id AND appointments.patient_id='{$id}'";
        $query = $this->db->query($sql);
        if($query){
            $rs = $query->result_array();
            return $rs;
        }else{
            return FALSE;
        }
    }
}