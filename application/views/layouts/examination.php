<section class="content">
    <div class="block-header">
        <div class="row">
            <div class="col-lg-7 col-md-6 col-sm-12">
                <h2>All Examination
                <!-- <small class="text-muted">Welcome to Oreo</small> -->
                </h2>
            </div>
            <div class="col-lg-5 col-md-6 col-sm-12">
                <button class="btn btn-primary btn-icon btn-round hidden-sm-down float-right m-l-10" type="button">
                    <i class="zmdi zmdi-plus"></i>
                </button>
                <ul class="breadcrumb float-md-right">
                    <li class="breadcrumb-item"><a href="index-2.html"><i class="zmdi zmdi-home"></i> Dr. Rajesh</a></li>
                    <li class="breadcrumb-item"><a href="javascript:void(0);">Examination</a></li>
                    <li class="breadcrumb-item active">All Examination</li>
                </ul>
            </div>
        </div>
    </div>
    <div class="container-fluid">
        <div class="row clearfix">
            <div class="col-md-12">
                <div class="card patients-list">
                <div class="row">
                    <div class="col-md-6">
                        <div class="header">
                            <h2><strong>Examination</strong> List</h2>
                        </div>
                    </div>
                    <div class="col-md-6 ">
                        <button type="button" data-color="light-blue" class="btn bg-light-blue waves-effect pull-right mar10" data-toggle="modal" data-target="#AddCompliants">Add Examination</button>
                    </div>
                </div>

                    <div class="body">
                            
                        <!-- Tab panes -->
                        <div class="tab-content m-t-10">
                            <div class="tab-pane table-responsive active" id="All">
                                <table class="table m-b-0 table-hover">
                                    <thead>
                                        <tr>                                       
                                            <th>Examination Name</th>
                                            <th>Description</th>
                                            <th>Status</th>
                                            <th>Action</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <?php foreach($GetExamination as $Examination){ ?>
                                        <tr>
                                            <td><?php echo $Examination["compliant_name"] ?></td>
                                            <td><?php echo $Examination["details"] ?></td>
                                            <td><span class="badge badge-success">Active</span></td>
                                            <td><a href="javascrip:void;" onclick="EditExamination(<?php echo $Examination["id"] ?>)"><i class="zmdi zmdi-edit"></i></a>&nbsp; &nbsp;<a href="javascript:void(0);" onclick="DeleteComplaints(<?php echo $Examination["id"] ?>);"><i class="zmdi zmdi-delete"></i></a></td>
                                        </tr>
                                        <?php 
                                        }
                                        ?>
                                    </tbody>
                                </table>                            
                            </div>
                        </div>
                    </div>
                   
                </div>
            </div>
        </div>
    </div>
</section>

<div class="modal fade" id="AddCompliants" tabindex="-1" role="dialog">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="title" id="largeModalLabel">Add Examination</h4>
            </div>
            <div class="modal-body"> 
                <div class="row clearfix">
                    <div class="col-sm-12">
                        <div class="form-group">
                            <input type="text" id="name" class="form-control" placeholder="Examination Name">
                        </div>
                        
                        <div class="form-group">
                            <label>Description</label>
                            <textarea class="form-control" id="description" placeholder="Description"></textarea>
                        </div>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-success btn-round waves-effect AddExamination">SAVE CHANGES</button>
                <button type="button" class="btn btn-danger waves-effect" data-dismiss="modal">CLOSE</button>
            </div>
        </div>
    </div>
</div>

<div class="modal fade" id="EditCompliants" tabindex="-1" role="dialog">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="title" id="largeModalLabel">Edit Examination</h4>
            </div>
            <div class="modal-body"> 
                <div class="row clearfix">
                    <div class="col-sm-12">
                    <div class="form-group">
                        <input type="hidden" id="id" class="form-control" placeholder="Employee Name">       
                        <input type="text" id="Editname" class="form-control" placeholder="Examination Name">
                    </div>
                        
                        <div class="form-group">
                            <label>Description</label>
                            <textarea class="form-control Editdescription" name="Editdescription" id="Editdescription" placeholder="Description"></textarea>
                        </div>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-success btn-round waves-effect" id="UpdateExamination">Update CHANGES</button>
                <button type="button" class="btn btn-danger waves-effect" data-dismiss="modal">CLOSE</button>
            </div>
        </div>
    </div>
</div>

