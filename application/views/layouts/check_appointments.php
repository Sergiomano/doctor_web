<section class="content">
    <div class="block-header">
        <div class="row">
            <div class="col-lg-7 col-md-6 col-sm-12">
                <h2>My Appointments
                <!-- <small class="text-muted">Welcome to Oreo</small> -->
                </h2>
            </div>
            <div class="col-lg-5 col-md-6 col-sm-12">
                <button class="btn btn-primary btn-icon btn-round hidden-sm-down float-right m-l-10" type="button">
                    <i class="zmdi zmdi-plus"></i>
                </button>
                <ul class="breadcrumb float-md-right">
                    <li class="breadcrumb-item"><a href="index-2.html"><i class="zmdi zmdi-home"></i> Dr. Rajesh</a></li>
                    <li class="breadcrumb-item"><a href="javascript:void(0);">Appointments</a></li>
                    <li class="breadcrumb-item active">My Appointments</li>
                </ul>
            </div>
        </div>
    </div>
    <div class="container-fluid">
        <div class="row clearfix">
            <div class="col-md-12">
                <div class="card patients-list">
                <div class="row">
                    <div class="col-md-6">
                        <div class="header">
                            <h2><strong>My Appointments</strong> List</h2>
                        </div>
                    </div>
                    <div class="col-md-6 ">
                        <!-- <a href="#" data-color="light-blue" class="btn bg-light-blue waves-effect pull-right mar10">Add Patients</a> -->
                    </div>
                </div>

                    <div class="body">
                            
                        <!-- Tab panes -->
                        <div class="tab-content m-t-10">
                            <div class="tab-pane table-responsive active" id="All">
                                <table class="table m-b-0 table-hover">
                                    <thead>
                                        <tr>                                       
                                            <th>Patient Id</th>
                                            <th>Name</th>
                                            <th>Gender</th>
                                            <th>Age</th>
                                            <th>Mobile</th>
                                            <th>Appointment Date</th>
                                            <!-- <th>Status</th> -->
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <?php foreach($GetCheckupAppointments as $Appointments){ ?>
                                        <tr>
                                        <td><?php echo $Appointments["unique_id"] ?></td>
                                            <td><?php echo $Appointments["name"] ?></td>
                                            <td><?php echo $Appointments["gender"] ?></td>
                                            <td><?php echo $Appointments["age"] ?></td>
                                            <td><?php echo $Appointments["mobile"] ?></td>
                                            <td><?php echo $Appointments["appointment_date"] ?></td>
                                            <!-- <td><span class="badge badge-success">Completed</span></td> -->
                                        </tr>
                                        <?php 
                                        }
                                        ?>
                                    </tbody>
                                </table>                            
                            </div>
                        </div>
                    </div>
                   
                </div>
            </div>
        </div>
    </div>
</section>

<div class="modal fade" id="AddAdmin" tabindex="-1" role="dialog">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="title" id="largeModalLabel">Add Admin</h4>
            </div>
            <div class="modal-body"> 
                <div class="row clearfix">
                    <div class="col-sm-12">
                        <div class="form-group">
                            <input type="text" id="name" class="form-control" placeholder="Employee Name">
                        </div>
                        <div class="input-group">
                            <span class="input-group-addon">
                                <i class="zmdi zmdi-calendar"></i>
                            </span>
                            <input type="text" id="joining_date" class="datetimepicker form-control" placeholder="Joining date">
                        </div> 
                        <div class="input-group">
                            <span class="input-group-addon">
                                <i class="zmdi zmdi-calendar"></i>
                            </span>
                            <input type="text" id="dob" class="datetimepicker form-control" placeholder="Date Of Birth">
                        </div> 
                        <div class="form-group">
                            <input type="text" id="age" class="form-control" placeholder="Age">
                        </div>
                        <div class="form-group">
                            <input type="text" id="mobile" class="form-control" placeholder="Mobile">
                        </div>
                        <div class="form-group">
                            <textarea class="form-control" id="address" placeholder="Address"></textarea>
                        </div>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-success btn-round waves-effect AddAdmin">SAVE CHANGES</button>
                <button type="button" class="btn btn-danger waves-effect" data-dismiss="modal">CLOSE</button>
            </div>
        </div>
    </div>
</div>

<div class="modal fade" id="EditAdmin" tabindex="-1" role="dialog">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="title" id="largeModalLabel">Edit Admin</h4>
            </div>
            <div class="modal-body"> 
                <div class="row clearfix">
                    <div class="col-sm-12">
                        <div class="form-group">
                            <input type="hidden" id="id" class="form-control" placeholder="Employee Name">
                            <input type="text" id="Editname" class="form-control" placeholder="Employee Name">
                        </div>
                        <div class="input-group">
                            <span class="input-group-addon">
                                <i class="zmdi zmdi-calendar"></i>
                            </span>
                            <input type="text" id="Editjoining_date" class="datetimepicker form-control" placeholder="Joining date">
                        </div> 
                        <div class="input-group">
                            <span class="input-group-addon">
                                <i class="zmdi zmdi-calendar"></i>
                            </span>
                            <input type="text" id="Editdob" class="datetimepicker form-control" placeholder="Date Of Birth">
                        </div> 
                        <div class="form-group">
                            <input type="text" id="Editage" class="form-control" placeholder="Age">
                        </div>
                        <div class="form-group">
                            <input type="text" id="Editmobile" class="form-control" placeholder="Mobile">
                        </div>
                        <div class="form-group">
                            <textarea class="form-control" id="Editaddress" placeholder="Address"></textarea>
                        </div>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-success btn-round waves-effect" id="UpdateAdmin">Update CHANGES</button>
                <button type="button" class="btn btn-danger waves-effect" data-dismiss="modal">CLOSE</button>
            </div>
        </div>
    </div>
</div>