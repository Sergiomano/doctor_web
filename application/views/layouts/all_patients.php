<section class="content">
    <div class="block-header">
        <div class="row">
            <div class="col-lg-7 col-md-6 col-sm-12">
                <h2>All Patients
                <!-- <small class="text-muted">Welcome to Oreo</small> -->
                </h2>
            </div>
            <div class="col-lg-5 col-md-6 col-sm-12">
                <button class="btn btn-primary btn-icon btn-round hidden-sm-down float-right m-l-10" type="button">
                    <i class="zmdi zmdi-plus"></i>
                </button>
                <ul class="breadcrumb float-md-right">
                    <li class="breadcrumb-item"><a href="index-2.html"><i class="zmdi zmdi-home"></i> Dr. Rajesh</a></li>
                    <li class="breadcrumb-item"><a href="javascript:void(0);">Patients</a></li>
                    <li class="breadcrumb-item active">All Patients</li>
                </ul>
            </div>
        </div>
    </div>
    <div class="container-fluid">
        <div class="row clearfix">
            <div class="col-md-12">
                <div class="card patients-list">
                <div class="row">
                    <div class="col-md-6">
                        <div class="header">
                            <h2><strong>Patients</strong> List</h2>
                        </div>
                    </div>
                    <div class="col-md-6 ">
                        <a href="<?php echo base_url(); ?>patients/add_patients" data-color="light-blue" class="btn bg-light-blue waves-effect pull-right mar10">Add Patients</a>
                    </div>
                </div>

                    <div class="body">
                            
                        <!-- Tab panes -->
                        <div class="tab-content m-t-10">
                            <div class="tab-pane table-responsive active" id="All">
                                <table class="table m-b-0 table-hover">
                                    <thead>
                                        <tr>                   
											<th>Check Appointments</th>  
											<th>Change status</th>                    
                                            <th>Patient Id</th>
                                            <th>Patient Name</th>
                                            <th>joining Date</th>
                                            <th>Age</th>
                                            <th>Mobile</th>
                                            <th>Status</th>
                                            <th>Appointment</th>
                                            <th>Action</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <?php foreach($GetPatients as $Patients){ ?>
                                        <tr class="<?php if($Patients["status"] == 1) {
															echo "bg-highlight";
														}else{

														} ?>">
                                        <td><a href="javascript:void(0);" onclick="CheckAppointments(<?php echo $Patients["id"] ?>);"><i class="zmdi zmdi-plus"></i></a></td>
										
										<td><label class="switch JobClose">
											<input type="hidden" id="GetPatientId" name="" class="primary"
													value="<?php echo $Patients["id"] ?>">

														<input type="checkbox" name="ChangeStatus" class="primary" onchange="ChangeStatus(<?php echo $Patients["id"] ?>)"
														<?php if($Patients["status"] == 1) {
															echo "checked";
														}else{

														} ?>
														>
														<span class="slider round"></span>
													</label></td>

                                        <td><a href="<?php echo base_url(); ?>Patients/add_patients_info/<?php echo $Patients["id"] ?>"><?php echo $Patients["unique_id"] ?></a></td>
                                            <td><a href="<?php echo base_url(); ?>Patients/add_patients_info/<?php echo $Patients["id"] ?>"><?php echo $Patients["name"] ?></a></td>
                                            <td><?php echo $Patients["joining_date"] ?></td>
                                            <td><?php echo $Patients["age"] ?></td>
                                            <td><?php echo $Patients["mobile"] ?></td>
                                            <td>
											<?php if($Patients["status"] == 1) {
													echo "<span class='badge badge-danger'>In Active</span>";
													
												}else{
													echo "<span class='badge badge-success'>Active</span>";
												} ?>
												
												</td>
                                            <td><a href="javascript:void(0);" onclick="BookAppointment(<?php echo $Patients["id"] ?>);"><span class="badge badge-danger">Book Appointment</span></a></td>
											<td><a href="<?php echo base_url(); ?>Patients/edit_patients/<?php echo $Patients["id"] ?>"><i class="zmdi zmdi-edit"></i></a>&nbsp; &nbsp;&nbsp; &nbsp;
											<a href="<?php echo base_url(); ?>Patients/add_patients_info/<?php echo $Patients["id"] ?>"><i class="zmdi zmdi-info"></i></a></td>
                                        </tr>
                                        <?php 
                                        }
                                        ?>
                                    </tbody>
                                </table>                            
                            </div>
                        </div>
                    </div>
                   
                </div>
            </div>
        </div>
    </div>
</section>

<div class="modal fade" id="BookAppointment" tabindex="-1" role="dialog">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="title" id="largeModalLabel">Book Appointment</h4>
            </div>
            <div class="modal-body"> 
                <div class="row clearfix">
                    <div class="col-sm-12">
                        <div class="form-group">
                            <input type="hidden" id="Patientid" class="form-control" placeholder="Id">
                            <select class="form-control show-tick" id="location">
                                <option value="">Select Location</option>
                                <?php foreach($GetLocation as $Location){ ?>
                                    <option value="<?php echo $Location["id"]; ?>"><?php echo $Location["location"]; ?></option>
                                <?php } ?>
                            </select>
                        </div>
                        
                        <div class="input-group">
                            <span class="input-group-addon">
                                <i class="zmdi zmdi-calendar"></i>
                            </span>
                            <!-- <input type="text" id="dob" class="datetimepicker form-control" placeholder="Date Of Birth"> -->
                            <input type="date" id="appointment_date" class="form-control" placeholder="Date Of Birth">
                        </div> 
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-success btn-round waves-effect BookAppointments">Book Appointment</button>
                <button type="button" class="btn btn-danger waves-effect" data-dismiss="modal">CLOSE</button>
            </div>
        </div>
    </div>
</div>

<div class="modal fade" id="CheckAppointments" tabindex="-1" role="dialog">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="title" id="largeModalLabel">Check Appointments</h4>
            </div>
            <div class="modal-body"> 
                <div class="row clearfix">
                    <div class="col-sm-12">
                    <table class="table m-b-0 table-hover">
                        <thead>
                            <tr>                   
                                <th>Patient Name</th>                    
                                <th>Location</th>
                                <th>Appointment Date</th>
                            </tr>
                        </thead>
                        <tbody id="GetAppointments">
                            
                        </tbody>
                    </table>    
                    </div>
                </div>
            </div>
            <!-- <div class="modal-footer">
                <button type="button" class="btn btn-success btn-round waves-effect AddAdmin">SAVE CHANGES</button>
                <button type="button" class="btn btn-danger waves-effect" data-dismiss="modal">CLOSE</button>
            </div> -->
        </div>
    </div>
</div>

<div class="modal fade" id="EditAdmin" tabindex="-1" role="dialog">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="title" id="largeModalLabel">Edit Admin</h4>
            </div>
            <div class="modal-body"> 
                <div class="row clearfix">
                    <div class="col-sm-12">
                        <div class="form-group">
                            <input type="hidden" id="id" class="form-control" placeholder="Employee Name">
                            <input type="text" id="Editname" class="form-control" placeholder="Employee Name">
                        </div>
                        <div class="input-group">
                            <span class="input-group-addon">
                                <i class="zmdi zmdi-calendar"></i>
                            </span>
                            <input type="text" id="Editjoining_date" class="datetimepicker form-control" placeholder="Joining date">
                        </div> 
                        <div class="input-group">
                            <span class="input-group-addon">
                                <i class="zmdi zmdi-calendar"></i>
                            </span>
                            <input type="text" id="Editdob" class="datetimepicker form-control" placeholder="Date Of Birth">
                        </div> 
                        <div class="form-group">
                            <input type="text" id="Editage" class="form-control" placeholder="Age">
                        </div>
                        <div class="form-group">
                            <input type="text" id="Editmobile" class="form-control" placeholder="Mobile">
                        </div>
                        <div class="form-group">
                            <textarea class="form-control" id="Editaddress" placeholder="Address"></textarea>
                        </div>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-success btn-round waves-effect" id="UpdateAdmin">Update CHANGES</button>
                <button type="button" class="btn btn-danger waves-effect" data-dismiss="modal">CLOSE</button>
            </div>
        </div>
    </div>
</div>
