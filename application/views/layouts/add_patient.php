<section class="content">
    <div class="block-header">
        <div class="row">
            <div class="col-lg-7 col-md-6 col-sm-12">
                <h2>Add Patient
                </h2>
            </div>
            <div class="col-lg-5 col-md-6 col-sm-12">               
                <ul class="breadcrumb float-md-right">
					<li class="breadcrumb-item"><a href="javascript:void(0);"><i class="zmdi zmdi-home"></i> Patient</a></li>
                    <li class="breadcrumb-item active">Add Patient</li>
                </ul>
            </div>
        </div>
    </div>
    <div class="container-fluid">
    <div class="row clearfix">
        <div class="col-lg-12 col-md-12 col-sm-12">
            <div class="card">
                <div class="header">
                    <h2><strong>Patient</strong> Informations</h2>
                </div>
                <div class="body"> 
                    <!-- Nav tabs -->
                    <ul class="nav nav-tabs" role="tablist">
                        <li class="nav-item"><a class="nav-link active" data-toggle="tab" href="#home_with_icon_title"> <i class="zmdi zmdi-home"></i> Add Details </a></li>
                        <!-- <li class="nav-item"><a class="nav-link" data-toggle="tab" href="#profile_with_icon_title"><i class="zmdi zmdi-account"></i> Compliants History </a></li> -->
                        <!-- <li class="nav-item"><a class="nav-link" data-toggle="tab" href="#messages_with_icon_title"><i class="zmdi zmdi-email"></i> Check Bookings </a></li> -->
                    </ul>
                    
                    <!-- Tab panes -->
                    <div class="tab-content">
                    
                        <div role="tabpanel" class="tab-pane in active" id="home_with_icon_title"> 
                            <div class="header">
                                <h2><strong>Basic</strong> Information <small>Description text here...</small> </h2>
                            </div>
                            <div class="body">
                            <div class="row clearfix">
                            <div class="col-sm-6">
                                <div class="form-group">
                                    <input type="text" id="name" class="form-control" placeholder="Your Name">
                                </div>
                            </div>
                            <!-- <div class="col-sm-4">
                                <div class="form-group">
                                    <input type="text" class="form-control" id="lname" placeholder="Last Name">
                                </div>
                            </div> -->
                            <div class="col-sm-6">
                                <div class="form-group">
                                    <input type="text" class="form-control" id="mobile" placeholder="Phone No.">
                                    <input type="checkbox"> Same As Whatsapp Number
                                </div>
                            </div>
                        </div>
                        <div class="row clearfix">  
                            <div class="col-sm-4">
                                <div class="form-group">
                                    <input type="text" id="whatsapp_no" class="form-control" placeholder="Whatsapp number">
                                </div>
                            </div>  
                            <div class="col-sm-4">
                                <div class="form-group">
                                    <input type="text" id="age" class="form-control" placeholder="Age">
                                </div>
                            </div>
                            <div class="col-sm-4">
                                <select class="form-control show-tick" id="gender">
                                    <option value="">Select Gender</option>
                                    <option value="Male">Male</option>
                                    <option value="Female">Female</option>
                                </select>
                            </div>                            
                            
                        </div>
                        <div class="row clearfix"> 
                            <div class="col-sm-6">
                                <div class="form-group">
                                    <input type="text" class="form-control" id="blood" placeholder="Enter blood group">
                                </div>
                            </div>                           
                            <div class="col-sm-6">
                                <div class="input-group">
                                    <span class="input-group-addon">
                                        <i class="zmdi zmdi-calendar"></i>
                                    </span>
                                    <input type="date" id="dob" class=" form-control" placeholder="Please choose date of birth">
                                </div>                               
                            </div> -->

                            <!-- <div class="col-sm-4">
                                <div class="input-group">
                                    <span class="input-group-addon">
                                        <i class="zmdi zmdi-calendar"></i>
                                    </span>
                                    <input type="text" id="appointment" class="datetimepicker form-control" placeholder="Please choose appointment date">
                                </div>                               
                            </div>    
                        </div>
                        <div class="row clearfix">   
                             
                           <div class="col-sm-12">
                                <button type="submit" class="btn btn-primary btn-round AddPatients">Submit</button>
                                <button type="submit" class="btn btn-default btn-round btn-simple">Cancel</button>
                            </div>
                        </div>
                    
                            </div>

                    <div class="header">
                                <h2><strong>Compliants</strong> Information </h2>
                            </div>
                            <div class="body">
                                <div class="row clearfix">  
                                    <div class="col-sm-6">
                                        <select class="form-control show-tick" id="compliants" onchange="CheckComplaints()">
                                            <option value="">Select Compliants</option>
                                            <?php foreach($GetCompliants as $Compliants){ ?>
                                                <option value="<?php echo $Compliants["id"]; ?>"><?php echo $Compliants["compliant_name"]; ?></option>
                                            <?php } ?>
                                        </select>
                                         <input type="text" id="compliants" class="form-control typeahead" placeholder="Compliants name">
                                        <br/> -->
                                        <table class="table m-b-0 table-hover" id="ComplaintsDetails">
                                            <thead>
                                                <tr>                   
                                                    <th>Complaint Name</th>                    
                                                    <th>Details</th>
                                                    <th>Action</th>
                                                </tr>
                                            </thead>
                                            <tbody >
                                                    <?php foreach($GetCompliantsByPatientId as $Compliants){ ?>
                                                <tr>
                                                    <td><span class="list-name"><?php echo $Compliants["compliant_name"]; ?></span></td>
                                                    <td><?php echo $Compliants["description"]; ?></td>
                                                    <td><a href="javascrip:void;" onclick="EditCompliantsInfo(<?php echo $Compliants["id"] ?>)"><i class="zmdi zmdi-edit"></i></a>&nbsp; &nbsp;<a href="javascript:void(0);" onclick="Delete_ComplaintsInfo(<?php echo $Compliants["id"] ?>);"><i class="zmdi zmdi-delete"></i></a></td>
                                                </tr>
                                            <?php } ?>
                                            </tbody>
                                        </table>  
                                    </div> 
                                    <div class="col-sm-6">
                                    <!-- <label>Description</label> -->
                                        <input type="text" id="description" class="form-control" placeholder="Description">
                                        <!-- <textarea class="form-control" name="description" id="description" placeholder="Address"></textarea> -->
                                    </div> 

                                    <div class="col-sm-6">
                                        
                                    </div> 
                                    <div class="col-sm-6">
                                        <button type="submit" class="btn btn-primary btn-round AddCompliantsInfo">Save</button>
                                    </div> 
                            </div>
                       
                    </div>

                    <div class="header">
                                <h2><strong>Examination</strong> Information </h2>
                            </div>
                            <div class="body">
                                <div class="row clearfix">  
                                    <div class="col-sm-6">
                                        <select class="form-control show-tick" id="examination" onchange="CheckExamination()">
                                            <option value="">Select Examination</option>
                                            <?php foreach($GetExamination as $Examination){ ?>
                                                <option value="<?php echo $Examination["id"]; ?>"><?php echo $Examination["compliant_name"]; ?></option>
                                            <?php } ?>
                                        </select>
                                        <!-- <input type="text" id="examination" class="form-control" placeholder="Examination name"> -->
                                        <br/>
                                        <table class="table m-b-0 table-hover" id="ExaminationDetails">
                                            <thead>
                                                <tr>                   
                                                    <th>Examination Name</th>                    
                                                    <th>Details</th>
                                                    <th>Action</th>
                                                </tr>
                                            </thead>
                                            <tbody >
                                                    <?php foreach($GetExaminationByPatientId as $Examination){ ?>
                                                <tr>
                                                    <td><span class="list-name"><?php echo $Examination["compliant_name"]; ?></span></td>
                                                    <td><?php echo $Examination["description"]; ?></td>
                                                    <td><a href="javascrip:void;" onclick="EditExamination(<?php echo $Examination["id"] ?>)"><i class="zmdi zmdi-edit"></i></a>&nbsp; &nbsp;<a href="javascript:void(0);" onclick="Delete_ComplaintsInfo(<?php echo $Examination["id"] ?>);"><i class="zmdi zmdi-delete"></i></a></td>
                                                </tr>
                                            <?php } ?>
                                            </tbody>
                                        </table>
                                    </div> 
                                    <div class="col-sm-6">
                                    <!-- <label>Description</label> -->
                                        <!-- <textarea class="form-control" name="examination_description" id="examination_description" placeholder="Description"></textarea> -->
                                        <input type="text" id="examination_description" class="form-control" placeholder="Description">
                                    </div> 

                                    <div class="col-sm-6">
                                        
                                    </div> 
                                    <div class="col-sm-6">
                                        <button type="submit" class="btn btn-primary btn-round AddExaminationInfo">Save</button>
                                    </div> 
                            </div>
                       
                    </div>

                    <div class="header">
                                <h2><strong>Investigation</strong> Information </h2>
                            </div>
                            <div class="body">
                                <div class="row clearfix">  
                                <div class="col-sm-6">
                                        <select class="form-control show-tick" id="investigation" onchange="CheckInvestigation()">
                                            <option value="">Select Examination</option>
                                            <?php foreach($GetInvestigation as $Examination){ ?>
                                                <option value="<?php echo $Examination["id"]; ?>"><?php echo $Examination["compliant_name"]; ?></option>
                                            <?php } ?>
                                        </select>
                                        <!-- <input type="text" id="investigation" class="form-control" placeholder="Investigation name"> -->
                                        <br/>
                                        <table class="table m-b-0 table-hover" id="InvestigationDetails">
                                            <thead>
                                                <tr>                   
                                                    <th>Investigation Name</th>                    
                                                    <th>Details</th>
                                                    <th>Action</th>
                                                </tr>
                                            </thead>
                                            <tbody >
                                                    <?php foreach($GetInvestigationByPatientId as $Investigation){ ?>
                                                <tr>
                                                    <td><span class="list-name"><?php echo $Investigation["compliant_name"]; ?></span></td>
                                                    <td><?php echo $Investigation["description"]; ?></td>
                                                    <td><a href="javascrip:void;" onclick="EditInvestigation(<?php echo $Investigation["id"] ?>)"><i class="zmdi zmdi-edit"></i></a>&nbsp; &nbsp;<a href="javascript:void(0);" onclick="Delete_ComplaintsInfo(<?php echo $Investigation["id"] ?>);"><i class="zmdi zmdi-delete"></i></a></td>
                                                </tr>
                                            <?php } ?>
                                            </tbody>
                                        </table>
                                    </div> 
                                    <div class="col-sm-6">
                                    <!-- <label>Description</label> -->
                                        <!-- <textarea class="form-control" name="examination_description" id="examination_description" placeholder="Description"></textarea> -->
                                        <input type="text" id="investigation_description" class="form-control" placeholder="Description">
                                    </div> 

                                    <div class="col-sm-6">
                                        
                                    </div> 
                                    <div class="col-sm-6">
                                        <button type="submit" class="btn btn-primary btn-round AddInvestigationInfo">Add</button>
                                        <a href="javascript:void(0);" data-toggle="modal" data-target="#AddInvestigationImages"><i class="material-icons">camera_alt</i></a>
                                        <!-- <a href="#"><i class="material-icons">image</i></a> -->
                                    </div> 
                            </div>
                       
                    </div>

                    <div class="header">
                                <h2><strong>Diagnosis</strong> Information </h2>
                            </div>
                            <div class="body">
                                <div class="row clearfix">  
                                <div class="col-sm-6">
                                        <select class="form-control show-tick" id="daignotics" onchange="CheckDiagnotics()">
                                            <option value="">Select Diagnosis</option>
                                            <?php foreach($GetDiagnotics as $Diagnotics){ ?>
                                                <option value="<?php echo $Diagnotics["id"]; ?>"><?php echo $Diagnotics["compliant_name"]; ?></option>
                                            <?php } ?>
                                        </select>

                                        <!-- <input type="text" id="daignotics" class="form-control" placeholder="Daignotics name"> -->
                                        <br/>
                                        <table class="table m-b-0 table-hover" id="DaignoticsDetails">
                                            <thead>
                                                <tr>                   
                                                    <th>Diagnotics Name</th>                    
                                                    <th>Details</th>
                                                    <th>Action</th>
                                                </tr>
                                            </thead>
                                            <tbody >
                                                    <?php foreach($GetDiagnoticsByPatientId as $Diagnotics){ ?>
                                                <tr>
                                                    <td><span class="list-name"><?php echo $Diagnotics["compliant_name"]; ?></span></td>
                                                    <td><?php echo $Diagnotics["description"]; ?></td>
                                                    <td><a href="javascrip:void;" onclick="EditInvestigation(<?php echo $Diagnotics["id"] ?>)"><i class="zmdi zmdi-edit"></i></a>&nbsp; &nbsp;<a href="javascript:void(0);" onclick="Delete_ComplaintsInfo(<?php echo $Diagnotics["id"] ?>);"><i class="zmdi zmdi-delete"></i></a></td>
                                                </tr>
                                            <?php } ?>
                                            </tbody>
                                        </table>
                                    </div> 
                                    <div class="col-sm-6">
                                    <!-- <label>Description</label> -->
                                        <!-- <textarea class="form-control" name="examination_description" id="examination_description" placeholder="Description"></textarea> -->
                                        <input type="text" id="daignotics_description" class="form-control" placeholder="Description">
                                    </div> 

                                    <div class="col-sm-6">
                                        
                                    </div> 
                                    <div class="col-sm-6">
                                        <button type="submit" class="btn btn-primary btn-round AddDaignoticsInfo">Save</button>
                                        <a href="javascript:void(0);" data-toggle="modal" data-target="#AddDiagnoticsImages"><i class="material-icons">camera_alt</i></a>
                                        <!-- <a href="#"><i class="material-icons">image</i></a> -->
                                    </div> 
                            </div>
                       
                    </div>

                    <div class="header">
                                <h2><strong>Treatments</strong> Information </h2>
                            </div>
                            <div class="body">
                                <div class="row clearfix">  
                                    <div class="col-sm-3">
										<label for="">Drug</label>
                                        <input type="text" id="drug" class="form-control" placeholder="Drug name">
                                        
                                    </div>
                                    <div class="col-sm-3">
										<label for="">Dose</label>
                                        <input type="text" id="dose" class="form-control" placeholder="Dose name">
                                        
                                    </div>

                                    <div class="col-sm-1 treatments-pad-right">
										<label for="">M</label>
                                        <input type="text" id="morning" class="form-control border-radius0" placeholder="">
                                        
                                    </div>
                                    <div class="col-sm-1 treatments-pad-right">
										<label for="">A</label>
                                        <input type="text" id="afternoon" class="form-control border-radius0" placeholder="">
                                        
                                    </div>
                                    <div class="col-sm-1 treatments-pad-right">
										<label for="">E</label>
                                        <input type="text" id="evening" class="form-control border-radius0" placeholder="">
                                        
                                    </div>
                                    <div class="col-sm-1">
										<label for="">N</label>
                                        <input type="text" id="night" class="form-control border-radius0" placeholder="">
                                        
                                    </div>

                                    <div class="col-sm-2">
										<label for="">SOS</label>	
                                        <input type="text" id="sos" class="form-control" placeholder="Sos">
                                        
                                    </div>
							</div>
							
							<div class="row clearfix">  
                                    <div class="col-sm-3">
                                        
                                    </div>
                                    <div class="col-sm-3">
                                        
                                    </div>

                                    <div class="col-sm-1 treatments-pad-right">
                                        
                                    </div>
                                    <div class="col-sm-1 treatments-pad-right">
									<input type="radio" name="radio1" id="af_food" value="1">
<label for="radio1">
    AF
</label>
										<!-- <label for="af_food">AF</label>
                                        <input type="radio" id="af_food" class="form-control border-radius0" placeholder="" value="1"> -->
                                        
                                    </div>
                                    <div class="col-sm-1 treatments-pad-right">
									<input type="radio" name="radio1" class="bf_food" id="radio2" value="1" >
<label for="radio2">
    BF
</label>

										<!-- <label for="bf_food">BF</label>
                                        <input type="radio" id="bf_food" class="bf_food form-control border-radius0" placeholder="" value="2"> -->
                                        
                                    </div>
                                    <div class="col-sm-1">
									<input type="radio" name="radio1" class="bf_food" id="radio3" value="1" >
<label for="radio3">
    BF
</label>

									<!-- <label for="na">NA</label>
                                        <input type="radio" id="na" class="bf_food form-control border-radius0" placeholder="" value="3"> -->
                                    </div>

                                    <div class="col-sm-2">
										<button type="button" id="AddTreatments" class="mar-top20 btn btn-primary btn-round">Save</button>
                                    </div>
							</div>
							
							<table class="table m-b-0 table-hover" id="TreatmentDetails">
                                            <thead>
                                                <tr>                   
                                                    <th>Drug</th>                    
                                                    <th>Dose</th>
													<th>Morning</th>
													<th>Afternoon</th>
													<th>Evening</th>
													<th>Night</th>
													<th>After food</th>
													<th>Before food</th>
													<th>SOS</th>
													<th>Action</th>
                                                </tr>
                                            </thead>
                                            <tbody >
											<?php foreach($GetTreatmentsByPatientId as $Patients){ ?>
												<tr>
												<td><?php echo $Patients['drug'] ?></td>
												<td><?php echo $Patients['dose'] ?></td>
												<td><?php echo $Patients['morning'] ?></td>
												<td><?php echo $Patients['afternoon'] ?></td>
												<td><?php echo $Patients['evening'] ?></td>
												<td><?php echo $Patients['night'] ?></td>
												<td><?php echo $Patients['after_food'] ?></td>
												<td><?php echo $Patients['before_food'] ?></td>
												<td><?php echo $Patients['sos'] ?></td>
												<td><a href="javascript:void(0);" onclick="EditTreatments(<?php echo $Patients["id"] ?>)"><i class="zmdi zmdi-edit"></i></a>&nbsp; &nbsp;<a href="javascript:void(0);" onclick="Delete_TreatmentsInfo(<?php echo $Patients["id"] ?>);"><i class="zmdi zmdi-delete"></i></a></td>
												</tr>
											<?php } ?> 
                                            </tbody>
                                        </table>
                            <br/>

                            <div class="header">
                                <h2><strong>Surgery</strong> Information </h2>
                            </div>
                            <div class="row clearfix">   
                                <div class="col-sm-3">
                                    <select class="form-control show-tick" id="surgery">
                                        <option >Select Surgery</option>
                                        <option value="00">Spin Surgery</option>
                                        <option value="01">Brain Surgery</option>
                                    </select>
                                    <!-- <input type="text" id="surgery" class="form-control" placeholder="Daignotics name"> -->
                                    
                                        <br/>
                                        <table class="table m-b-0 table-hover" id="SurgeryDetails">
                                            <thead>
                                                <tr>                   
                                                    <th>Surgery Name</th>                    
													<th>Date</th>
													<th>Details</th>
                                                    <th>Action</th>
                                                </tr>
                                            </thead>
                                            <tbody >
                                                 <?php foreach($GetSurgeryByPatientId as $Surgery){ ?>
                                                <tr>
                                                    <td><span class="list-name"><?php
                                                        if($Surgery["surgery_name"] == 0){
                                                            echo "Spin Surgery";
                                                        }else{
                                                            echo "Brain Surgery";
                                                        }
													?></span></td>
													<td><?php echo $Surgery["date"]; ?></td>
                                                    <td><?php echo $Surgery["details"]; ?></td>
                                                    <td><a href="javascript:void(0);" onclick="EditSurgery(<?php echo $Surgery["id"] ?>)"><i class="zmdi zmdi-edit"></i></a>&nbsp; &nbsp;<a href="javascript:void(0);" onclick="Delete_Surgery(<?php echo $Surgery["id"] ?>);"><i class="zmdi zmdi-delete"></i></a></td>
                                                </tr>
                                            <?php } ?> 
                                            </tbody>
                                        </table>
								</div>
								<div class="col-sm-3">
                                    <input type="date" class="form-control" id="get_date">
                                </div> 
                                <div class="col-sm-6">
                                    <!-- <textarea class="form-control" name="surgery_description" id="surgery_description" placeholder="Description"></textarea> -->
                                    <input type="text" id="surgery_description" class="form-control" placeholder="Description">
                                        <!-- <textarea class="form-control" name="treatments_description" id="treatments_description" placeholder="Description"></textarea> -->
                                </div> 

                                <div class="col-sm-6">
                                    
                                </div> 
                                <div class="col-sm-6">
                                    <button type="submit" class="btn btn-primary btn-round AddSurgery">Save</button>
                                    <a href="javascript:void(0);" data-toggle="modal" data-target="#AddSurgeryImages"><i class="material-icons">camera_alt</i></a>
                                    <a href="javascript:void(0);" data-toggle="modal" data-target="#AddSurgeryVideo"><i class="material-icons">videocam</i></a>
                                </div> 
                            </div>
                       
                        <!-- <div class="col-sm-12">
                            <button type="submit" class="btn btn-primary btn-round UpdatePatientsInfo">Submit</button>
                            <button type="submit" class="btn btn-default btn-round btn-simple">Cancel</button>
                        </div> -->
                    </div>
                        </div>
                        <div role="tabpanel" class="tab-pane" id="profile_with_icon_title"> 
                        <h4>Compliant History</h4>
                        <table class="table m-b-0 table-hover">
                                    <thead>
                                        <tr>                                       
                                            <th>Compliant Name</th>
                                            <th>Description</th>
                                            <th>Date</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                    <?php foreach($GetCompliantsByPatientId as $Compliants){ ?>
                                        <tr>
                                            <td><span class="list-name"><?php echo $Compliants["compliant_name"]; ?></span></td>
                                            <td><?php echo $Compliants["details"]; ?></td>
                                            <td><?php echo date('d-m-Y', strtotime($Compliants["created_at"])); ?></td>
                                        </tr>
                                    <?php } ?>
                                    </tbody>
                                </table> 
                                <hr/>
                                <h4>Examination History</h4>
                        <table class="table m-b-0 table-hover">
                                    <thead>
                                        <tr>                                       
                                            <th>Examination Name</th>
                                            <th>Description</th>
                                            <th>Date</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                    <?php foreach($GetExaminationByPatientId as $Examination){ ?>
                                        <tr>
                                            <td><span class="list-name"><?php echo $Examination["compliant_name"]; ?></span></td>
                                            <td><?php echo $Examination["details"]; ?></td>
                                            <td><?php echo date('d-m-Y', strtotime($Examination["created_at"])); ?></td>
                                        </tr>
                                    <?php } ?>
                                    </tbody>
                                </table> 


                                <hr/>
                                <h4>Investigation History</h4>
                        <table class="table m-b-0 table-hover">
                                    <thead>
                                        <tr>                                       
                                            <th>Investigation Name</th>
                                            <th>Description</th>
                                            <th>Date</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                    <?php foreach($GetInvestigationByPatientId as $Investigation){ ?>
                                        <tr>
                                            <td><span class="list-name"><?php echo $Investigation["compliant_name"]; ?></span></td>
                                            <td><?php echo $Investigation["details"]; ?></td>
                                            <td><?php echo date('d-m-Y', strtotime($Investigation["created_at"])); ?></td>
                                        </tr>
                                    <?php } ?>
                                    </tbody>
                                </table> 


                                <hr/>
                                <h4>Treatments History</h4>
                        <table class="table m-b-0 table-hover">
                                    <thead>
                                        <tr>                                       
                                            <th>Treatment Name</th>
                                            <th>Description</th>
                                            <th>Date</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                    <?php foreach($GetTreatmentsByPatientId as $Treatments){ ?>
                                        <tr>
                                            <td><span class="list-name"><?php echo $Treatments["compliant_name"]; ?></span></td>
                                            <td><?php echo $Treatments["details"]; ?></td>
                                            <td><?php echo date('d-m-Y', strtotime($Treatments["created_at"])); ?></td>
                                        </tr>
                                    <?php } ?>
                                    </tbody>
                                </table> 
                        </div>
                        
                        

                    </div>
                </div>
            </div>
        </div>
    </div>

    </div>
</section>



<div class="modal fade" id="EditCompliants" tabindex="-1" role="dialog">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="title" id="largeModalLabel">Edit Compliants</h4>
            </div>
            <div class="modal-body"> 
                <div class="row clearfix">
                    <div class="col-sm-12">
                    <div class="form-group">
                        <input type="hidden" id="id" class="form-control" placeholder="Employee Name">       
                        <input type="text" id="Editname" class="form-control" placeholder="Compliant Name">
                    </div>
                        
                        <div class="form-group">
                            <textarea class="form-control" id="Editdescription" placeholder="Description"></textarea>
                        </div>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-success btn-round waves-effect" id="UpdateCompliantsInfo">Update CHANGES</button>
                <button type="button" class="btn btn-danger waves-effect" data-dismiss="modal">CLOSE</button>
            </div>
        </div>
    </div>
</div>




<div class="modal fade" id="EditExamination" tabindex="-1" role="dialog">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="title" id="largeModalLabel">Edit Examination</h4>
            </div>
            <div class="modal-body"> 
                <div class="row clearfix">
                    <div class="col-sm-12">
                    <div class="form-group">
                        <input type="hidden" id="eid" class="form-control" placeholder="Employee Name">       
                        <input type="text" id="Editexaminationname" class="form-control" placeholder="Examination Name">
                    </div>
                        
                        <div class="form-group">
                            <label>Description</label>
                            <textarea class="form-control " name="Editdescription" id="Editexaminationdescription" placeholder="Description"></textarea>
                        </div>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-success btn-round waves-effect" id="UpdateExamination">Update CHANGES</button>
                <button type="button" class="btn btn-danger waves-effect" data-dismiss="modal">CLOSE</button>
            </div>
        </div>
    </div>
</div>


<div class="modal fade" id="EditSurgeryModal" tabindex="-1" role="dialog">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="title" id="largeModalLabel">Edit Surgery</h4>
            </div>
            <div class="modal-body"> 
                <div class="row clearfix">
                    <div class="col-sm-12">
                    <div class="form-group">
                        <input type="hidden" id="Sid" class="form-control" placeholder="Employee Name">       
                        <!-- <input type="text" id="EditSurgeryname" class="form-control" placeholder="Compliant Name"> -->
                        <select class="form-control show-tick" id="EditSurgeryname">
                            <option >Select Surgery</option>
                            <option value="00">Spin Surgery</option>
                            <option value="01">Brain Surgery</option>
                        </select>
                    </div>
                        
                        <div class="form-group">
                            <textarea class="form-control" id="EditSurgerydescription" placeholder="Description"></textarea>
                        </div>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-success btn-round waves-effect" id="UpdateSurgery">Update CHANGES</button>
                <button type="button" class="btn btn-danger waves-effect" data-dismiss="modal">CLOSE</button>
            </div>
        </div>
    </div>
</div>



<div class="modal fade" id="AddInvestigationImages" tabindex="-1" role="dialog">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="title" id="largeModalLabel">Add Investigation Images</h4>
            </div>
            <div class="modal-body"> 
            <form action="<?php echo base_url(); ?>Patient_info/investigationimages" method="post" enctype='multipart/form-data'>
                <div class="row clearfix">
                    <div class="col-sm-12">
                    <div class="form-group">
                        <input type="file" id="" name="userfile[]" class="form-control" placeholder="Examination Name" multiple="">
                    </div>
                        
                    </div>
                </div>
                <button type="submit"  class="btn btn-success btn-round waves-effect">Update CHANGES</button>
            </form>
            </div>
            <div class="modal-footer">
                <!-- <button type="button" class="btn btn-success btn-round waves-effect" id="UpdateExamination">Update CHANGES</button> -->
                <button type="button" class="btn btn-danger waves-effect" data-dismiss="modal">CLOSE</button>
            </div>
        </div>
    </div>
</div>



<div class="modal fade" id="AddDiagnoticsImages" tabindex="-1" role="dialog">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="title" id="largeModalLabel">Add Diagnotics Images</h4>
            </div>
            <div class="modal-body"> 
            <form action="<?php echo base_url(); ?>Patient_info/diagnoticsimages" method="post" enctype='multipart/form-data'>
                <div class="row clearfix">
                    <div class="col-sm-12">
                    <div class="form-group">
                        <input type="file" id="" name="userfile[]" class="form-control" placeholder="Examination Name" multiple="">
                    </div>
                        
                    </div>
                </div>
                <button type="submit"  class="btn btn-success btn-round waves-effect">Update CHANGES</button>
            </form>
            </div>
            <div class="modal-footer">
                <!-- <button type="button" class="btn btn-success btn-round waves-effect" id="UpdateExamination">Update CHANGES</button> -->
                <button type="button" class="btn btn-danger waves-effect" data-dismiss="modal">CLOSE</button>
            </div>
        </div>
    </div>
</div>



<div class="modal fade" id="AddTreatmentsImages" tabindex="-1" role="dialog">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="title" id="largeModalLabel">Add Treatments Images</h4>
            </div>
            <div class="modal-body"> 
            <form action="<?php echo base_url(); ?>Patient_info/treatmentsimages" method="post" enctype='multipart/form-data'>
                <div class="row clearfix">
                    <div class="col-sm-12">
                    <div class="form-group">
                        <input type="file" id="" name="userfile[]" class="form-control" placeholder="Examination Name" multiple="">
                    </div>
                        
                    </div>
                </div>
                <button type="submit"  class="btn btn-success btn-round waves-effect">Update CHANGES</button>
            </form>
            </div>
            <div class="modal-footer">
                <!-- <button type="button" class="btn btn-success btn-round waves-effect" id="UpdateExamination">Update CHANGES</button> -->
                <button type="button" class="btn btn-danger waves-effect" data-dismiss="modal">CLOSE</button>
            </div>
        </div>
    </div>
</div>

<div class="modal fade" id="AddSurgeryImages" tabindex="-1" role="dialog">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="title" id="largeModalLabel">Add Treatments Images</h4>
            </div>
            <div class="modal-body"> 
            <form action="<?php echo base_url(); ?>Patient_info/surgeryimages" method="post" enctype='multipart/form-data'>
                <div class="row clearfix">
                    <div class="col-sm-12">
                    <div class="form-group">
                        <input type="file" id="" name="userfile[]" class="form-control" placeholder="Examination Name" multiple="">
                    </div>
                        
                    </div>
                </div>
                <button type="submit"  class="btn btn-success btn-round waves-effect">Update CHANGES</button>
            </form>
            </div>
            <div class="modal-footer">
                <!-- <button type="button" class="btn btn-success btn-round waves-effect" id="UpdateExamination">Update CHANGES</button> -->
                <button type="button" class="btn btn-danger waves-effect" data-dismiss="modal">CLOSE</button>
            </div>
        </div>
    </div>
</div>


<div class="modal fade" id="AddSurgeryVideo" tabindex="-1" role="dialog">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="title" id="largeModalLabel">Add Surgery Video</h4>
            </div>
            <div class="modal-body"> 
            <form action="<?php echo base_url(); ?>Patient_info/UploadVideo" method="post" enctype='multipart/form-data'>
                <div class="row clearfix">
                    <div class="col-sm-12">
                    <div class="form-group">
                        <input type="file" id="" name="surgery_video" class="form-control" placeholder="Examination Name">
                    </div>
                        
                    </div>
                </div>
                <button type="submit"  class="btn btn-success btn-round waves-effect">Update CHANGES</button>
            </form>
            </div>
            <div class="modal-footer">
                <!-- <button type="button" class="btn btn-success btn-round waves-effect" id="UpdateExamination">Update CHANGES</button> -->
                <button type="button" class="btn btn-danger waves-effect" data-dismiss="modal">CLOSE</button>
            </div>
        </div>
    </div>
</div>





<div class="modal fade" id="EditTreatments" tabindex="-1" role="dialog">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="title" id="largeModalLabel">Edit Treatments</h4>
            </div>
            <div class="modal-body"> 
			<div class="row clearfix">  
                                    <div class="col-sm-3">
										<label for="">Drug</label>
										<input type="hidden" id="Tid" class="form-control" placeholder="Drug name">
                                        <input type="text" id="Edit_drug" class="form-control" placeholder="Drug name">
                                        
                                    </div>
                                    <div class="col-sm-3">
										<label for="">Dose</label>
                                        <input type="text" id="Edit_dose" class="form-control" placeholder="Dose name">
                                        
                                    </div>

                                    <div class="col-sm-1 treatments-pad-right">
										<label for="">M</label>
                                        <input type="text" id="Edit_morning" class="form-control border-radius0" placeholder="">
                                        
                                    </div>
                                    <div class="col-sm-1 treatments-pad-right">
										<label for="">A</label>
                                        <input type="text" id="Edit_afternoon" class="form-control border-radius0" placeholder="">
                                        
                                    </div>
                                    <div class="col-sm-1 treatments-pad-right">
										<label for="">E</label>
                                        <input type="text" id="Edit_evening" class="form-control border-radius0" placeholder="">
                                        
                                    </div>
                                    <div class="col-sm-1">
										<label for="">N</label>
                                        <input type="text" id="Edit_night" class="form-control border-radius0" placeholder="">
                                        
                                    </div>

                                    <div class="col-sm-2">
										<label for="">SOS</label>	
                                        <input type="text" id="Edit_sos" class="form-control" placeholder="Sos">
                                        
                                    </div>
							</div>
							
							<div class="row clearfix">  
                                    <div class="col-sm-3">
                                        
                                    </div>
                                    <div class="col-sm-3">
                                        
                                    </div>

                                    <div class="col-sm-1 treatments-pad-right">
                                        
                                    </div>
                                    <div class="col-sm-1 treatments-pad-right">
										<label for="">AF</label>
                                        <input type="text" id="Edit_af_food" class="form-control border-radius0" placeholder="">
                                        
                                    </div>
                                    <div class="col-sm-1 treatments-pad-right">
										<label for="">BF</label>
                                        <input type="text" id="Edit_bf_food" class="form-control border-radius0" placeholder="">
                                        
                                    </div>
                                    <div class="col-sm-1">

                                        
                                    </div>
							</div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-success btn-round waves-effect" id="UpdateTreatments">Update CHANGES</button>
                <button type="button" class="btn btn-danger waves-effect" data-dismiss="modal">CLOSE</button>
            </div>
        </div>
    </div>
</div>
