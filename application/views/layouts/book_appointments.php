<section class="content">
    <div class="block-header">
        <div class="row">
            <div class="col-lg-7 col-md-6 col-sm-12">
                <h2>Book Appointment
                <!-- <small>Welcome to Oreo</small> -->
                </h2>
            </div>
            <div class="col-lg-5 col-md-6 col-sm-12">
                <ul class="breadcrumb float-md-right">
                    <li class="breadcrumb-item"><a href="index-2.html"><i class="zmdi zmdi-home"></i> Dr. Rajesh</a></li>
                    <li class="breadcrumb-item"><a href="javascript:void(0);">Appointment</a></li>
                    <li class="breadcrumb-item active">Book Appointment</li>
                </ul>
            </div>
        </div>
    </div>    
    <div class="container-fluid">        
        <div class="row clearfix">
			<div class="col-lg-12 col-md-12 col-sm-12">
				<div class="card">
                <div class="row">
                    <div class="col-md-6">
                        <div class="header">
                        <h2><strong>Book</strong> Appointment<small>Description text here...</small> </h2>
                        </div>
                    </div>
                    <div class="col-md-6 ">
                        <button type="button" data-color="light-blue" class="btn bg-light-blue waves-effect pull-right mar10 BookAppointments">Book Appointments</button>
                    </div>
                </div>
					<div class="body">
                        <div class="row clearfix">
                            <div class="col-sm-6">
                                <div class="form-group">
                                    <input class="typeahead form-control" id="name" onchange="CheckpatientName()" type="text" placeholder="Patient Name" autocomplete="off">
                                </div>
                            </div>
                            <div class="col-sm-6">
                                <div class="input-group">
                                    <span class="input-group-addon">
                                        <i class="zmdi zmdi-calendar"></i>
                                    </span>
                                    <input type="text" id="appointment" class="datetimepicker form-control" placeholder="Please choose appointment date...">
                                </div>
                            </div>
                        </div>
                        <br/>
                        <div class="row clearfix">
                            <div class="col-sm-4">
                                <div class="form-group">
                                    <h6>Id: <span id="id"></span></h6>    
                                    <h6>Patient Id: <span id="unique_id"></span></h6>
                                </div>
                            </div>
                            <div class="col-sm-4">
                                <div class="form-group">
                                    <h6>Name: <span id="patient_name"></span></h6>
                                </div>
                            </div>
                            <div class="col-sm-4">
                                <div class="form-group">
                                    <h6 id="">Age: <span id="age"></span></h6>
                                </div>
                            </div>
                        </div>
                        <br/>
                        <div class="row clearfix">
                            <div class="col-sm-4">
                                <div class="form-group">
                                    <h6 id="">Address: <span id="address"></span></h6>
                                </div>
                            </div>
                            <div class="col-sm-4">
                                <div class="form-group">
                                    <h6 id="">Gender: <span id="gender"></span></h6>
                                </div>
                            </div>
                            <div class="col-sm-4">
                                <div class="form-group">
                                    <h6 id="">Blood Group: <span id="blood"></span></h6>
                                </div>
                            </div>
                        </div>
                        <br/>
                        <div class="row clearfix">
                            <div class="col-sm-4">
                                <div class="form-group">
                                    <h6 id="">Mobile: <span id="mobile"></span></h6>
                                </div>
                            </div>
                            <!-- <div class="col-sm-4">
                                <div class="form-group">
                                    <h6>Address: </h6>
                                </div>
                            </div> -->
                        </div>
                    </div>
				</div>
			</div>
		</div>
    </div>
</section>

