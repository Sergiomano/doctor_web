<section class="content">
	<div class="block-header">
		<div class="row">
			<div class="col-lg-7 col-md-6 col-sm-12">
				<h2>My Profile
                
					<!-- <small class="text-muted">Welcome to Oreo</small> -->
				</h2>
			</div>
			<div class="col-lg-5 col-md-6 col-sm-12">
				<button class="btn btn-primary btn-icon btn-round hidden-sm-down float-right m-l-10" type="button">
					<i class="zmdi zmdi-plus"></i>
				</button>
				<ul class="breadcrumb float-md-right">
					<li class="breadcrumb-item">
						<a href="index-2.html">
							<i class="zmdi zmdi-home"></i> Dr. Rajesh
						</a>
					</li>
					<li class="breadcrumb-item">
						<a href="javascript:void(0);">My Profile</a>
					</li>
					<li class="breadcrumb-item active">My Profile</li>
				</ul>
			</div>
		</div>
	</div>
	<div class="container-fluid">
		<div class="row clearfix">
			<div class="col-md-12">
				<div class="card patients-list">
					<div class="row">
						<div class="col-md-6">
							<div class="header">
								<h2>
									<strong>My Profile</strong>
								</h2>
							</div>
						</div>
					</div>
					<div class="body">
						<div class="row clearfix">
							<div class="col-sm-12">
                            <?php foreach($GetMyProfileById as $Profile){ ?>
								<div class="form-group">
                                    <input type="hidden" id="id" class="form-control" placeholder="Name" value="<?php echo $Profile["id"] ?>">
									<input type="text" id="name" class="form-control" placeholder="Name" value="<?php echo $Profile["name"] ?>">
									</div>
                                <div class="form-group">
                                    <input type="text" id="designation" class="form-control" placeholder="Designation" value="<?php echo $Profile["designation"] ?>">
                                </div>
                                
                                <div class="form-group">
                                    <input type="text" id="email" class="form-control" placeholder="Email" value="<?php echo $Profile["email"] ?>">
                                </div>
                                <div class="form-group">
                                    <input type="text" id="phone" class="form-control" placeholder="Phone" value="<?php echo $Profile["phone"] ?>">
                                </div>
                                <div class="form-group">
                                    <input type="text" id="website" class="form-control" placeholder="Website" value="<?php echo $Profile["website"] ?>">
                                </div>
                                <div class="form-group">
                                    <textarea class="form-control" id="address" placeholder="Address"><?php echo $Profile["address"] ?></textarea>
                                </div>
                                
                                <div class="col-sm-12">
                                <button type="submit" class="btn btn-primary btn-round UpdateProfile">Submit</button>
                            </div>            
                            <?php }?>
                                </div>
                            </div>
                        </div>
                        <!-- <div class="row clearfix">
                            <div class="col-md-12">
                                <div class="card">
                                    <div class="header">
                                        <h2>
                                            <strong>Social Accounts</strong> Information  
                                        </h2>
                                    </div>
                                    <div class="body">
                                        <div class="row clearfix">
                                            <div class="col-sm-6">
                                                <div class="form-group">
                                                    <input type="text" id="name" class="form-control" placeholder="Facebook">
                                                    </div>
                                                </div>
                                                <div class="col-sm-6">
                                                    <div class="form-group">
                                                        <input type="text" id="name" class="form-control" placeholder="Twitter">
                                                        </div>
                                                    </div>
                                                    <div class="col-sm-6">
                                                        <div class="form-group">
                                                            <input type="text" id="name" class="form-control" placeholder="Youtube">
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div> -->
                    </section>



<section class="content">
    <div class="container-fluid">
        <div class="row clearfix">
            <div class="col-md-12">
                <div class="card patients-list">
                <div class="row">
                    <div class="col-md-6">
                        <div class="header">
                            <h2><strong>Patients Tips</strong> List</h2>
                        </div>
                    </div>
                    <div class="col-md-6 ">
                        <button type="button" data-color="light-blue" class="btn bg-light-blue waves-effect pull-right mar10" data-toggle="modal" data-target="#AddTips">Add Details</button>
                    </div>
                </div>

                    <div class="body">
                            
                        <!-- Tab panes -->
                        <div class="tab-content m-t-10">
                            <div class="tab-pane table-responsive active" id="All">
                                <table class="table m-b-0 table-hover">
                                    <thead>
                                        <tr>                                       
                                            <th>Name</th>
                                            <th>Description</th>
                                            <!-- <th>Status</th> -->
                                            <th>Action</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <?php foreach($GetTips as $Tips){ ?>
                                        <tr>
                                            <td><?php echo $Tips["name"] ?></td>
                                            <td><?php echo $Tips["details"] ?></td>
                                            <!-- <td><span class="badge badge-success">Active</span></td> -->
                                            <td><a href="javascrip:void;" onclick="EditTips(<?php echo $Tips["id"] ?>)"><i class="zmdi zmdi-edit"></i></a>&nbsp; &nbsp;<a href="javascript:void(0);" onclick="DeleteTips(<?php echo $Tips["id"] ?>);"><i class="zmdi zmdi-delete"></i></a></td>
                                        </tr>
                                        <?php 
                                        }
                                        ?>
                                    </tbody>
                                </table>                            
                            </div>
                        </div>
                    </div>
                   
                </div>
            </div>
        </div>
    </div>
</section>

<div class="modal fade" id="AddTips" tabindex="-1" role="dialog">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="title" id="largeModalLabel">Add Tips</h4>
            </div>
            <div class="modal-body"> 
                <div class="row clearfix">
                    <div class="col-sm-12">
                        <div class="form-group">
                            <input type="text" id="tipsname" class="form-control" placeholder="Tips Name">
                        </div>
                        
                        <div class="form-group">
                            <label>Description</label>
                            <textarea class="form-control" id="description" name="description" placeholder="Address"></textarea>
                        </div>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-success btn-round waves-effect AddTips">SAVE CHANGES</button>
                <button type="button" class="btn btn-danger waves-effect" data-dismiss="modal">CLOSE</button>
            </div>
        </div>
    </div>
</div>

<div class="modal fade" id="EditTips" tabindex="-1" role="dialog">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="title" id="largeModalLabel">Edit Tips</h4>
            </div>
            <div class="modal-body"> 
                <div class="row clearfix">
                    <div class="col-sm-12">
                    <div class="form-group">
                        <input type="hidden" id="id" class="form-control" placeholder="Employee Name">       
                        <input type="text" id="Editname" class="form-control" placeholder="Tips Name">
                    </div>
                        
                        <div class="form-group">
                            <textarea class="form-control" id="Editdescription" placeholder="Description"></textarea>
                        </div>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-success btn-round waves-effect" id="UpdateTips">Update CHANGES</button>
                <button type="button" class="btn btn-danger waves-effect" data-dismiss="modal">CLOSE</button>
            </div>
        </div>
    </div>
</div>