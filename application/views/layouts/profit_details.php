<section class="content">
    <div class="block-header">
        <div class="row">
            <div class="col-lg-7 col-md-6 col-sm-12">
                <h2>All Profit
                <!-- <small class="text-muted">Welcome to Oreo</small> -->
                </h2>
            </div>
            <div class="col-lg-5 col-md-6 col-sm-12">
                <button class="btn btn-primary btn-icon btn-round hidden-sm-down float-right m-l-10" type="button">
                    <i class="zmdi zmdi-plus"></i>
                </button>
                <ul class="breadcrumb float-md-right">
                    <li class="breadcrumb-item"><a href="index-2.html"><i class="zmdi zmdi-home"></i> Dr. Rajesh</a></li>
                    <li class="breadcrumb-item"><a href="javascript:void(0);">Profit</a></li>
                    <li class="breadcrumb-item active">All Profit</li>
                </ul>
            </div>
        </div>
    </div>
    <div class="container-fluid">
        <div class="row clearfix">
            <div class="col-md-12">
                <div class="card patients-list">
                <div class="row">
                    <div class="col-md-6">
                    <br/>
                        <select class="form-control show-tick" id="location" onchange="GetProfitDetailsBylocation()">
                                <option value="">Select Location</option>
                                <?php foreach($GetLocation as $Location){ ?>
                                    <option value="<?php echo $Location["id"]; ?>"><?php echo $Location["location"]; ?></option>
                                <?php } ?>
                            </select>
                            <br/>
                        <div class="header">
                            
                            
                            <h2><strong>Profit</strong> List</h2>
                        </div>
                    </div>
                </div>

                    <div class="body">
                            
                        <!-- Tab panes -->
                        <div class="tab-content m-t-10">
                            <div class="tab-pane table-responsive active" id="All">
                                <table class="table m-b-0 table-hover">
                                    <thead>
                                        <tr>                                       
                                            <th>Location Name</th>
                                            <th>Total Patients</th>
                                            <th>Payments</th>
                                        </tr>
                                    </thead>
                                    <tbody id="ProfitDetails">
                                        <?php foreach($GetProfitDetails as $Payments){ ?>
                                        <tr>
                                            <td><?php echo $Payments["LocationName"] ?></td>
                                            <td><?php echo $Payments["patientscount"] ?></td>
                                            <td><?php echo $Payments["Amount"] ?></td>
                                        </tr>
                                        <?php 
                                        }
                                        ?>
                                    </tbody>
                                </table>                            
                            </div>
                        </div>
                    </div>
                   
                </div>
            </div>
        </div>
    </div>
</section>

<div class="modal fade" id="AddCompliants" tabindex="-1" role="dialog">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="title" id="largeModalLabel">Add Payments</h4>
            </div>
            <div class="modal-body"> 
                <div class="row clearfix">
                    <div class="col-sm-12">
                        <div class="form-group">
                            <input type="text" id="location" class="form-control" placeholder="Location Name">
                        </div>
                        
                        <div class="form-group">
                            <input type="text" id="amount" class="form-control" placeholder="Payments">
                        </div>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-success btn-round waves-effect AddPayments">SAVE CHANGES</button>
                <button type="button" class="btn btn-danger waves-effect" data-dismiss="modal">CLOSE</button>
            </div>
        </div>
    </div>
</div>

<div class="modal fade" id="EditCompliants" tabindex="-1" role="dialog">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="title" id="largeModalLabel">Edit Payments</h4>
            </div>
            <div class="modal-body"> 
                <div class="row clearfix">
                    <div class="col-sm-12">
                    <div class="form-group">
                        <input type="hidden" id="id" class="form-control" placeholder="Employee Name">  
                    </div>
                        
                    <div class="form-group">
                            <input type="text" id="Editlocation" class="form-control" placeholder="Location Name">
                        </div>
                        
                        <div class="form-group">
                            <input type="text" id="Editamount" class="form-control" placeholder="Payments">
                        </div>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-success btn-round waves-effect UpdatePayments" id="">Update CHANGES</button>
                <button type="button" class="btn btn-danger waves-effect" data-dismiss="modal">CLOSE</button>
            </div>
        </div>
    </div>
</div>