<section class="content">
    <div class="block-header">
        <div class="row">
            <div class="col-lg-7 col-md-6 col-sm-12">
                <h2>Edit Patient
                </h2>
            </div>
            <div class="col-lg-5 col-md-6 col-sm-12">               
                <ul class="breadcrumb float-md-right">
                    <li class="breadcrumb-item"><a href="index-2.html"><i class="zmdi zmdi-home"></i> Oreo</a></li>
                    <li class="breadcrumb-item"><a href="javascript:void(0);">Patient</a></li>
                    <li class="breadcrumb-item active">Edit Patient</li>
                </ul>
            </div>
        </div>
    </div>
    <div class="container-fluid">
    <?php foreach($GetPatientsById as $Patients){ ?>
        <div class="row clearfix">
            <div class="col-lg-12 col-md-12 col-sm-12">
                <div class="card">
                    <div class="header">
                        <h2><strong>Basic</strong> Information <small>Description text here...</small> </h2>
                    </div>
                    <div class="body">
                        <div class="row clearfix">
                            <div class="col-sm-6">
                                <div class="form-group">
                                    <input type="hidden" id="id" class="form-control" placeholder="Name" value="<?php echo $Patients["id"] ?>">
                                    <input type="text" id="fname" class="form-control" placeholder="Name" value="<?php echo $Patients["name"] ?>">
                                </div>
                            </div>
                            <!-- <div class="col-sm-4">
                                <div class="form-group">
                                    <input type="text" class="form-control" id="lname" placeholder="Last Name" value="<?php echo $Patients["name"] ?>">
                                </div>
                            </div> -->
                            <div class="col-sm-6">
                                <div class="form-group">
                                    <input type="text" class="form-control" id="mobile" placeholder="Phone No." value="<?php echo $Patients["mobile"] ?>">
                                    <input type="checkbox"> Same As Whatsapp Number
                                </div>
                            </div>
                        </div>
                        <div class="row clearfix">  
                            <div class="col-sm-4">
                                <div class="form-group">
                                    <input type="text" id="whatsapp_no" class="form-control" placeholder="Whatsapp number" value="<?php echo $Patients["whatsapp_no"] ?>">
                                </div>
                            </div>  
                            <div class="col-sm-4">
                                <div class="form-group">
                                    <input type="text" id="age" class="form-control" placeholder="Age" value="<?php echo $Patients["age"] ?>">
                                </div>
                            </div>
                            <div class="col-sm-4">
                                <select class="form-control show-tick" id="gender">
                                    <option value="">Select Gender</option>
                                    <option value="10">Male</option>
                                    <option value="20">Female</option>
                                </select>
                            </div>                            
                            
                        </div>
                        <div class="row clearfix"> 
                            <div class="col-sm-6">
                                <div class="form-group">
                                    <input type="text" class="form-control" id="blood" placeholder="Enter blood group" value="<?php echo $Patients["blood_group"] ?>">
                                </div>
                            </div>                           
                            <div class="col-sm-6">
                                <div class="input-group">
                                    <span class="input-group-addon">
                                        <i class="zmdi zmdi-calendar"></i>
                                    </span>
                                    <input type="text" id="dob" class="datetimepicker form-control" placeholder="Please choose date of birth" value="<?php echo $Patients["date_of_birth"] ?>">
                                </div>                               
                            </div>

                            <!-- <div class="col-sm-4">
                                <div class="input-group">
                                    <span class="input-group-addon">
                                        <i class="zmdi zmdi-calendar"></i>
                                    </span>
                                    <input type="text" id="appointment" class="datetimepicker form-control" placeholder="Please choose appointment date">
                                </div>                               
                            </div>     -->
                        </div>
                        <div class="row clearfix">
            <div class="col-md-12">
                <div class="card">
                    <div class="header">
                        <h2><strong>Compliants</strong> Information  </h2>
                    </div>
                    <div class="body">
                        <div class="row clearfix">
                            <div class="col-sm-6">
                                <select class="form-control show-tick" onchange="CheckCompliants()" id="compliant_name">
                                    <option selected>Select Compliants -</option>
                                    <?php foreach($GetCompliants as $Compliants){ ?>
                                        <option value="<?php echo $Compliants["id"]; ?>"><?php echo $Compliants["compliant_name"]; ?></option>
                                    <?php 
                                    }
                                    ?>
                                </select>
                            </div> 
                            
                            <div class="col-sm-6">
                                <p id="Description">Description</p>
                            </div> 
                        </div>
                    </div>
                </div>
            </div>
        </div>
                        <div class="row clearfix">                            
                            <div class="col-sm-6">
                                <select class="form-control show-tick" id="examination">
                                    <option value="">Select Examination </option>
                                    <option value="10">Examination 1</option>
                                    <option value="20">Examination 2</option>
                                </select>
                            </div>  
                            
                            <div class="col-sm-6">
                                <div class="form-group">
                                    <input type="text" id="diagnotics" class="form-control" placeholder="Enter Diagnotics" value="<?php echo $Patients["diagnotics"] ?>">
                                </div>
                            </div>                           
                            <!-- <div class="col-sm-3">
                                <div class="form-group">
                                    <input type="text" class="form-control" placeholder="Enter blood group">
                                </div>
                            </div> -->
                        </div>
                        <!-- <div class="row clearfix">                            
                                <div class="col-sm-6">
                                    <div class="form-group">
                                        <label>Investigation</label>
                                        <input type="file" id="investigation" class="form-control" placeholder="Investigation">
                                    </div>
                                </div> 
                                <div class="col-sm-6">

                                </div>
                                
                                <div class="col-sm-6">
                                    <div class="form-group">
                                        <label>Treatment</label>
                                        <input type="file" id="treatment" class="form-control" placeholder="Diagnotics">
                                    </div>
                                </div>              
                                <div class="col-sm-6">

                                </div>             
                                
                            </div> -->
                        <!-- <div class="row clearfix">
                            <div class="col-lg-12 col-md-12 col-sm-12">
                                <form action="https://thememakker.com/" id="frmFileUpload" class="dropzone" method="post" enctype="multipart/form-data">
                                    <div class="dz-message">
                                        <div class="drag-icon-cph"> <i class="material-icons">touch_app</i> </div>
                                        <h3>Drop files here or click to upload.</h3>
                                        <em>(This is just a demo dropzone. Selected files are <strong>not</strong> actually uploaded.)</em> </div>
                                    <div class="fallback">
                                        <input name="file" type="file" multiple />
                                    </div>
                                </form>
                            </div>
                        </div> -->
                        <div class="row clearfix">   
                            <div class="col-sm-12">
                                <div class="form-group">
                                    <textarea rows="4" class="form-control no-resize" placeholder="Address" id="address"><?php echo $Patients["address"] ?></textarea>
                                </div>
                            </div>                         
                            <!-- <div class="col-sm-12">
                                <div class="form-group">
                                    <textarea rows="4" class="form-control no-resize" placeholder="Description" id="description" ></textarea>
                                </div>
                            </div> -->
                           <div class="col-sm-12">
                                <button type="submit" class="btn btn-primary btn-round UpdatePatients">Submit</button>
                                <button type="submit" class="btn btn-default btn-round btn-simple">Cancel</button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
                                <?php } ?>
</section>