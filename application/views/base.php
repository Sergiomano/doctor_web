<!doctype html>
<html class="no-js " lang="en">

<!-- Mirrored from thememakker.com/templates/oreo/hospital/html/light/ by HTTrack Website Copier/3.x [XR&CO'2014], Thu, 30 May 2019 12:02:26 GMT -->
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=Edge">
<meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
<meta name="description" content="Responsive Bootstrap 4 and web Application ui kit.">
<title>:: Dr. Rajesh :: Home</title>
<link rel="icon" href="favicon.ico" type="image/x-icon"> <!-- Favicon-->
<link rel="stylesheet" href="<?php echo base_url(); ?>public/plugins/bootstrap/css/bootstrap.min.css">
<link rel="stylesheet" href="<?php echo base_url(); ?>public/plugins/jvectormap/jquery-jvectormap-2.0.3.min.css"/>
<link rel="stylesheet" href="<?php echo base_url(); ?>public/plugins/morrisjs/morris.min.css" />

<!-- <link rel="stylesheet" href="//cdnjs.cloudflare.com/ajax/libs/morris.js/0.5.1/morris.css"> -->
<!-- Custom Css -->
<link rel="stylesheet" href="<?php echo base_url(); ?>public/plugins/bootstrap-select/css/bootstrap-select.css"/>
<link rel="stylesheet" href="<?php echo base_url(); ?>public/plugins/sweetalert/sweetalert.css"/>


<link rel="stylesheet" href="<?php echo base_url(); ?>public/assets/css/main.css">
<link rel="stylesheet" href="<?php echo base_url(); ?>public/assets/css/custom.css">
<link rel="stylesheet" href="<?php echo base_url(); ?>public/assets/css/color_skins.css">
<link rel="stylesheet" href="//cdn.datatables.net/1.10.19/css/jquery.dataTables.min.css">

<link href="<?php echo base_url(); ?>public/plugins/bootstrap-material-datetimepicker/css/bootstrap-material-datetimepicker.css" rel="stylesheet" />

</head>
<body class="theme-cyan">
<!-- Page Loader -->
<div class="page-loader-wrapper">
    <div class="loader">
        <div class="m-t-30"><img class="zmdi-hc-spin" src="https://thememakker.com/templates/oreo/hospital/html/images/logo.svg" width="48" height="48" alt="Oreo"></div>
        <p>Please wait...</p>        
    </div>
</div>
<!-- Overlay For Sidebars -->
<?php $this->load->view("common/sidebar"); ?>
<!-- Top Bar -->
<?php $this->load->view("common/top_bar"); ?>
<!-- Left Sidebar -->
<?php $this->load->view("common/left_sidebar"); ?>
<!-- Right Sidebar -->
<?php $this->load->view("common/right_sidebar"); ?>
<!-- Chat-launcher -->
<!-- <div class="chat-launcher"><i class="material-icons">camera_alt</i></div> -->
<div class="chat-wrapper">
    <div class="card">
        <div class="header">
            <ul class="list-unstyled team-info margin-0">
                <li class="m-r-15"><h2>Dr. Team</h2></li>
                <li>
                    <img src="../images/xs/avatar2.jpg" alt="Avatar">
                </li>
                <li>
                    <img src="../images/xs/avatar3.jpg" alt="Avatar">
                </li>
                <li>
                    <img src="../images/xs/avatar4.jpg" alt="Avatar">
                </li>
                <li>
                    <img src="../images/xs/avatar6.jpg" alt="Avatar">
                </li>
                <li>
                    <a href="javascript:void(0);" title="Add Member"><i class="zmdi zmdi-plus-circle"></i></a>
                </li>
            </ul>                       
        </div>
        <div class="body">
            <div class="chat-widget">
            <ul class="chat-scroll-list clearfix">
                <li class="left float-left">
                    <img src="../images/xs/avatar3.jpg" class="rounded-circle" alt="">
                    <div class="chat-info">
                        <a class="name" href="#">Alexander</a>
                        <span class="datetime">6:12</span>                            
                        <span class="message">Hello, John </span>
                    </div>
                </li>
                <li class="right">
                    <div class="chat-info"><span class="datetime">6:15</span> <span class="message">Hi, Alexander<br> How are you!</span> </div>
                </li>
                <li class="right">
                    <div class="chat-info"><span class="datetime">6:16</span> <span class="message">There are many variations of passages of Lorem Ipsum available</span> </div>
                </li>
                <li class="left float-left"> <img src="../images/xs/avatar2.jpg" class="rounded-circle" alt="">
                    <div class="chat-info"> <a class="name" href="#">Elizabeth</a> <span class="datetime">6:25</span> <span class="message">Hi, Alexander,<br> John <br> What are you doing?</span> </div>
                </li>
                <li class="left float-left"> <img src="../images/xs/avatar1.jpg" class="rounded-circle" alt="">
                    <div class="chat-info"> <a class="name" href="#">Michael</a> <span class="datetime">6:28</span> <span class="message">I would love to join the team.</span> </div>
                </li>
                    <li class="right">
                    <div class="chat-info"><span class="datetime">7:02</span> <span class="message">Hello, <br>Michael</span> </div>
                </li>
            </ul>
            </div>
            <div class="input-group p-t-15">
                <input type="text" class="form-control" placeholder="Enter text here...">
                <span class="input-group-addon">
                    <i class="zmdi zmdi-mail-send"></i>
                </span>
            </div>
        </div>
    </div>
</div>
<!-- Main Content -->
<?php $this->load->view('layouts/'.$_contents); ?>
<!-- Jquery Core Js --> 
<script src="<?php echo base_url(); ?>public/assets/bundles/libscripts.bundle.js"></script> 

<!-- Lib Scripts Plugin Js ( jquery.v3.2.1, Bootstrap4 js) --> 
<script src="<?php echo base_url(); ?>public/assets/bundles/vendorscripts.bundle.js"></script> <!-- slimscroll, waves Scripts Plugin Js -->


<script src="<?php echo base_url(); ?>public/assets/bundles/morrisscripts.bundle.js"></script>
<!-- Morris Plugin Js -->
<script src="<?php echo base_url(); ?>public/assets/bundles/jvectormap.bundle.js"></script> <!-- JVectorMap Plugin Js -->
<script src="<?php echo base_url(); ?>public/assets/bundles/knob.bundle.js"></script> <!-- Jquery Knob, Count To, Sparkline Js -->

<script src="<?php echo base_url(); ?>public/assets/bundles/mainscripts.bundle.js"></script>
<script src="<?php echo base_url(); ?>public/assets/js/pages/index.js"></script>
<script src="<?php echo base_url(); ?>public/plugins/momentjs/moment.js"></script> <!-- Moment Plugin Js -->
<script src="<?php echo base_url(); ?>public/plugins/bootstrap-material-datetimepicker/js/bootstrap-material-datetimepicker.js"></script>

<script src="<?php echo base_url(); ?>public/plugins/sweetalert/sweetalert.min.js"></script>

<script src="<?php echo base_url(); ?>public/assets/js/custom/admin.js"></script> 
<script src="<?php echo base_url(); ?>public/assets/js/custom/superadmin.js"></script> 
<script src="<?php echo base_url(); ?>public/assets/js/custom/patients.js"></script> 
<script src="<?php echo base_url(); ?>public/assets/js/custom/checkups.js"></script>
<script src="<?php echo base_url(); ?>public/assets/js/custom/patientsinfo.js"></script> 
<script src="<?php echo base_url(); ?>public/assets/js/custom/payments.js"></script> 

<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-3-typeahead/4.0.1/bootstrap3-typeahead.min.js"></script>  

<!-- <script src="//cdn.datatables.net/1.10.19/js/jquery.dataTables.min.js"></script> -->
<script src="//cdn.ckeditor.com/4.11.4/basic/ckeditor.js"></script>

<script type="text/javascript">
//     $('input.typeahead').typeahead({
//     source:  function (query, process) {
//     return $.get('/Appointments/ajaxpro', { query: query }, function (data) {
//             console.log(data);
//             data = $.parseJSON(data);
//             return process(data);
            
//         });
//     }
// });
$('input.typeahead').typeahead({
    source:  function (query, process) {
    return $.get('/Patient_Info/GetComplaintName', { query: query }, function (data) {
            console.log(data);
            data = $.parseJSON(data);
            return process(data);
            
        });
    }
});

CKEDITOR.replace( 'description');
CKEDITOR.add
CKEDITOR.replace( 'Editdescription');
CKEDITOR.add
CKEDITOR.replace( 'Editexaminationdescription');
CKEDITOR.add
CKEDITOR.replace( 'EditSurgerydescription');
CKEDITOR.add

// </script>

// <script>
CKEDITOR.replace( 'examination_description');
CKEDITOR.add

CKEDITOR.replace( 'investigation_description');
CKEDITOR.add

CKEDITOR.replace( 'treatments_description');
CKEDITOR.add

CKEDITOR.replace( 'daignotics_description');
CKEDITOR.add

CKEDITOR.replace( 'surgery_description');
CKEDITOR.add
</script>

<script>
    $(function () {
    //Datetimepicker plugin
    $('.datetimepicker').bootstrapMaterialDatePicker({
        format: 'YYYY-MM-DD',
        clearButton: true,
        weekStart: 1
    });
});

</script>


</body>

<!-- Mirrored from thememakker.com/templates/oreo/hospital/html/light/ by HTTrack Website Copier/3.x [XR&CO'2014], Thu, 30 May 2019 12:02:53 GMT -->
</html>