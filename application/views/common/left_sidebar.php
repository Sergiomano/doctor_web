<aside id="leftsidebar" class="sidebar">
    <ul class="nav nav-tabs">
        <li class="nav-item"><a class="nav-link active" data-toggle="tab" href="#dashboard"><i class="zmdi zmdi-home m-r-5"></i>Admin</a></li>
        <li class="nav-item"><a class="nav-link" data-toggle="tab" href="#user">Docter</a></li>
    </ul>
    <div class="tab-content">
        <div class="tab-pane stretchRight active" id="dashboard">
            <div class="menu">
            <?php if($_SESSION["LoginType"] == 1){ ?>
                <ul class="list">
                    <!-- <li>
                        <div class="user-info">
                            <div class="image"><a href="profile.html"><img src="../images/profile_av.jpg" alt="User"></a></div>
                            <div class="detail">
                                <h4>Dr. Rajesh</h4>
                                <small>Neurologist</small>                        
                            </div>
                        </div>
                    </li> -->
                    <li class="header">MAIN</li>
                    <li class="active open"><a href="<?php echo base_url(); ?>Admin"><i class="zmdi zmdi-home"></i><span>Dashboard</span></a></li>  
                    
                   
                    <li><a href="<?php echo base_url(); ?>Super_admin/all_patients"><i class="zmdi zmdi-calendar-check"></i><span>Patients</span> </a></li>
                    <!-- <li><a href="javascript:void(0);" class="menu-toggle"><i class="zmdi zmdi-balance-wallet"></i><span>Payments</span> </a>
                        <ul class="ml-menu">
                            <li><a href="payments.html">Payments</a></li>
                            <li><a href="add-payments.html">Add Payment</a></li>
                            <li><a href="invoice.html">Invoice</a></li>
                        </ul>
                    </li> -->
                </ul>
                <?php }else if($_SESSION["LoginType"] == 2){ ?>
                    <ul class="list">
                    <!-- <li>
                        <div class="user-info">
                            <div class="image"><a href="profile.html"><img src="../images/profile_av.jpg" alt="User"></a></div>
                            <div class="detail">
                                <h4>Patient</h4>
                            </div>
                        </div>
                    </li> -->
                    <li class="header">MAIN</li>
                    <li class="active open"><a href="<?php echo base_url(); ?>Patients"><i class="zmdi zmdi-home"></i><span>Dashboard</span></a></li>            
                    <li><a href="<?php echo base_url(); ?>Patients/CheckAppointments"><i class="zmdi zmdi-calendar-check"></i><span>Check Appointment</span> </a></li>
                    <!-- <li><a href="javascript:void(0);" class="menu-toggle"><i class="zmdi zmdi-account-add"></i><span>Doctors</span> </a>
                        <ul class="ml-menu">
                            <li><a href="doctors.html">All Doctors</a></li>
                            <li><a href="add-doctor.html">Add Doctor</a></li>                       
                            <li><a href="profile.html">Doctor Profile</a></li>
                            <li><a href="events.html">Doctor Schedule</a></li>
                        </ul>
                    </li> -->

                    <!-- <li><a href="#"><i class="zmdi zmdi-calendar-check"></i><span>Chat</span> </a></li> -->
                    <li><a href="<?php echo base_url(); ?>Patients/CheckCheckups"><i class="zmdi zmdi-calendar-check"></i><span>Checkup Complaints</span> </a></li>
                    <!-- <li><a href="#"><i class="zmdi zmdi-calendar-check"></i><span>My Profile</span> </a></li> -->
                </ul>
            
                <?php }else{ ?>
                <ul class="list">
                    <!-- <li>
                        <div class="user-info">
                            <div class="image"><a href="profile.html"><img src="../images/profile_av.jpg" alt="User"></a></div>
                            <div class="detail">
                                <h4>Dr. Rajesh</h4>
                                <small>Neurologist</small>                        
                            </div>
                        </div>
                    </li> -->
                    <li class="header">MAIN</li>
                    <li class="active open"><a href="<?php echo base_url(); ?>Super_admin/"><i class="zmdi zmdi-home"></i><span>Dashboard</span></a></li>  
                    <li><a href="<?php echo base_url(); ?>Super_admin/all_patients"><i class="zmdi zmdi-calendar-check"></i><span>Patients</span> </a></li>

                    <!-- <li><a href="<?php echo base_url(); ?>Checkups"><i class="zmdi zmdi-calendar-check"></i><span>Checkups Details</span> </a></li>

                    <li><a href="<?php echo base_url(); ?>Checkups/all_checkups"><i class="zmdi zmdi-calendar-check"></i><span>Modify Checkups</span> </a></li> -->

                    <li><a href="<?php echo base_url(); ?>Admin/all_admin" ><i class="zmdi zmdi-account-o"></i><span>Admin</span> </a>
                    </li>

                   
                    <li><a href="<?php echo base_url(); ?>Super_admin/all_compliants"><i class="zmdi zmdi-calendar-check"></i><span>Complaints</span> </a></li>
                    <li><a href="<?php echo base_url(); ?>Patient_info/all_examination"><i class="zmdi zmdi-calendar-check"></i><span>Examination</span> </a></li>
                    <!-- <li><a href="<?php echo base_url(); ?>Super_admin/all_compliants"><i class="zmdi zmdi-calendar-check"></i><span>Diagnotics</span> </a></li> -->
                    <li><a href="<?php echo base_url(); ?>Patient_info/all_investigation"><i class="zmdi zmdi-calendar-check"></i><span>Investigation</span> </a></li>
                    <li><a href="<?php echo base_url(); ?>Patient_info/all_treatments"><i class="zmdi zmdi-calendar-check"></i><span>Treatments</span> </a></li>

                    <li><a href="<?php echo base_url(); ?>Patient_info/all_diagnotics"><i class="zmdi zmdi-calendar-check"></i><span>Diagnosis</span> </a></li>

                    <!-- <li><a href="javascript:void(0);" class="menu-toggle"><i class="zmdi zmdi-balance-wallet"></i><span>Payments</span> </a>
                        <ul class="ml-menu">
                            <li><a href="payments.html">Payments</a></li>
                            <li><a href="add-payments.html">Add Payment</a></li>
                            <li><a href="invoice.html">Invoice</a></li>
                        </ul>
                    </li> -->
                    <li class="header">EXTRA DETAILS</li>
                    <li><a href="<?php echo base_url(); ?>Payments"><i class="zmdi zmdi-calendar-check"></i><span>Payment Details</span> </a></li>

                    <li><a href="<?php echo base_url(); ?>Payments/Profit_details"><i class="zmdi zmdi-calendar-check"></i><span>Profit Details</span> </a></li>

                    <?php foreach($GetMyProfile as $Profile){ ?>
                        <li><a href="<?php echo base_url(); ?>Super_admin/my_profile/<?php echo $Profile["id"]; ?>"><i class="zmdi zmdi-account-o"></i><span>My Profile</span> </a></li>
                    <?php } ?>
                    <?php
                    }
                    ?>
                </ul>
            </div>
        </div>
        <div class="tab-pane stretchLeft" id="user">
            <div class="menu">
                <ul class="list">
                <?php foreach($GetMyProfile as $Profile){ ?>
                    <li>
                        <div class="user-info m-b-20 p-b-15">
                            <div class="image"><a href="#"><img src="<?php echo base_url(); ?>public/images/profile_av.jpg" alt="User"></a></div>
                            <div class="detail">
                                <h4><?php echo $Profile["name"]; ?></h4>
                                <small><?php echo $Profile["designation"]; ?></small>                        
                            </div>
                            <!-- <div class="row">
                                <div class="col-12">
                                    <a title="facebook" href="#"><i class="zmdi zmdi-facebook"></i></a>
                                    <a title="twitter" href="#"><i class="zmdi zmdi-twitter"></i></a>
                                    <a title="instagram" href="#"><i class="zmdi zmdi-instagram"></i></a>
                                </div>                              
                            </div> -->
                        </div>
                    </li>
                    <li>
                        <small class="text-muted">Location: </small>
                        <p><?php echo $Profile["address"]; ?></p>
                        <hr>
                        <small class="text-muted">Email address: </small>
                        <p><?php echo $Profile["email"]; ?></p>
                        <hr>
                        <small class="text-muted">Phone: </small>
                        <p><?php echo $Profile["phone"]; ?></p>
                        <hr>
                        <small class="text-muted">Website: </small>
                        <p><a href="<?php echo $Profile["website"]; ?>" target="_blank"><?php echo $Profile["website"]; ?></a> </p>
                        <hr>                
                    </li>    
                <?php } ?>                
                </ul>
            </div>
        </div>
    </div>    
</aside>
