<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Super_admin extends CI_Controller {
	public function __construct(){
        parent::__construct();
		$this->load->model('AdminModel');
		$this->load->model('SuperAdminModel');
		$this->load->model('PatientsInfo_model');
		
    }
	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */
	public function index()
	{
		$data["_contents"] = "dashboard";
		$data["GetMyProfile"] = $this->SuperAdminModel->GetMyProfile();
		$data["GetPatientsCount"] = $this->SuperAdminModel->GetPatientsCount();
		$data["GetAdmins"] = $this->AdminModel->GetAdmin();
		$data["GetPatientsLast30Days"] = $this->SuperAdminModel->GetPatientsLast30Days();
		$data["GetPatientsLast7Days"] = $this->SuperAdminModel->GetPatientsLast7Days();
		$data["GetTotalRevenue"] = $this->SuperAdminModel->GetTotalRevenue();
		$data["GetSpinSurgery"] = $this->PatientsInfo_model->GetSpinSurgery();
		$data["GetBrainSurgery"] = $this->PatientsInfo_model->GetBrainSurgery();
		
		  
		$this->load->view('base', $data);
	}

	public function my_profile($Id)
	{
		$Id = $this->uri->segment(3);
		$data["_contents"] = "myprofile";
		$data["GetMyProfile"] = $this->SuperAdminModel->GetMyProfile();
		$data["GetMyProfileById"] = $this->SuperAdminModel->GetMyProfileById($Id);
		$data["GetTips"] = $this->SuperAdminModel->GetTips();
		$this->load->view('base', $data);
	}

	public function all_compliants()
	{
		$data["_contents"] = "compliants";
		$data["GetMyProfile"] = $this->SuperAdminModel->GetMyProfile();
        $data["GetCompliants"] = $this->SuperAdminModel->GetCompliants();
		$this->load->view('base', $data);
	}

	public function all_patients()
	{
		$data["_contents"] = "all_patients";
		$data["GetMyProfile"] = $this->SuperAdminModel->GetMyProfile();
		$data["GetPatients"] = $this->AdminModel->GetPatients();
		$data["GetLocation"] = $this->SuperAdminModel->GetPayments();
		$this->load->view('base', $data);
	}

	public function add_diagnotics()
	{
		// echo $JoiningDate;
		// exit;
		$data = array(
				"compliant_name"=> $this->input->post("name"),
				"details"=> $this->input->post("description"),
				"type" => 5
		);
		$InsertCompliants = $this->SuperAdminModel->add_compliants($data);
		if($InsertCompliants){
			echo json_encode(array(
				"error" => FALSE,
				"message" => "Diagnotics added successfully"
			));
		}else{
			echo json_encode(array(
				"error" => TRUE,
				"message" => "Failed"
			));
		}
		
	}

	public function add_compliants()
	{
		// echo $JoiningDate;
		// exit;
		$data = array(
				"compliant_name"=> $this->input->post("name"),
				"details"=> $this->input->post("description"),
				"type" => 1
		);
		$InsertCompliants = $this->SuperAdminModel->add_compliants($data);
		if($InsertCompliants){
			echo json_encode(array(
				"error" => FALSE,
				"message" => "Compliants added successfully"
			));
		}else{
			echo json_encode(array(
				"error" => TRUE,
				"message" => "Failed"
			));
		}
		
	}

	public function GetCompliantsbyId($Id)
	{
		// $Id = $this->input->post("id");
        $GetCompliantsbyId = $this->SuperAdminModel->GetCompliantsbyId($Id);
		echo json_encode($GetCompliantsbyId);
	}


	public function GetPatientsInfobyId($Id)
	{
		// $Id = $this->input->post("id");
        $GetPatientsInfobyId = $this->SuperAdminModel->GetPatientsInfobyId($Id);
		echo json_encode($GetPatientsInfobyId);
	}

	public function GetSurgerybyId($Id)
	{
		// $Id = $this->input->post("id");
        $GetSurgerybyId = $this->SuperAdminModel->GetSurgerybyId($Id);
		echo json_encode($GetSurgerybyId);
	}


	public function update_compliants()
	{
		$Id = $this->input->post("id");
		$data = array(
			// "compliant_name"=> $this->input->post("name"),
			"details"=> $this->input->post("description")
		);
	$UpdateCompliants = $this->SuperAdminModel->updatecompliants($Id, $data);
	if($UpdateCompliants){
		echo json_encode(array(
			"error" => FALSE,
			"message" => "Diagnotics updated successfully"
		));
	}else{
		echo json_encode(array(
			"error" => TRUE,
			"message" => "Failed"
		));
	}
		
	}

	public function update_compliantsInfo()
	{
		$Id = $this->input->post("id");
		$data = array(
			// "compliant_name"=> $this->input->post("name"),
			"description"=> $this->input->post("description")
		);
	$UpdateCompliants = $this->SuperAdminModel->updatecompliantsinfo($Id, $data);
	if($UpdateCompliants){
		echo json_encode(array(
			"error" => FALSE,
			"message" => "Diagnotics updated successfully"
		));
	}else{
		echo json_encode(array(
			"error" => TRUE,
			"message" => "Failed"
		));
	}
		
	}


	public function update_surgery()
	{
		$Id = $this->input->post("id");
		$data = array(
			"surgery_name"=> $this->input->post("name"),
			"details"=> $this->input->post("description")
		);
		// echo $Id;
		// exit;
	$UpdateCompliants = $this->SuperAdminModel->updatesurgery($Id, $data);
	if($UpdateCompliants){
		echo json_encode(array(
			"error" => FALSE,
			"message" => "Surgery updated successfully"
		));
	}else{
		echo json_encode(array(
			"error" => TRUE,
			"message" => "Failed"
		));
	}
		
	}


	public function update_profile()
	{
		$Id = $this->input->post("id");
		$data = array(
			"name"=> $this->input->post("name"),
			"designation"=> $this->input->post("designation"),
			"email"=> $this->input->post("email"),
			"phone"=> $this->input->post("phone"),
			"website"=> $this->input->post("website"),
			"address"=> $this->input->post("address")
		);
	$UpdateProfile = $this->SuperAdminModel->updateprofile($Id, $data);
	if($UpdateProfile){
		echo json_encode(array(
			"error" => FALSE,
			"message" => "Profile updated successfully"
		));
	}else{
		echo json_encode(array(
			"error" => TRUE,
			"message" => "Failed"
		));
	}
		
	}


	// Tips

	public function add_tips()
	{
		// echo $JoiningDate;
		// exit;
		$data = array(
				"name"=> $this->input->post("name"),
				"details"=> $this->input->post("description")
		);
		$InsertCompliants = $this->SuperAdminModel->add_tips($data);
		if($InsertCompliants){
			echo json_encode(array(
				"error" => FALSE,
				"message" => "Tips added successfully"
			));
		}else{
			echo json_encode(array(
				"error" => TRUE,
				"message" => "Failed"
			));
		}
		
	}

	public function GetTipsbyId($Id)
	{
		// $Id = $this->input->post("id");
        $GetTipsbyId = $this->SuperAdminModel->GetTipsById($Id);
		echo json_encode($GetTipsbyId);
	}

	public function update_tips()
	{
		$Id = $this->input->post("id");
		$data = array(
			"name"=> $this->input->post("name"),
			"details"=> $this->input->post("description")
		);
	$UpdateCompliants = $this->SuperAdminModel->updatetips($Id, $data);
	if($UpdateCompliants){
		echo json_encode(array(
			"error" => FALSE,
			"message" => "Tips updated successfully"
		));
	}else{
		echo json_encode(array(
			"error" => TRUE,
			"message" => "Failed"
		));
	}
		
	}


	public function update_TreatmentsInfo()
	{
		$Id = $this->input->post("id");
		$data = array(
			"drug"=> $this->input->post("drug"),
			"dose"=> $this->input->post("dose"),
			"morning"=> $this->input->post("morning"),
			"afternoon"=> $this->input->post("afternoon"),
			"evening"=> $this->input->post("evening"),
			"night"=> $this->input->post("night"),
			"after_food"=> $this->input->post("after_food"),
			"before_food"=> $this->input->post("before_food"),
			"sos"=> $this->input->post("sos")
	);
	$UpdateCompliants = $this->SuperAdminModel->updatetreatmentsinfo($Id, $data);
	if($UpdateCompliants){
		echo json_encode(array(
			"error" => FALSE,
			"message" => "Treatments updated successfully"
		));
	}else{
		echo json_encode(array(
			"error" => TRUE,
			"message" => "Failed"
		));
	}
		
	}

	

	// Delete Functions
	public function Delete_Complaints($id){
		$Delete_Complaints = $this->SuperAdminModel->Delete_Complaints($id);
		if($Delete_Complaints){
			echo json_encode(array(
				"error" => FALSE,
				"message" => "Complaints deleted successfully"
			));
		}else{
			echo json_encode(array(
				"error" => TRUE,
				"message" => "Failed"
			));
		}
	}

	public function Delete_ComplaintsInfo($id){
		$Delete_Complaints = $this->SuperAdminModel->Delete_ComplaintsInfo($id);
		if($Delete_Complaints){
			echo json_encode(array(
				"error" => FALSE,
				"message" => "Complaints deleted successfully"
			));
		}else{
			echo json_encode(array(
				"error" => TRUE,
				"message" => "Failed"
			));
		}
	}


	public function Delete_TreatmentsInfo($id){
		$Delete_Complaints = $this->SuperAdminModel->Delete_TreatmentsInfo($id);
		if($Delete_Complaints){
			echo json_encode(array(
				"error" => FALSE,
				"message" => "Treatments deleted successfully"
			));
		}else{
			echo json_encode(array(
				"error" => TRUE,
				"message" => "Failed"
			));
		}
	}

	public function Delete_Surgery($id){
		$Delete_Complaints = $this->SuperAdminModel->Delete_Surgery($id);
		if($Delete_Complaints){
			echo json_encode(array(
				"error" => FALSE,
				"message" => "Surgery deleted successfully"
			));
		}else{
			echo json_encode(array(
				"error" => TRUE,
				"message" => "Failed"
			));
		}
	}

	public function Delete_Tips($id){
		$Delete_Tips = $this->SuperAdminModel->Delete_Tips($id);
		if($Delete_Tips){
			echo json_encode(array(
				"error" => FALSE,
				"message" => "Tips deleted successfully"
			));
		}else{
			echo json_encode(array(
				"error" => TRUE,
				"message" => "Failed"
			));
		}
	}
}
