<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Appointments extends CI_Controller {
    public function __construct(){
        parent::__construct();
		$this->load->model('AdminModel');
        $this->load->model('SuperAdminModel');
        $this->load->model('PatientsModel');
    }
	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */
	public function index()
	{
        $data["_contents"] = "book_appointments";
        $data["GetMyProfile"] = $this->SuperAdminModel->GetMyProfile();
		$this->load->view('base', $data);
    }
    

    public function ajaxPro()
    {
        $query = $this->input->get('query');
        $this->db->where('type', 2);
        $this->db->like('name', $query);


        $data = $this->db->get("patients")->result();


        echo json_encode( $data);
    }

    public function GetPatientsById(){
        $id = $this->input->post("name");

        $GetPatientsById = $this->PatientsModel->GetPatientsByName($id);;
        echo json_encode($GetPatientsById);
    }

    public function CheckAppointmentsByPatientId($id){
        // $id = $this->input->post("name");

        $CheckAppointmentsByPatientId = $this->PatientsModel->CheckAppointmentsByPatientId($id);;
        echo json_encode($CheckAppointmentsByPatientId);
    }
    
    // public function AddAppointments(){
    //     $AppointmentData = array(
    //         "patient_id" => $this->input->post("id"),
    //         "appointment_date" => $this->input->post("appointment")
    //         // "description" => $this->input->post("description")
    //     );
    //     $InsertAppointments = $this->PatientsModel->add_appointments($AppointmentData);
    //     if($InsertAppointments){
    //         echo json_encode(array(
    //             "error" => FALSE,
    //             "message" => "Appointments booked successfully"
    //         ));
    //     }else{
    //         echo json_encode(array(
    //             "error" => TRUE,
    //             "message" => "Failed"
    //         ));
    //     }
       
    // }

    public function book_appointment()
	{
		// echo $this->input->post("description");
        // exit;
        $patientId = $this->input->post("patient_id");
        $AppointmentDate = $this->input->post("appointment_date");
        $CheckAppointments = $this->PatientsModel->CheckAppointments($patientId, $AppointmentDate);

        if(!$CheckAppointments){
            $AppointmentData = array(
				"patient_id"=> $patientId,
                "location_id"=> $this->input->post("location_id"),
                "appointment_date" => $AppointmentDate
            );
            $InsertCompliants = $this->PatientsModel->add_appointments($AppointmentData);
            if($InsertCompliants){
                echo json_encode(array(
                    "error" => FALSE,
                    "message" => "Appointment booked successfully"
                ));
            }else{
                echo json_encode(array(
                    "error" => TRUE,
                    "message" => "Failed"
                ));
            }
        }else{
            echo json_encode(array(
                "error" => TRUE,
                "message" => "Appointment is already booked. In this date"
            ));
        }
		
		
	}
}
