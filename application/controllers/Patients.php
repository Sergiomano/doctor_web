<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Patients extends CI_Controller {
	public function __construct(){
        parent::__construct();
		$this->load->model('AdminModel');
		$this->load->model('SuperAdminModel');
		$this->load->model('PatientsModel');
		$this->load->model('PatientsInfo_model');
    }
	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */
	public function index()
	{
		$data["_contents"] = "patient_dashboard";
		$data["GetMyProfile"] = $this->SuperAdminModel->GetMyProfile();
		// $data["GetCompliants"] = $this->SuperAdminModel->GetCompliants();
		$data["GetTips"] = $this->SuperAdminModel->GetTips();
		$this->load->view('base', $data);
	}

	public function CheckAppointments()
	{
		$GetId = $_SESSION["LoginId"];
		$data["_contents"] = "check_appointments";
		$data["GetMyProfile"] = $this->SuperAdminModel->GetMyProfile();
		$data["GetCheckupAppointments"] = $this->PatientsModel->GetCheckAppointments($GetId);
		$this->load->view('base', $data);
	}

	public function CheckCheckups()
	{
		$GetId = $_SESSION["LoginId"];
		$data["_contents"] = "check_patient_checkups";
		$data["GetMyProfile"] = $this->SuperAdminModel->GetMyProfile();
		$data["GetCheckCheckups"] = $this->PatientsModel->GetCheckCheckups($GetId);
		$data["GetCompliantsByPatientId"] = $this->PatientsInfo_model->GetCompliantsByPatientId($GetId);
		$data["GetExaminationByPatientId"] = $this->PatientsInfo_model->GetExaminationByPatientId($GetId);
		$data["GetInvestigationByPatientId"] = $this->PatientsInfo_model->GetInvestigationByPatientId($GetId);
		$data["GetTreatmentsByPatientId"] = $this->PatientsInfo_model->GetTreatmentsByPatientId($GetId);
		$this->load->view('base', $data);
	}

	 public function add_patients()
	{
		$Id = "";
		if(isset($_SESSION["GetPatientId"])){
			$Id = $_SESSION["GetPatientId"];
		}else{
			$Id = 0;
		}
		// echo $Id;
		// exit;
		$data["_contents"] = "add_patient";
		$data["GetMyProfile"] = $this->SuperAdminModel->GetMyProfile();
		$data["GetCompliants"] = $this->SuperAdminModel->GetCompliants();
		$data["GetPatientsById"] = $this->PatientsModel->GetPatientsById($Id);
		$data["GetExamination"] = $this->SuperAdminModel->GetExamination();
		$data["GetInvestigation"] = $this->SuperAdminModel->GetInvestigation();
		$data["GetTreatments"] = $this->SuperAdminModel->GetTreatments();
		$data["GetDiagnotics"] = $this->SuperAdminModel->GetDiagnotics();

		$data["GetCompliantsByPatientId"] = $this->PatientsInfo_model->GetCompliantsByPatientId($Id);
		$data["GetExaminationByPatientId"] = $this->PatientsInfo_model->GetExaminationByPatientId($Id);
		$data["GetInvestigationByPatientId"] = $this->PatientsInfo_model->GetInvestigationByPatientId($Id);
		$data["GetTreatmentsByPatientId"] = $this->PatientsInfo_model->GetTreatmentsByPatientId($Id);
		$data["GetDiagnoticsByPatientId"] = $this->PatientsInfo_model->GetDiagnoticsByPatientId($Id);

		$data["GetSurgeryByPatientId"] = $this->PatientsInfo_model->GetSurgeryByPatientId($Id);
		// echo json_encode($data["GetSurgeryByPatientId"]);
		// exit;
		$this->load->view('base', $data);
	}

	public function add_patients_info($id)
	{
		$Id = $this->uri->segment(3);
		$data["_contents"] = "add_patient_info";
		$data["GetMyProfile"] = $this->SuperAdminModel->GetMyProfile();
		$data["GetCompliants"] = $this->SuperAdminModel->GetCompliants();
		$data["GetPatientsById"] = $this->PatientsModel->GetPatientsById($Id);
		$data["GetExamination"] = $this->SuperAdminModel->GetExamination();
		$data["GetInvestigation"] = $this->SuperAdminModel->GetInvestigation();
		$data["GetTreatments"] = $this->SuperAdminModel->GetTreatments();
		$data["GetDiagnotics"] = $this->SuperAdminModel->GetDiagnotics();

		$data["GetCompliantsByPatientId"] = $this->PatientsInfo_model->GetCompliantsByPatientId($Id);
		$data["GetExaminationByPatientId"] = $this->PatientsInfo_model->GetExaminationByPatientId($Id);
		$data["GetInvestigationByPatientId"] = $this->PatientsInfo_model->GetInvestigationByPatientId($Id);
		$data["GetTreatmentsByPatientId"] = $this->PatientsInfo_model->GetTreatmentsByPatientId($Id);
		$data["GetDiagnoticsByPatientId"] = $this->PatientsInfo_model->GetDiagnoticsByPatientId($Id);
		
		$data["GetSurgeryByPatientId"] = $this->PatientsInfo_model->GetSurgeryByPatientId($Id);
		
		$this->load->view('base', $data);
	}

	public function edit_patients($Id)
	{
		$Id = $this->uri->segment(3);
		$data["_contents"] = "edit_patient";
		$data["GetMyProfile"] = $this->SuperAdminModel->GetMyProfile();
		$data["GetCompliants"] = $this->SuperAdminModel->GetCompliants();
		$data["GetPatientsById"] = $this->PatientsModel->GetPatientsById($Id);
		$this->load->view('base', $data);
	}

	public function all_compliants()
	{
		$data["_contents"] = "compliants";
		$data["GetMyProfile"] = $this->SuperAdminModel->GetMyProfile();
        $data["GetCompliants"] = $this->SuperAdminModel->GetCompliants();
		$this->load->view('base', $data);
	}

	

	public function all_patients()
	{
		$data["_contents"] = "all_patients";
		$data["GetMyProfile"] = $this->SuperAdminModel->GetMyProfile();
		$data["GetAdmins"] = $this->AdminModel->GetAdmin();
		
		$this->load->view('base', $data);
	}

	public function insert_patients()
	{
	// 	$investigation = './investigation/' . mt_rand(100000, 999999) . $_FILES['investigation']['name'];
	//    move_uploaded_file($_FILES['investigation']['tmp_name'], $investigation);

	//    $treatment = './treatment/' . mt_rand(100000, 999999) . $_FILES['treatment']['name'];
	//    move_uploaded_file($_FILES['treatment']['tmp_name'], $treatment);
		// echo $JoiningDate;
		// exit;
		$data = array(
				"unique_id"=> "Dr-". rand("9999", 1000),
				"name"=> $this->input->post("name"),
				"password"=> $this->input->post("name"),
				"age"=> $this->input->post("age"),
				"address"=> $this->input->post("address"),
				"gender"=> $this->input->post("gender"),
				"blood_group"=> $this->input->post("blood"),
				"joining_date"=> $this->input->post("dob"),
				"date_of_birth"=> $this->input->post("dob"),
				"mobile"=> $this->input->post("mobile"),
				"whatsapp_no"=> $this->input->post("whatsapp_no"),
				// "compliants_id"=> $this->input->post("compliant_name"),
				"examination"=> $this->input->post("examination"),
				"diagnotics"=> $this->input->post("diagnotics"),
				// "investigation"=> $investigation,
				// "treatment"=> $treatment,
				"type"=> "2",
		);
		$InsertPatients = $this->PatientsModel->add_patients($data);
		$_SESSION["GetPatientId"] = $InsertPatients;
		// echo $_SESSION["GetPatientId"];
		if($InsertPatients){
			// $AppointmentData = array(
			// 	"patient_id" => $InsertPatients,
			// 	"appointment_date" => $this->input->post("appointment"),
			// 	"description" => $this->input->post("description")
			// );
			// $InsertAppointments = $this->PatientsModel->add_appointments($AppointmentData);
			echo json_encode(array(
				"error" => FALSE,
				"message" => "Patients added successfully"
			));
		}else{
			echo json_encode(array(
				"error" => TRUE,
				"message" => "Failed"
			));
		}
		
	}

	public function update_patients()
	{
	// 	$investigation = './investigation/' . mt_rand(100000, 999999) . $_FILES['investigation']['name'];
	//    move_uploaded_file($_FILES['investigation']['tmp_name'], $investigation);

	//    $treatment = './treatment/' . mt_rand(100000, 999999) . $_FILES['treatment']['name'];
	//    move_uploaded_file($_FILES['treatment']['tmp_name'], $treatment);
		$Id = $this->input->post("id");
		$data = array(
				// "unique_id"=> "Dr-". rand("9999", 1000),
				"name"=> $this->input->post("name"),
				"password"=> $this->input->post("name"),
				"age"=> $this->input->post("age"),
				"address"=> $this->input->post("address"),
				"gender"=> $this->input->post("gender"),
				"blood_group"=> $this->input->post("blood"),
				// "joining_date"=> $this->input->post("dob"),
				"date_of_birth"=> $this->input->post("dob"),
				"mobile"=> $this->input->post("mobile"),
				"whatsapp_no"=> $this->input->post("whatsapp_no"),
				"compliants_id"=> $this->input->post("compliant_name"),
				"examination"=> $this->input->post("examination"),
				"diagnotics"=> $this->input->post("diagnotics"),
				// "investigation"=> $investigation,
				// "treatment"=> $treatment,
				// "type"=> "2",
		);
		$UpdatePatients = $this->PatientsModel->update_patients($Id, $data);
		if($UpdatePatients){
			// $AppointmentData = array(
			// 	"patient_id" => $InsertPatients,
			// 	"appointment_date" => $this->input->post("appointment"),
			// 	"description" => $this->input->post("description")
			// );
			// $InsertAppointments = $this->PatientsModel->add_appointments($AppointmentData);
			echo json_encode(array(
				"error" => FALSE,
				"message" => "Patients updated successfully"
			));
		}else{
			echo json_encode(array(
				"error" => TRUE,
				"message" => "Failed"
			));
		}
		
	}



	public function GetCompliantsbyId()
	{
		$Id = $this->input->post("cname");
		// echo $Id;
		// exit;
        $GetCompliantsbyId = $this->SuperAdminModel->GetCompliantsbyId($Id);
		echo json_encode($GetCompliantsbyId);
	}

	public function update_compliants()
	{
		$Id = $this->input->post("id");
		$data = array(
			"compliant_name"=> $this->input->post("name"),
			"details"=> $this->input->post("description")
		);
	$UpdateCompliants = $this->SuperAdminModel->updatecompliants($Id, $data);
	if($UpdateCompliants){
		echo json_encode(array(
			"error" => FALSE,
			"message" => "Compliants updated successfully"
		));
	}else{
		echo json_encode(array(
			"error" => TRUE,
			"message" => "Failed"
		));
	}
		
	}


	public function GetpatientsbyId($Id)
	{
        $GetpatientsbyId = $this->PatientsModel->GetPatientsById($Id);
		echo json_encode($GetpatientsbyId);
	}


	public function book_appointment()
	{
		// echo $this->input->post("description");
		// exit;
		$data = array(
				"patient_id"=> $this->input->post("patient_id"),
                "location_id"=> $this->input->post("location_id"),
                "appointment_date" => $this->input->post("appointment_date")
		);
		$InsertCompliants = $this->SuperAdminModel->add_compliants($data);
		if($InsertCompliants){
			echo json_encode(array(
				"error" => FALSE,
				"message" => "Appointment booked successfully"
			));
		}else{
			echo json_encode(array(
				"error" => TRUE,
				"message" => "Failed"
			));
		}
		
	}

	// public function update_patients_info()
	// {
	// // 	$investigation = './investigation/' . mt_rand(100000, 999999) . $_FILES['investigation']['name'];
	// //    move_uploaded_file($_FILES['investigation']['tmp_name'], $investigation);

	// //    $treatment = './treatment/' . mt_rand(100000, 999999) . $_FILES['treatment']['name'];
	// //    move_uploaded_file($_FILES['treatment']['tmp_name'], $treatment);
	// 	$Id = $this->input->post("id");
	// 	$CompliantsId = $this->input->post("compliant_name");
	// 	$examinationId = $this->input->post("examination");
	// 	$investigationId = $this->input->post("investigation");
	// 	$treatmentId = $this->input->post("treatments");

	// 	$data = array(
	// 			"compliants_id"=> $CompliantsId,
	// 			"examination"=> $examinationId,
	// 			"investigation"=> $investigationId,
	// 			"treatment"=> $treatmentId
	// 	);
	// 	$UpdatePatients = $this->PatientsModel->update_patients($Id, $data);
	// 	if($UpdatePatients){
	// 		$CompliantsData =  array("details" => $this->input->post("description"));
	// 		$UpdateCompliants = $this->SuperAdminModel->updatecompliants($CompliantsId, $CompliantsData);

	// 		$ExaminationData =  array("details" =>$this->input->post("examination_description"));
	// 		$UpdateExaminationData = $this->SuperAdminModel->updatecompliants($examinationId, $ExaminationData);

	// 		$InvestigationData =  array("details" =>$this->input->post("investigation_description"));
	// 		$UpdateInvestigationData = $this->SuperAdminModel->updatecompliants($investigationId, $InvestigationData);

	// 		$TreatmentsData =  array("details" =>$this->input->post("treatments_description"));
	// 		$UpdateTreatmentsData = $this->SuperAdminModel->updatecompliants($treatmentId, $TreatmentsData);
			
	// 		echo json_encode(array(
	// 			"error" => FALSE,
	// 			"message" => "Patients updated successfully"
	// 		));
	// 	}else{
	// 		echo json_encode(array(
	// 			"error" => TRUE,
	// 			"message" => "Failed"
	// 		));
	// 	}
		
	// }

	public function UpdatePatientStatus($id) {

        $UpdatePatientStatus = $this->PatientsInfo_model->UpdatePatients($id);
        // redirect($_SERVER['HTTP_REFERER']);
        if($UpdatePatientStatus){
            echo json_encode(array(
                "error" => FALSE,
                "message" => "Status updated"
            ));
        }else{
            echo json_encode(array(
                "error" => TRUE,
                "message" => "Failed"
            ));
        }
    }
}
