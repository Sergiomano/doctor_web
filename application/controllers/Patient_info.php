<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Patient_info extends CI_Controller {
	public function __construct(){
        parent::__construct();
		$this->load->model('AdminModel');
		$this->load->model('SuperAdminModel');
		$this->load->model('PatientsModel');
		$this->load->model('PatientsInfo_model');
    }
	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */

	// Examination 
	public function all_examination()
	{
		$data["_contents"] = "examination";
		$data["GetMyProfile"] = $this->SuperAdminModel->GetMyProfile();
        $data["GetExamination"] = $this->SuperAdminModel->GetExamination();
		$this->load->view('base', $data);
	}

	public function all_diagnotics()
	{
		$data["_contents"] = "diagnotics";
		$data["GetMyProfile"] = $this->SuperAdminModel->GetMyProfile();
        $data["GetDiagnotics"] = $this->SuperAdminModel->GetDiagnotics();
		$this->load->view('base', $data);
	}

	public function GetExaminationbyId($Id)
	{
        // $Id = $this->input->post("cname");
        // echo $Id;
        // exit;
        $GetCompliantsbyId = $this->SuperAdminModel->GetCompliantsbyId($Id);
		echo json_encode($GetCompliantsbyId);
	}

	public function update_examination()
	{
        $Id = $this->input->post("id");
		$data = array(
			"compliant_name"=> $this->input->post("name"),
			"details"=> $this->input->post("description")
		);
	$UpdateCompliants = $this->SuperAdminModel->updatecompliantsinfo($Id, $data);
	if($UpdateCompliants){
		echo json_encode(array(
			"error" => FALSE,
			"message" => "Examination updated successfully"
		));
	}else{
		echo json_encode(array(
			"error" => TRUE,
			"message" => "Failed"
		));
	}
		
    }
    
    public function add_examination()
	{
		// echo $this->input->post("description");
		// exit;
		$data = array(
				"compliant_name"=> $this->input->post("name"),
                "details"=> $this->input->post("description"),
                "type" => 2
		);
		$InsertCompliants = $this->SuperAdminModel->add_compliants($data);
		if($InsertCompliants){
			echo json_encode(array(
				"error" => FALSE,
				"message" => "Examination added successfully"
			));
		}else{
			echo json_encode(array(
				"error" => TRUE,
				"message" => "Failed"
			));
		}
		
	}



	// Investigation 
	public function all_investigation()
	{
		$data["_contents"] = "investigation";
		$data["GetMyProfile"] = $this->SuperAdminModel->GetMyProfile();
        $data["GetInvestigation"] = $this->SuperAdminModel->GetInvestigation();
		$this->load->view('base', $data);
	}


	public function GetInvestigationbyId($Id)
	{
        // $Id = $this->input->post("cname");
        // echo $Id;
        // exit;
        $GetCompliantsbyId = $this->SuperAdminModel->GetCompliantsbyId($Id);
		echo json_encode($GetCompliantsbyId);
	}

	public function update_investigation()
	{
        $Id = $this->input->post("id");
		$data = array(
			"compliant_name"=> $this->input->post("name"),
			"details"=> $this->input->post("description")
		);
	$UpdateCompliants = $this->SuperAdminModel->updatecompliantsinfo($Id, $data);
	if($UpdateCompliants){
		echo json_encode(array(
			"error" => FALSE,
			"message" => "Investigation updated successfully"
		));
	}else{
		echo json_encode(array(
			"error" => TRUE,
			"message" => "Failed"
		));
	}
		
    }
    
    public function add_investigation()
	{
		// echo $this->input->post("description");
		// exit;
		$data = array(
				"compliant_name"=> $this->input->post("name"),
                "details"=> $this->input->post("description"),
                "type" => 3
		);
		$InsertCompliants = $this->SuperAdminModel->add_compliants($data);
		if($InsertCompliants){
			echo json_encode(array(
				"error" => FALSE,
				"message" => "Investigation added successfully"
			));
		}else{
			echo json_encode(array(
				"error" => TRUE,
				"message" => "Failed"
			));
		}
		
	}




	// Treatments 
	public function all_treatments()
	{
		$data["_contents"] = "treatments";
		$data["GetMyProfile"] = $this->SuperAdminModel->GetMyProfile();
        $data["GetTreatments"] = $this->SuperAdminModel->GetTreatments();
		$this->load->view('base', $data);
	}


	public function GetTreatmentsbyId($Id)
	{
        // $Id = $this->input->post("cname");
        // echo $Id;
        // exit;
        $GetCompliantsbyId = $this->SuperAdminModel->GetTreatmentsbyId($Id);
		echo json_encode($GetCompliantsbyId);
	}

	public function update_treatments()
	{
        $Id = $this->input->post("id");
		$data = array(
			"compliant_name"=> $this->input->post("name"),
			"details"=> $this->input->post("description")
		);
	$UpdateCompliants = $this->SuperAdminModel->updatecompliantsinfo($Id, $data);
	if($UpdateCompliants){
		echo json_encode(array(
			"error" => FALSE,
			"message" => "Treatments updated successfully"
		));
	}else{
		echo json_encode(array(
			"error" => TRUE,
			"message" => "Failed"
		));
	}
		
    }
    
    public function add_treatments()
	{
		// echo $this->input->post("description");
		// exit;
		$PatientId = "";
		if(isset($_SESSION["GetPatientId"])){
			$PatientId = $_SESSION["GetPatientId"];
		}else{
			$PatientId = $this->input->post("patient_id");
		}
		// $Id = $this->input->post("eid");

		$data = array(
				"patient_id"=> $PatientId,
				"drug"=> $this->input->post("drug"),
				"dose"=> $this->input->post("dose"),
				"morning"=> $this->input->post("morning"),
				"afternoon"=> $this->input->post("afternoon"),
				"evening"=> $this->input->post("evening"),
				"night"=> $this->input->post("night"),
				"after_food"=> $this->input->post("after_food"),
				"before_food"=> $this->input->post("before_food"),
				"sos"=> $this->input->post("sos")
		);
		$InsertCompliants = $this->PatientsInfo_model->add_treatments($data);
		if($InsertCompliants){
			echo json_encode(array(
				"error" => FALSE,
				"message" => "Treatments added successfully"
			));
		}else{
			echo json_encode(array(
				"error" => TRUE,
				"message" => "Failed"
			));
		}
	}

	// Patients Info

	public function add_compliants_info()
	{
		$PatientId = "";
		if(isset($_SESSION["GetPatientId"])){
			$PatientId = $_SESSION["GetPatientId"];
		}else{
			$PatientId = $this->input->post("patient_id");
		}
		$Id = $this->input->post("cid");
		
		
		// $data = array(
		// 	"details"=> $this->input->post("description")
		// );
		// $UpdateCompliants = $this->SuperAdminModel->updatecompliants($Id, $data);
		// $CheckPatientsAndInfo = $this->PatientsInfo_model->CheckPatientsAndInfo($PatientId, $Id);
		// echo json_encode($CheckPatientsAndInfo);
		// exit;
		// if(!$CheckPatientsAndInfo){
			$data = array(
				"patient_id"=> $PatientId,
				"patient_info_id"=> $Id,
				"description"=> $this->input->post("description")
			);
			// $InsertCompliants = $this->SuperAdminModel->add_compliants($data);
			$InsertCompliants = $this->PatientsInfo_model->add_patients_info($data);
			echo json_encode(array(
				"error" => FALSE,
				"message" => "Compliants updated successfully"
			));
		// }else{
			
		// 	echo json_encode(array(
		// 		"error" => TRUE,
		// 		"message" => "Complaints already added in this patient"
		// 	));
		// }
	}


	public function add_examination_info()
	{
		$PatientId = "";
		if(isset($_SESSION["GetPatientId"])){
			$PatientId = $_SESSION["GetPatientId"];
		}else{
			$PatientId = $this->input->post("patient_id");
		}
		$Id = $this->input->post("eid");
		
		// $data = array(
		// 	"details"=> $this->input->post("examination_description")
		// );
		// $UpdateCompliants = $this->SuperAdminModel->updatecompliants($Id, $data);
		// $CheckPatientsAndInfo = $this->PatientsInfo_model->CheckPatientsAndInfo($PatientId, $Id);
		// // echo json_encode($CheckPatientsAndInfo);
		// // exit;
		// if(!$CheckPatientsAndInfo){
			$data = array(
				"patient_id"=> $PatientId,
				"patient_info_id"=> $Id,
				"description"=> $this->input->post("examination_description")
			);
			// $InsertCompliants = $this->SuperAdminModel->add_compliants($data);
			$InsertCompliants = $this->PatientsInfo_model->add_patients_info($data);
			echo json_encode(array(
				"error" => FALSE,
				"message" => "Examination updated successfully"
			));
		// }else{
			
		// 	echo json_encode(array(
		// 		"error" => TRUE,
		// 		"message" => "Examination already added in this patient"
		// 	));
		// }
		
	}


	public function add_investigation_info()
	{
		$Id = $this->input->post("Iid");
		$PatientId = "";
		if(isset($_SESSION["GetPatientId"])){
			$PatientId = $_SESSION["GetPatientId"];
		}else{
			$PatientId = $this->input->post("patient_id");
		}
		
		$data = array(
			"patient_id"=> $PatientId,
			"patient_info_id"=> $Id,
			"description"=> $this->input->post("investigation_description")
		);
		// $InsertCompliants = $this->SuperAdminModel->add_compliants($data);
		$InsertCompliants = $this->PatientsInfo_model->add_patients_info($data);
		echo json_encode(array(
			"error" => FALSE,
			"message" => "Investigation updated successfully"
		));

		// $data = array(
		// 	"details"=> $this->input->post("investigation_description")
		// );
		// $UpdateCompliants = $this->SuperAdminModel->updatecompliants($Id, $data);
		// $CheckPatientsAndInfo = $this->PatientsInfo_model->CheckPatientsAndInfo($PatientId, $Id);
		// // echo json_encode($CheckPatientsAndInfo);
		// // exit;
		// if(!$CheckPatientsAndInfo){
		// 	$data = array(
		// 		"patient_id"=> $PatientId,
		// 		"patient_info_id"=> $Id
		// 	);
		// 	$InsertCompliants = $this->PatientsInfo_model->add_patients_info($data);
		// 	echo json_encode(array(
		// 		"error" => FALSE,
		// 		"message" => "Investigation updated successfully"
		// 	));
		// }else{
			
		// 	echo json_encode(array(
		// 		"error" => TRUE,
		// 		"message" => "Investigation already added in this patient"
		// 	));
		// }
		
	}

	public function add_treatments_info()
	{
		$Id = $this->input->post("Tid");
		$PatientId = "";
		if(isset($_SESSION["GetPatientId"])){
			$PatientId = $_SESSION["GetPatientId"];
		}else{
			$PatientId = $this->input->post("patient_id");
		}
		// $PatientId = $this->input->post("patient_id");
		$data = array(
			"patient_id"=> $PatientId,
			"patient_info_id"=> $Id,
			"description"=> $this->input->post("treatments_description")
		);
		// $InsertCompliants = $this->SuperAdminModel->add_compliants($data);
		$InsertCompliants = $this->PatientsInfo_model->add_patients_info($data);
		echo json_encode(array(
			"error" => FALSE,
			"message" => "Treatments updated successfully"
		));


		// $data = array(
		// 	"details"=> $this->input->post("treatments_description")
		// );
		// $UpdateCompliants = $this->SuperAdminModel->updatecompliants($Id, $data);
		// $CheckPatientsAndInfo = $this->PatientsInfo_model->CheckPatientsAndInfo($PatientId, $Id);
		// // echo json_encode($CheckPatientsAndInfo);
		// // exit;
		// if(!$CheckPatientsAndInfo){
		// 	$data = array(
		// 		"patient_id"=> $PatientId,
		// 		"patient_info_id"=> $Id
		// 	);
		// 	$InsertCompliants = $this->PatientsInfo_model->add_patients_info($data);
		// 	echo json_encode(array(
		// 		"error" => FALSE,
		// 		"message" => "Treatments updated successfully"
		// 	));
		// }else{
			
		// 	echo json_encode(array(
		// 		"error" => TRUE,
		// 		"message" => "Treatments already added in this patient"
		// 	));
		// }
		
	}

	public function add_surgery()
	{	
		// $Id = $this->input->post("Tid");
		$PatientId = "";
		if(isset($_SESSION["GetPatientId"])){
			$PatientId = $_SESSION["GetPatientId"];
		}else{
			$PatientId = $this->input->post("patient_id");
		}


		$SurgeryId = $this->input->post("surgery");
		// echo $SurgeryId;P
		// exit;
		// $PatientId = $this->input->post("patient_id");
		$data = array(
			"patient_id"=> $PatientId,
			"surgery_name"=> $SurgeryId,

			"date"=> $this->input->post("get_date"),
			"details"=> $this->input->post("surgery_description")
		);
		// $InsertCompliants = $this->SuperAdminModel->add_compliants($data);
		$InsertSurgery = $this->PatientsInfo_model->add_surgery($data);
		echo json_encode(array(
			"error" => FALSE,
			"message" => "Surgery updated successfully"
		));



		// $data = array(
		// 	"details"=> $this->input->post("surgery_description")
		// );
		// $UpdateCompliants = $this->PatientsInfo_model->update_surgery($SurgeryId, $PatientId , $data);


		// $CheckPatientsSurgery = $this->PatientsInfo_model->CheckPatientsSurgery($PatientId, $SurgeryId);
		// if(!$CheckPatientsSurgery){
		// 	$data = array(
		// 			"patient_id"=> $PatientId,
		// 			"surgery_name"=> $SurgeryId,
		// 			"details"=> $this->input->post("surgery_description")
		// 	);
		// 	$InsertSurgery = $this->PatientsInfo_model->add_surgery($data);
		// 	if($InsertSurgery){
		// 		echo json_encode(array(
		// 			"error" => FALSE,
		// 			"message" => "Surgery added successfully"
		// 		));
		// 	}else{
		// 		echo json_encode(array(
		// 			"error" => TRUE,
		// 			"message" => "Failed"
		// 		));
		// 	}
		// }else{
				
		// 	echo json_encode(array(
		// 		"error" => TRUE,
		// 		"message" => "Surgery already added in this patient"
		// 	));
		// }
		
	}



	public function GetComplaintName()
    {
		$query = $this->input->get('query');
        $this->db->where('type', 2);
        $this->db->like('name', $query);


        $data = $this->db->get("patients")->result();


		echo json_encode( $data);
		
        // $query = $this->input->get('query');
        // // $this->db->where('type', 1);
        // // $this->db->like('compliant_name', $query);
		// // $data = $this->db->get("compliants")->result();
		// $data = $this->PatientsInfo_model->SearchNyComplaintName($query);
        // echo json_encode( $data);
	}
	


	// Multiple images
	public function investigationimages(){
		$PatientId = "";
		if(isset($_SESSION["GetPatientId"])){
			$PatientId = $_SESSION["GetPatientId"];
		}else{
			$PatientId = $this->input->post("patient_id");
		}

		$files = $_FILES;
		$count = count($_FILES['userfile']['name']);
		for($i=0; $i<$count; $i++)
		{
			$_FILES['userfile']['name']= time().$files['userfile']['name'][$i];
			$_FILES['userfile']['type']= $files['userfile']['type'][$i];
			$_FILES['userfile']['tmp_name']= $files['userfile']['tmp_name'][$i];
			$_FILES['userfile']['error']= $files['userfile']['error'][$i];
			$_FILES['userfile']['size']= $files['userfile']['size'][$i];
			$config['upload_path'] = './investigation/';
			$config['allowed_types'] = 'gif|jpg|png|jpeg';
			$config['max_size'] = '2000000';
			$config['remove_spaces'] = true;
			$config['overwrite'] = false;
			$config['max_width'] = '';
			$config['max_height'] = '';
			$this->load->library('upload', $config);
			$this->upload->initialize($config);
			$this->upload->do_upload();
			$fileName = $_FILES['userfile']['name'];
			$images[] = $fileName;
		}
		$fileName = implode(',',$images);
		// $gallery_data = array(
		// 	'category_id' => $this->input->post('category_id'),
		// 	'images' => $fileName
		// );
		$this->SuperAdminModel->add_investigation($fileName,$PatientId);
		redirect($_SERVER['HTTP_REFERER']);
	}


	public function diagnoticsimages(){
		$PatientId = "";
		if(isset($_SESSION["GetPatientId"])){
			$PatientId = $_SESSION["GetPatientId"];
		}else{
			$PatientId = $this->input->post("patient_id");
		}

		$files = $_FILES;
		$count = count($_FILES['userfile']['name']);
		for($i=0; $i<$count; $i++)
		{
			$_FILES['userfile']['name']= time().$files['userfile']['name'][$i];
			$_FILES['userfile']['type']= $files['userfile']['type'][$i];
			$_FILES['userfile']['tmp_name']= $files['userfile']['tmp_name'][$i];
			$_FILES['userfile']['error']= $files['userfile']['error'][$i];
			$_FILES['userfile']['size']= $files['userfile']['size'][$i];
			$config['upload_path'] = './diagnotics/';
			$config['allowed_types'] = 'gif|jpg|png|jpeg';
			$config['max_size'] = '2000000';
			$config['remove_spaces'] = true;
			$config['overwrite'] = false;
			$config['max_width'] = '';
			$config['max_height'] = '';
			$this->load->library('upload', $config);
			$this->upload->initialize($config);
			$this->upload->do_upload();
			$fileName = $_FILES['userfile']['name'];
			$images[] = $fileName;
		}
		$fileName = implode(',',$images);
		// $gallery_data = array(
		// 	'category_id' => $this->input->post('category_id'),
		// 	'images' => $fileName
		// );
		$this->SuperAdminModel->add_diagnotics($fileName,$PatientId);
		redirect($_SERVER['HTTP_REFERER']);
	}

	public function treatmentsimages(){
		$PatientId = "";
		if(isset($_SESSION["GetPatientId"])){
			$PatientId = $_SESSION["GetPatientId"];
		}else{
			$PatientId = $this->input->post("patient_id");
		}

		$files = $_FILES;
		$count = count($_FILES['userfile']['name']);
		for($i=0; $i<$count; $i++)
		{
			$_FILES['userfile']['name']= time().$files['userfile']['name'][$i];
			$_FILES['userfile']['type']= $files['userfile']['type'][$i];
			$_FILES['userfile']['tmp_name']= $files['userfile']['tmp_name'][$i];
			$_FILES['userfile']['error']= $files['userfile']['error'][$i];
			$_FILES['userfile']['size']= $files['userfile']['size'][$i];
			$config['upload_path'] = './treatments/';
			$config['allowed_types'] = 'gif|jpg|png|jpeg';
			$config['max_size'] = '2000000';
			$config['remove_spaces'] = true;
			$config['overwrite'] = false;
			$config['max_width'] = '';
			$config['max_height'] = '';
			$this->load->library('upload', $config);
			$this->upload->initialize($config);
			$this->upload->do_upload();
			$fileName = $_FILES['userfile']['name'];
			$images[] = $fileName;
		}
		$fileName = implode(',',$images);
		// $gallery_data = array(
		// 	'category_id' => $this->input->post('category_id'),
		// 	'images' => $fileName
		// );
		$this->SuperAdminModel->add_treatments($fileName,$PatientId);
		redirect($_SERVER['HTTP_REFERER']);
	}

	public function surgeryimages(){
		$PatientId = "";
		if(isset($_SESSION["GetPatientId"])){
			$PatientId = $_SESSION["GetPatientId"];
		}else{
			$PatientId = $this->input->post("patient_id");
		}

		$files = $_FILES;
		$count = count($_FILES['userfile']['name']);
		for($i=0; $i<$count; $i++)
		{
			$_FILES['userfile']['name']= time().$files['userfile']['name'][$i];
			$_FILES['userfile']['type']= $files['userfile']['type'][$i];
			$_FILES['userfile']['tmp_name']= $files['userfile']['tmp_name'][$i];
			$_FILES['userfile']['error']= $files['userfile']['error'][$i];
			$_FILES['userfile']['size']= $files['userfile']['size'][$i];
			$config['upload_path'] = './surgery/';
			$config['allowed_types'] = 'gif|jpg|png|jpeg';
			$config['max_size'] = '2000000';
			$config['remove_spaces'] = true;
			$config['overwrite'] = false;
			$config['max_width'] = '';
			$config['max_height'] = '';
			$this->load->library('upload', $config);
			$this->upload->initialize($config);
			$this->upload->do_upload();
			$fileName = $_FILES['userfile']['name'];
			$images[] = $fileName;
		}
		$fileName = implode(',',$images);
		// $gallery_data = array(
		// 	'category_id' => $this->input->post('category_id'),
		// 	'images' => $fileName
		// );
		$this->SuperAdminModel->add_surgery($fileName,$PatientId);
		redirect($_SERVER['HTTP_REFERER']);
	}

	public function UploadVideo(){
		$PatientId = "";
		if(isset($_SESSION["GetPatientId"])){
			$PatientId = $_SESSION["GetPatientId"];
		}else{
			$PatientId = $this->input->post("patient_id");
		}
		$surgery = './surgery_video/' . mt_rand(100000, 999999) . $_FILES['surgery_video']['name'];
	   move_uploaded_file($_FILES['surgery_video']['tmp_name'], $surgery);
		$data = array(
				'patient_id'=> $PatientId,
				'images' => $surgery
		);
		
	   $this->SuperAdminModel->add_surgery_video($data);
	   redirect($_SERVER['HTTP_REFERER']);
	}
}
