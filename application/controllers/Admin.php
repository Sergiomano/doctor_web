<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Admin extends CI_Controller {
    public function __construct(){
        parent::__construct();
		$this->load->model('AdminModel');
		$this->load->model('SuperAdminModel');
    }
	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */
	public function index()
	{
		$data["_contents"] = "admin_dashboard";
		$data["GetMyProfile"] = $this->SuperAdminModel->GetMyProfile();
        $data["GetAdmins"] = $this->AdminModel->GetAdmin();
		$this->load->view('base', $data);
	}

	 public function all_admin()
	{
		$data["_contents"] = "all_admin";
		$data["GetMyProfile"] = $this->SuperAdminModel->GetMyProfile();
        $data["GetAdmins"] = $this->AdminModel->GetAdmin();
		$this->load->view('base', $data);
	}

	public function CheckLogin(){
		$Name = $this->input->post("name");
		$Password = $this->input->post("password");

		$GetType = "";
		$GetId = "";
		$GetDataByLogin = $this->AdminModel->GetDataByName($Name, $Password);
		if($GetDataByLogin){
			foreach($GetDataByLogin as $Login){
				$GetType = $Login["type"];
				$GetId = $Login["id"];
			}
			$_SESSION["LoginType"] = $GetType;
			$_SESSION["LoginId"] = $GetId;
			if($GetType == 1){
				echo json_encode(array(
					"error" => FALSE,
					"message" => "Login successfully",
					"redirect" => "admin"
				));
			}elseif($GetType == 2){
				echo json_encode(array(
					"error" => FALSE,
					"message" => "Login successfully",
					"redirect" => "Patients"
				));
			}
			
			// echo json_encode($GetType);
		}elseif($Name == "Admin" && $Password == "Admin"){
			$_SESSION["LoginType"] = "0";
			echo json_encode(array(
				"error" => FALSE,
				"message" => "Login successfully",
				"redirect" => "super_admin"
			));
		}else{
			echo json_encode(array(
				"error" => TRUE,
				"message" => "Username or password wrong..."
			));
		}
		
	}

	public function add_admin()
	{
		$JoiningDate = $this->input->post("joining_date");
		// echo $JoiningDate;
		// exit;
		$data = array(
				"unique_id" => "Dr-". rand("9999", 1000),
				"name"=> $this->input->post("name"),
				"joining_date"=> $JoiningDate,
				"password" => $this->input->post("name"),
				"date_of_birth"=> $this->input->post("dob"),
				"age"=> $this->input->post("age"),
				"mobile"=> $this->input->post("mobile"),
				"address"=> $this->input->post("address"),
				"type"=> "1"
		);
		$InsertAdmins = $this->AdminModel->add_admin($data);
		if($InsertAdmins){
			echo json_encode(array(
				"error" => FALSE,
				"message" => "Admin added successfully"
			));
		}else{
			echo json_encode(array(
				"error" => TRUE,
				"message" => "Failed"
			));
		}
		
	}

	public function GetAdminbyId($Id)
	{
		// $Id = $this->input->post("id");
        $GetAdminById = $this->AdminModel->GetAdminById($Id);
		echo json_encode($GetAdminById);
	}

	
	public function update_admin()
	{
		$Id = $this->input->post("id");
		$JoiningDate = $this->input->post("joining_date");
		// echo $JoiningDate;
		// exit;
		$data = array(
				"name"=> $this->input->post("name"),
				"joining_date"=> $JoiningDate,
				"password" => $this->input->post("name"),
				"date_of_birth"=> $this->input->post("dob"),
				"age"=> $this->input->post("age"),
				"mobile"=> $this->input->post("mobile"),
				"address"=> $this->input->post("address")
		);
		$UpdateAdmins = $this->AdminModel->updateadmin($Id, $data);
		if($UpdateAdmins){
			echo json_encode(array(
				"error" => FALSE,
				"message" => "Admin updated successfully"
			));
		}else{
			echo json_encode(array(
				"error" => TRUE,
				"message" => "Failed"
			));
		}
		
	}

	public function Delete_Admins($id){
		$Delete_Admins = $this->AdminModel->Delete_Admins($id);
		if($Delete_Admins){
			echo json_encode(array(
				"error" => FALSE,
				"message" => "Admin deleted successfully"
			));
		}else{
			echo json_encode(array(
				"error" => TRUE,
				"message" => "Failed"
			));
		}
	}
}
