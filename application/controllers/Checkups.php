<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Checkups extends CI_Controller {
	public function __construct(){
        parent::__construct();
		$this->load->model('AdminModel');
        $this->load->model('SuperAdminModel');
        $this->load->model('CheckupsModel');
    }
	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */

    public function index()
	{
        $data["_contents"] = "check_checkups";
        $data["GetCheckups"] = $this->CheckupsModel->GetCheckups();
        $data["GetMyProfile"] = $this->SuperAdminModel->GetMyProfile();
		$this->load->view('base', $data);
    }

	public function all_checkups()
	{
		$data["_contents"] = "checkups";
		$data["GetMyProfile"] = $this->SuperAdminModel->GetMyProfile();
        $data["GetCheckups"] = $this->CheckupsModel->GetCheckups();
		$this->load->view('base', $data);
	}

	public function all_patients()
	{
		$data["_contents"] = "all_patients";
		$data["GetMyProfile"] = $this->SuperAdminModel->GetMyProfile();
        $data["GetAdmins"] = $this->AdminModel->GetAdmin();
		$this->load->view('base', $data);
	}

	public function add_checkups()
	{
		// echo $JoiningDate;
		// exit;
		$data = array(
				"checkup_name"=> $this->input->post("name"),
				"description"=> $this->input->post("description")
		);
		$InsertCheckups = $this->CheckupsModel->add_checkups($data);
		if($InsertCheckups){
			echo json_encode(array(
				"error" => FALSE,
				"message" => "Checkups added successfully"
			));
		}else{
			echo json_encode(array(
				"error" => TRUE,
				"message" => "Failed"
			));
		}
		
	}

	public function GetCheckupsbyId($Id)
	{
		// $Id = $this->input->post("id");
        $GetCheckupsbyId = $this->CheckupsModel->GetCheckupsbyId($Id);
		echo json_encode($GetCheckupsbyId);
    }
    
    public function GetCheckupDetailsbyId()
	{
        $Id = $this->input->post("name");
        $GetCheckupsbyId = $this->CheckupsModel->GetCheckupsbyId($Id);
		echo json_encode($GetCheckupsbyId);
	}

	public function update_checkups()
	{
		$Id = $this->input->post("id");
		$data = array(
			"checkup_name"=> $this->input->post("name"),
			"description"=> $this->input->post("description")
		);
	$UpdateCheckups = $this->CheckupsModel->updatecheckups($Id, $data);
	if($UpdateCheckups){
		echo json_encode(array(
			"error" => FALSE,
			"message" => "Checkups updated successfully"
		));
	}else{
		echo json_encode(array(
			"error" => TRUE,
			"message" => "Failed"
		));
	}
		
	}


	public function AddPatientCheckups(){
        $CheckupsData = array(
            "patient_id" => $this->input->post("id"),
            "checkup_id" => $this->input->post("checkup_id"),
            "checkup_date" => $this->input->post("checkup_date")
            // "description" => $this->input->post("description")
        );
        $InsertCheckups = $this->CheckupsModel->add_patient_checkups($CheckupsData);
        if($InsertCheckups){
            echo json_encode(array(
                "error" => FALSE,
                "message" => "Checkups successfully"
            ));
        }else{
            echo json_encode(array(
                "error" => TRUE,
                "message" => "Failed"
            ));
        }
       
    }
}
