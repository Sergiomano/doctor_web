<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Payments extends CI_Controller {
	public function __construct(){
        parent::__construct();
		$this->load->model('AdminModel');
		$this->load->model('SuperAdminModel');
    }
	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */
	public function index()
	{
		$data["_contents"] = "payments";
		$data["GetMyProfile"] = $this->SuperAdminModel->GetMyProfile();
        $data["GetPayments"] = $this->SuperAdminModel->GetPayments();
		$this->load->view('base', $data);
	}

	public function Profit_details()
	{
		$data["_contents"] = "profit_details";
		$data["GetMyProfile"] = $this->SuperAdminModel->GetMyProfile();
		$data["GetPayments"] = $this->SuperAdminModel->GetPayments();
		$data["GetLocation"] = $this->SuperAdminModel->GetPayments();
		$data["GetProfitDetails"] = $this->SuperAdminModel->GetProfitDetails();
		
		$this->load->view('base', $data);
	}

	public function GetProfitDetailsBylocation($Id){
		$GetProfitDetailsBylocation = $this->SuperAdminModel->GetProfitDetailsBylocation($Id);
		echo json_encode($GetProfitDetailsBylocation);
	}

	public function add_payments()
	{
		// echo $JoiningDate;
		// exit;
		$data = array(
				"location"=> $this->input->post("location"),
				"amount"=> $this->input->post("amount")
		);
		$InsertPayments = $this->SuperAdminModel->add_payments($data);
		if($InsertPayments){
			echo json_encode(array(
				"error" => FALSE,
				"message" => "Payments added successfully"
			));
		}else{
			echo json_encode(array(
				"error" => TRUE,
				"message" => "Failed"
			));
		}
		
	}

	public function GetPaymentsbyId($Id)
	{
		// $Id = $this->input->post("id");
        $GetPaymentsbyId = $this->SuperAdminModel->GetPaymentsbyId($Id);
		echo json_encode($GetPaymentsbyId);
	}

	public function update_payments()
	{
		$Id = $this->input->post("id");
		$data = array(
			"location"=> $this->input->post("location"),
			"amount"=> $this->input->post("amount")
		);
	$UpdatePayments = $this->SuperAdminModel->updatepayments($Id, $data);
	if($UpdatePayments){
		echo json_encode(array(
			"error" => FALSE,
			"message" => "Payments updated successfully"
		));
	}else{
		echo json_encode(array(
			"error" => TRUE,
			"message" => "Failed"
		));
	}
		
	}

	public function Delete_Payments($id){
		$Delete_Payments = $this->SuperAdminModel->Delete_Payments($id);
		if($Delete_Payments){
			echo json_encode(array(
				"error" => FALSE,
				"message" => "Payments deleted successfully"
			));
		}else{
			echo json_encode(array(
				"error" => TRUE,
				"message" => "Failed"
			));
		}
	}
}
