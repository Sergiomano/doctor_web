HOME_URL = "/";

$(document).on("click", ".AddCompliants", function(){
    var name = $("#name").val();
    var description = CKEDITOR.instances['description'].getData();

    var CompliantsData = {
        name: name,
        description: description
    };

    $.ajax({
        type: "POST",
        url: HOME_URL+"Super_admin/add_compliants",
        data: CompliantsData,
        dataType: "JSON",
        success: function (data) {
            if(data.error){
                alert(data.message);
                return false;
            }else{
                alert(data.message);
                
                // window.location.href=data.redirect;
                return false;
            }
        }
    });
});



function EditCompliants(id){
    $.ajax({
        type: "POST",
        url: HOME_URL+"Super_admin/GetPatientsInfobyId/"+id,
        data: {id: id},
        dataType: "JSON",
        success: function (data) {
            if(data.error){
                alert(data.message);
                return false;
            }else{
                $.each(data, function (i, admin) { 
                    $("#id").val(admin.id);
                    $("#Editname").val(admin.compliant_name);
                    CKEDITOR.instances['Editdescription'].setData(admin.description);
                    // $("#Editdescription").val(admin.details);
                });
                $("#EditCompliants").modal("show");
            }
        }
    });
}

$(document).on("click", "#UpdateCompliants", function(){
    var id = $("#id").val();
    var name = $("#Editname").val();
    var description = CKEDITOR.instances['Editdescription'].getData();

    var CompliantsData = {
        id: id,
        name: name,
        description: description
    };

    // alert(address);
    // return false;
    $.ajax({
        type: "POST",
        url: HOME_URL+"Super_admin/update_compliants",
        data: CompliantsData,
        dataType: "JSON",
        success: function (data) {
            if(data.error){
                alert(data.message);
                return false;
            }else{
                alert(data.message);
                return false;
            }
        }
    });
});


$(document).on("click", ".AddDiagnotics", function(){
    var name = $("#name").val();
    var description = CKEDITOR.instances['description'].getData();

    var CompliantsData = {
        name: name,
        description: description
    };

    $.ajax({
        type: "POST",
        url: HOME_URL+"Super_admin/add_diagnotics",
        data: CompliantsData,
        dataType: "JSON",
        success: function (data) {
            if(data.error){
                alert(data.message);
                return false;
            }else{
                alert(data.message);
                
                // window.location.href=data.redirect;
                return false;
            }
        }
    });
});


function EditCompliants(id){
    $.ajax({
        type: "POST",
        url: HOME_URL+"Super_admin/GetCompliantsbyId/"+id,
        data: {id: id},
        dataType: "JSON",
        success: function (data) {
            if(data.error){
                alert(data.message);
                return false;
            }else{
                $.each(data, function (i, admin) { 
                    $("#id").val(admin.id);
                    $("#Editname").val(admin.compliant_name);
                    CKEDITOR.instances['Editdescription'].setData(admin.details);
                    // $("#Editdescription").val(admin.details);
                });
                $("#EditCompliants").modal("show");
            }
        }
    });
}

function EditCompliantsInfo(id){
    $.ajax({
        type: "POST",
        url: HOME_URL+"Super_admin/GetPatientsInfobyId/"+id,
        data: {id: id},
        dataType: "JSON",
        success: function (data) {
            if(data.error){
                alert(data.message);
                return false;
            }else{
                $.each(data, function (i, admin) { 
                    $("#id").val(admin.id);
                    $("#Editname").val(admin.compliant_name);
                    CKEDITOR.instances['Editdescription'].setData(admin.description);
                    // $("#Editdescription").val(admin.details);
                });
                $("#EditCompliants").modal("show");
            }
        }
    });
}

$(document).on("click", "#UpdateCompliantsInfo", function(){
    var id = $("#id").val();
    var name = $("#Editname").val();
    var description = CKEDITOR.instances['Editdescription'].getData();

    var CompliantsData = {
        id: id,
        name: name,
        description: description
    };

    // alert(address);
    // return false;
    $.ajax({
        type: "POST",
        url: HOME_URL+"Super_admin/update_compliantsInfo",
        data: CompliantsData,
        dataType: "JSON",
        success: function (data) {
            if(data.error){
                alert(data.message);
                return false;
            }else{
                alert(data.message);
                return false;
            }
        }
    });
});




$(document).on("click", ".UpdateProfile", function(){
    var id = $("#id").val();
    var name = $("#name").val();
    var designation = $("#designation").val();
    var email = $("#email").val();
    var phone = $("#phone").val();
    var address = $("#address").val();
    var website = $("#website").val();

    var ProfilesData = {
        id: id,
        name: name,
        designation: designation,
        email: email,
        phone: phone,
        address: address,
        website: website
    };

    // alert(address);
    // return false;
    $.ajax({
        type: "POST",
        url: HOME_URL+"Super_admin/update_profile",
        data: ProfilesData,
        dataType: "JSON",
        success: function (data) {
            if(data.error){
                alert(data.message);
                return false;
            }else{
                alert(data.message);
                return false;
            }
        }
    });
});



function CheckpatientName(){
    var name = $("#name").val();
    // var name = $("#id").text();

    $.ajax({
        type: "POST",
        url: HOME_URL+"Appointments/GetPatientsById",
        data: {name: name},
        dataType: "JSON",
        success: function (data) {
            if(data.error){
                console.log(data);
                return false;
            }else{
                $.each(data, function (i, admin) { 
                    $("#id").text(admin.id);
                    $("#unique_id").text(admin.unique_id);
                     $("#patient_name").text(admin.name);
                     $("#age").text(admin.age);
                     $("#address").text(admin.address);
                     $("#gender").text(admin.gender);
                     $("#blood").text(admin.blood_group);
                     $("#mobile").text(admin.mobile);
                });
                return false;
            }
        }
    });
}




$(document).on("click", ".BookAppointments", function(){
    var patient_id = $("#Patientid").val();
    var location_id = $("#location").val();
    var appointment_date = $("#appointment_date").val();

    if(appointment_date == ""){
        alert("Please choose appointment date...");
        $("#appointment").focus();
        return false;
    }

    var AppointmentsData = {
        patient_id: patient_id,
        location_id: location_id,
        appointment_date: appointment_date
    };

    // alert(address);
    // return false;
    $.ajax({
        type: "POST",
        url: HOME_URL+"Appointments/book_appointment",
        data: AppointmentsData,
        dataType: "JSON",
        success: function (data) {
            if(data.error){
                alert(data.message);
                return false;
            }else{
                alert(data.message);
                return false;
            }
        }
    });
});



// Tips
$(document).on("click", ".AddTips", function(){
    var name = $("#tipsname").val();
    var description = CKEDITOR.instances['description'].getData();

    // console.log(description);
    // return false;
    var CompliantsData = {
        name: name,
        description: description
    };

    $.ajax({
        type: "POST",
        url: HOME_URL+"Super_admin/add_tips",
        data: CompliantsData,
        dataType: "JSON",
        success: function (data) {
            if(data.error){
                alert(data.message);
                return false;
            }else{
                alert(data.message);
                // window.location.href=data.redirect;
                return false;
            }
        }
    });
});



function EditTips(id){
    $.ajax({
        type: "POST",
        url: HOME_URL+"Super_admin/GetTipsbyId/"+id,
        data: {id: id},
        dataType: "JSON",
        success: function (data) {
            if(data.error){
                alert(data.message);
                return false;
            }else{
                $.each(data, function (i, admin) { 
                    $("#id").val(admin.id);
                    $("#Editname").val(admin.name);
                    // $("#Editdescription").val(admin.details);
                    CKEDITOR.instances['Editdescription'].setData(admin.details)
                });
                $("#EditTips").modal("show");
            }
        }
    });
}

$(document).on("click", "#UpdateTips", function(){
    var id = $("#id").val();
    var name = $("#Editname").val();
    var description = CKEDITOR.instances['Editdescription'].getData();

    var CompliantsData = {
        id: id,
        name: name,
        description: description
    };

    // alert(address);
    // return false;
    $.ajax({
        type: "POST",
        url: HOME_URL+"Super_admin/update_tips",
        data: CompliantsData,
        dataType: "JSON",
        success: function (data) {
            if(data.error){
                alert(data.message);
                return false;
            }else{
                alert(data.message);
                return false;
            }
        }
    });
});



function DeleteComplaints(id){
    swal({
        title: "Are you sure?",
        text: "You will not be able to recover this imaginary file!",
        type: "warning",
        showCancelButton: true,
        confirmButtonColor: "#DD6B55",
        confirmButtonText: "Yes, delete it!",
        closeOnConfirm: false
    }, function () {
        $.ajax({
            type: "POST",
            url: HOME_URL+"Super_admin/Delete_Complaints/"+id,
            data: {id: id},
            dataType: "JSON",
            success: function (data) {
                if(data.error){
                    swal("Error", data.message, "error");
                    return false;
                }else{
					swal("Success", data.message);
					location.reload();
                    return false;
                }
            }
        });
    });
}


function Delete_ComplaintsInfo(id){
    swal({
        title: "Are you sure?",
        text: "You will not be able to recover this imaginary file!",
        type: "warning",
        showCancelButton: true,
        confirmButtonColor: "#DD6B55",
        confirmButtonText: "Yes, delete it!",
        closeOnConfirm: false
    }, function () {
        $.ajax({
            type: "POST",
            url: HOME_URL+"Super_admin/Delete_ComplaintsInfo/"+id,
            data: {id: id},
            dataType: "JSON",
            success: function (data) {
                if(data.error){
                    swal("Error", data.message, "error");
                    return false;
                }else{
					swal("Success", data.message);
					location.reload();
                    return false;
                }
            }
        });
    });
}


function Delete_TreatmentsInfo(id){
    swal({
        title: "Are you sure?",
        text: "You will not be able to recover this imaginary file!",
        type: "warning",
        showCancelButton: true,
        confirmButtonColor: "#DD6B55",
        confirmButtonText: "Yes, delete it!",
        closeOnConfirm: false
    }, function () {
        $.ajax({
            type: "POST",
            url: HOME_URL+"Super_admin/Delete_TreatmentsInfo/"+id,
            data: {id: id},
            dataType: "JSON",
            success: function (data) {
                if(data.error){
                    swal("Error", data.message, "error");
                    return false;
                }else{
                    swal("Success", data.message);
					location.reload();
                    return false;
                }
            }
        });
    });
}

function Delete_Surgery(id){
    swal({
        title: "Are you sure?",
        text: "You will not be able to recover this imaginary file!",
        type: "warning",
        showCancelButton: true,
        confirmButtonColor: "#DD6B55",
        confirmButtonText: "Yes, delete it!",
        closeOnConfirm: false
    }, function () {
        $.ajax({
            type: "POST",
            url: HOME_URL+"Super_admin/Delete_Surgery/"+id,
            data: {id: id},
            dataType: "JSON",
            success: function (data) {
                if(data.error){
                    swal("Error", data.message, "error");
                    return false;
                }else{
                    swal("Success", data.message);
					location.reload();
                    return false;
                }
            }
        });
    });
}

function DeleteTips(id){
    swal({
        title: "Are you sure?",
        text: "You will not be able to recover this imaginary file!",
        type: "warning",
        showCancelButton: true,
        confirmButtonColor: "#DD6B55",
        confirmButtonText: "Yes, delete it!",
        closeOnConfirm: false
    }, function () {
        $.ajax({
            type: "POST",
            url: HOME_URL+"Super_admin/Delete_Tips/"+id,
            data: {id: id},
            dataType: "JSON",
            success: function (data) {
                if(data.error){
                    swal("Error", data.message, "error");
                    return false;
                }else{
                    swal("Success", data.message);
					location.reload();
                    return false;
                }
            }
        });
    });
}
