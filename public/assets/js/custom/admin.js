HOME_URL = "/";

$(document).on("click", ".AddAdmin", function(){
    var name = $("#name").val();
    var joining_date = $("#joining_date").val();
    var dob = $("#dob").val();
    var age = $("#age").val();
    var mobile = $("#mobile").val();
    var address = $("#address").val();

    var AdminData = {
        name: name,
        joining_date: joining_date,
        dob: dob,
        age: age,
        mobile: mobile,
        address: address
    };

    $.ajax({
        type: "POST",
        url: HOME_URL+"Admin/add_admin",
        data: AdminData,
        dataType: "JSON",
        success: function (data) {
            if(data.error){
                alert(data.message);
                return false;
            }else{
                alert(data.message);
                // window.location.href=data.redirect;
                return false;
            }
        }
    });
});



function EditAdmin(id){
    $.ajax({
        type: "POST",
        url: HOME_URL+"Admin/GetAdminbyId/"+id,
        data: {id: id},
        dataType: "JSON",
        success: function (data) {
            if(data.error){
                alert(data.message);
                return false;
            }else{
                $.each(data, function (i, admin) { 
                    $("#id").val(admin.id);
                     $("#Editname").val(admin.name);
                     $("#Editjoining_date").val(admin.joining_date);
                     $("#Editdob").val(admin.date_of_birth);
                     $("#Editage").val(admin.age);
                     $("#Editmobile").val(admin.mobile);
                     $("#Editaddress").val(admin.address);
                });
                $("#EditAdmin").modal("show");
            }
        }
    });
}

$(document).on("click", "#UpdateAdmin", function(){
    var id = $("#id").val();
    var name = $("#Editname").val();
    var joining_date = $("#Editjoining_date").val();
    var dob = $("#Editdob").val();
    var age = $("#Editage").val();
    var mobile = $("#Editmobile").val();
    var address = $("#Editaddress").val();

    var AdminUpdateData = {
        id: id,
        name: name,
        joining_date: joining_date,
        dob: dob,
        age: age,
        mobile: mobile,
        address: address
    };

    // alert(address);
    // return false;
    $.ajax({
        type: "POST",
        url: HOME_URL+"Admin/update_admin",
        data: AdminUpdateData,
        dataType: "JSON",
        success: function (data) {
            if(data.error){
                alert(data.message);
                return false;
            }else{
                alert(data.message);
                return false;
            }
        }
    });
});


function DeleteAdmins(id){
    swal({
        title: "Are you sure?",
        text: "You will not be able to recover this imaginary file!",
        type: "warning",
        showCancelButton: true,
        confirmButtonColor: "#DD6B55",
        confirmButtonText: "Yes, delete it!",
        closeOnConfirm: false
    }, function () {
        $.ajax({
            type: "POST",
            url: HOME_URL+"Admin/Delete_Admins/"+id,
            data: {id: id},
            dataType: "JSON",
            success: function (data) {
                if(data.error){
                    swal("Error", data.message, "error");
                    return false;
                }else{
                    swal("Success", data.message);
                    return false;
                }
            }
        });
    });
}