HOME_URL = "/";

$(document).on("click", ".AddPayments", function(){
    var location = $("#location").val();
    var amount = $("#amount").val();

    var PaymentsData = {
        location: location,
        amount: amount
    };

    $.ajax({
        type: "POST",
        url: HOME_URL+"Payments/add_payments",
        data: PaymentsData,
        dataType: "JSON",
        success: function (data) {
            if(data.error){
                alert(data.message);
                return false;
            }else{
                alert(data.message);
                // window.location.href=data.redirect;
                return false;
            }
        }
    });
});


function EditPayments(id){
    $.ajax({
        type: "POST",
        url: HOME_URL+"Payments/GetPaymentsbyId/"+id,
        data: {id: id},
        dataType: "JSON",
        success: function (data) {
            if(data.error){
                alert(data.message);
                return false;
            }else{
                $.each(data, function (i, admin) { 
                    $("#id").val(admin.id);
                    $("#Editlocation").val(admin.location);
                    // $("#Editdescription").val(admin.details);
                    $("#Editamount").val(admin.amount);
                });
                $("#EditCompliants").modal("show");
            }
        }
    });
}

$(document).on("click", ".UpdatePayments", function(){
    var id = $("#id").val();
    var location = $("#Editlocation").val();
    var amount = $("#Editamount").val();

    var PaymentsData = {
        id: id,
        location: location,
        amount: amount
    };

    // alert(address);
    // return false;
    $.ajax({
        type: "POST",
        url: HOME_URL+"Payments/update_payments",
        data: PaymentsData,
        dataType: "JSON",
        success: function (data) {
            if(data.error){
                alert(data.message);
                return false;
            }else{
                alert(data.message);
                return false;
            }
        }
    });
});


function BookAppointment(id){
    $.ajax({
        type: "POST",
        url: HOME_URL+"Patients/GetpatientsbyId/"+id,
        data: {id: id},
        dataType: "JSON",
        success: function (data) {
            if(data.error){
                alert(data.message);
                return false;
            }else{
                $.each(data, function (i, admin) { 
                    $("#Patientid").val(admin.id);
                    // $("#Editlocation").val(admin.location);
                    // // $("#Editdescription").val(admin.details);
                    // $("#Editamount").val(admin.amount);
                });
                $("#BookAppointment").modal("show");
            }
        }
    });
}


function GetProfitDetailsBylocation(id){
    var id = $("#location").val();
    
    $.ajax({
        type: "POST",
        url: HOME_URL+"Payments/GetProfitDetailsBylocation/"+id,
        data: {id: id},
        dataType: "JSON",
        success: function (data) {
            if(data.error){
                alert(data.message);
                return false;
            }else{
                $("#ProfitDetails").text("");
                $.each(data, function (i, admin) { 
                    var ProfitData = "<tr><td>"+ admin.LocationName +"</td><td>"+ admin.patientscount +"</td><td>"+ admin.Amount +"</td></tr>";
    
                    $("#ProfitDetails").append(ProfitData);
                });
            }
        }
    });
}




function DeletePayments(id){
    swal({
        title: "Are you sure?",
        text: "You will not be able to recover this imaginary file!",
        type: "warning",
        showCancelButton: true,
        confirmButtonColor: "#DD6B55",
        confirmButtonText: "Yes, delete it!",
        closeOnConfirm: false
    }, function () {
        $.ajax({
            type: "POST",
            url: HOME_URL+"Payments/Delete_Payments/"+id,
            data: {id: id},
            dataType: "JSON",
            success: function (data) {
                if(data.error){
                    swal("Error", data.message, "error");
                    return false;
                }else{
                    swal("Success", data.message);
                    return false;
                }
            }
        });
    });
}

