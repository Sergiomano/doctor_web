HOME_URL = "/";

function CheckCompliants(){
    var cname = $("#compliant_name").val();

    $.ajax({
        type: "POST",
        url: HOME_URL+"Patients/GetCompliantsbyId/",
        data: {cname: cname},
        dataType: "JSON",
        success: function (data) {
            if(data.error){
                alert(data.message);
                return false;
            }else{
                $("#Description").text("");
                $.each(data, function (i, admin) { 
                    $("#Description").text(admin.details);
                });
                return false;
            }
        }
    });
}


$('input[type="checkbox"]').click(function(){
    var mobile = $("#mobile").val();
    if(mobile == ""){
        alert("Mobile number should not empty");
        $("#mobile").focus();
        return false;
    }

    if($(this).is(":checked")){
        $("#whatsapp_no").val(mobile);
        $("#whatsapp_no").prop("disabled", true);
    }
    else if($(this).is(":not(:checked)")){
        $("#whatsapp_no").val("");
        $("#whatsapp_no").prop("disabled", false);
    }
});


$(document).on("click", ".AddPatients", function(){
    // var fname = $("#fname").val();
    // var lname = $("#lname").val();
    var name = $("#name").val();

    var mobile = $("#mobile").val();
    var whatsapp_no = $("#whatsapp_no").val();
    var age = $("#age").val();
    var gender = $("#gender").val();
    var blood = $("#blood").val();
    var dob = $("#dob").val();
    var appointment = $("#appointment").val();
    var compliant_name = $("#compliant_name").val();
    var examination = $("#examination").val();
    var diagnotics = $("#diagnotics").val();
    // var investigation = $("#investigation").val();
    // var treatment = $("#treatment").val();
    var address = $("#address").val();
    var description = $("#description").val();

    var PatientData = new FormData();

    PatientData.append("name", name);
    PatientData.append("mobile", mobile);
    PatientData.append("whatsapp_no", whatsapp_no);
    PatientData.append("age", age);
    PatientData.append("gender", gender);
    PatientData.append("blood", blood);
    PatientData.append("dob", dob);
    PatientData.append("appointment", appointment);
    PatientData.append("compliant_name", compliant_name);
    PatientData.append("examination", examination);
    PatientData.append("diagnotics", diagnotics);
    // PatientData.append('investigation', $('#investigation')[0].files[0]);
    // PatientData.append('treatment', $('#treatment')[0].files[0]);
    PatientData.append("address", address);
    PatientData.append("description", description);


    $.ajax({
        type: "POST",
        url: HOME_URL+"Patients/insert_patients",
        data: PatientData,
        dataType: "JSON",
        processData: false,
        contentType: false,
        success: function (data) {
            if(data.error){
                alert(data.message);
                return false;
            }else{
                alert(data.message);
                return false;
            }
        }
    });

});


$(document).on("click", ".UpdatePatients", function(){
    var id = $("#id").val();
    // var lname = $("#lname").val();
    // var name = fname + lname;

    var mobile = $("#mobile").val();
    var whatsapp_no = $("#whatsapp_no").val();
    var age = $("#age").val();
    var gender = $("#gender").val();
    var blood = $("#blood").val();
    var dob = $("#dob").val();
    var appointment = $("#appointment").val();
    var compliant_name = $("#compliant_name").val();
    var examination = $("#examination").val();
    var diagnotics = $("#diagnotics").val();
    // var investigation = $("#investigation").val();
    // var treatment = $("#treatment").val();
    var address = $("#address").val();
    // var description = $("#description").val();

    var PatientData = new FormData();

    PatientData.append("id", id);
    PatientData.append("name", name);
    PatientData.append("mobile", mobile);
    PatientData.append("whatsapp_no", whatsapp_no);
    PatientData.append("age", age);
    PatientData.append("gender", gender);
    PatientData.append("blood", blood);
    PatientData.append("dob", dob);
    PatientData.append("appointment", appointment);
    PatientData.append("compliant_name", compliant_name);
    PatientData.append("examination", examination);
    PatientData.append("diagnotics", diagnotics);
    // PatientData.append('investigation', $('#investigation')[0].files[0]);
    // PatientData.append('treatment', $('#treatment')[0].files[0]);
    PatientData.append("address", address);
    // PatientData.append("description", description);


    $.ajax({
        type: "POST",
        url: HOME_URL+"Patients/update_patients",
        data: PatientData,
        dataType: "JSON",
        processData: false,
        contentType: false,
        success: function (data) {
            if(data.error){
                alert(data.message);
                return false;
            }else{
                alert(data.message);
                return false;
            }
        }
    });

});



function CheckComplaints(){
    var cname = $("#compliants").val();
    
    $.ajax({
        type: "POST",
        url: HOME_URL+"Patients/GetCompliantsbyId/",
        data: {cname: cname},
        dataType: "JSON",
        success: function (data) {
            if(data.error){
                alert(data.message);
                return false;
            }else{
                $("#description").text("");
                $.each(data, function (i, admin) { 
                    // var content = $("#Getdescription").text(admin.details);
                    CKEDITOR.instances['description'].setData(admin.details)
                    // $("#Getdescription").text(admin.details);
                });
                return false;
            }
        }
    });
}


function CheckDiagnotics(){
    var cname = $("#daignotics").val();
    
    $.ajax({
        type: "POST",
        url: HOME_URL+"Patients/GetCompliantsbyId/",
        data: {cname: cname},
        dataType: "JSON",
        success: function (data) {
            if(data.error){
                alert(data.message);
                return false;
            }else{
                $("#daignotics_description").text("");
                $.each(data, function (i, admin) { 
                    // var content = $("#Getdescription").text(admin.details);
                    CKEDITOR.instances['daignotics_description'].setData(admin.details)
                    // $("#Getdescription").text(admin.details);
                });
                return false;
            }
        }
    });
}


$(document).on("click", ".UpdatePatientsInfo", function(){
    var id = $("#id").val();
    // var lname = $("#lname").val();
    // var name = fname + lname;

    var compliant_name = $("#compliants").val();
    var examination = $("#examination").val();
    var investigation = $("#investigation").val();
    var treatments = $("#treatments").val();

    var description = CKEDITOR.instances['description'].getData();
    var examination_description = CKEDITOR.instances['examination_description'].getData();
    var investigation_description = CKEDITOR.instances['investigation_description'].getData();
    var treatments_description = CKEDITOR.instances['treatments_description'].getData();

    var PatientData = new FormData();

    PatientData.append("id", id);
    
    PatientData.append("compliant_name", compliant_name);
    PatientData.append("examination", examination);
    PatientData.append("investigation", investigation);
    PatientData.append("treatments", treatments);

    PatientData.append("description", description);
    PatientData.append("examination_description", examination_description);
    PatientData.append("investigation_description", investigation_description);
    PatientData.append("treatments_description", treatments_description);


    $.ajax({
        type: "POST",
        url: HOME_URL+"Patients/update_patients_info",
        data: PatientData,
        dataType: "JSON",
        processData: false,
        contentType: false,
        success: function (data) {
            if(data.error){
                alert(data.message);
                return false;
            }else{
                alert(data.message);
                return false;
            }
        }
    });

});


function CheckAppointments(id){
    $.ajax({
        type: "POST",
        url: HOME_URL+"Appointments/CheckAppointmentsByPatientId/"+id,
        data: {id: id},
        dataType: "JSON",
        success: function (data) {
            if(data.error){
                alert(data.message);
                return false;
            }else{
                $("#GetAppointments").text("");
                $.each(data, function (i, admin) { 
                    var AppointmentsData = "<tr><td>"+ admin.name +"</td><td>"+ admin.location +"</td><td>"+ admin.appointment_date +"</td></tr>";

                    $("#GetAppointments").append(AppointmentsData);
                });
                $("#CheckAppointments").modal("show");
            }
        }
    });
    
}

















