HOME_URL = "/";

$(document).on("click", ".AddCheckups", function(){
    var name = $("#name").val();
    var description = $("#description").val();

    var CheckupsData = {
        name: name,
        description: description
    };

    $.ajax({
        type: "POST",
        url: HOME_URL+"Checkups/add_checkups",
        data: CheckupsData,
        dataType: "JSON",
        success: function (data) {
            if(data.error){
                alert(data.message);
                return false;
            }else{
                alert(data.message);
                // window.location.href=data.redirect;
                return false;
            }
        }
    });
});



function EditCheckups(id){
    $.ajax({
        type: "POST",
        url: HOME_URL+"Checkups/GetCheckupsbyId/"+id,
        data: {id: id},
        dataType: "JSON",
        success: function (data) {
            if(data.error){
                alert(data.message);
                return false;
            }else{
                $.each(data, function (i, admin) { 
                    $("#id").val(admin.id);
                    $("#Editname").val(admin.checkup_name);
                    $("#Editdescription").val(admin.description);
                });
                $("#EditCompliants").modal("show");
            }
        }
    });
}

$(document).on("click", "#UpdateCheckups", function(){
    var id = $("#id").val();
    var name = $("#Editname").val();
    var description = $("#Editdescription").val();

    var CompliantsData = {
        id: id,
        name: name,
        description: description
    };

    // alert(address);
    // return false;
    $.ajax({
        type: "POST",
        url: HOME_URL+"Checkups/update_checkups",
        data: CompliantsData,
        dataType: "JSON",
        success: function (data) {
            if(data.error){
                alert(data.message);
                return false;
            }else{
                alert(data.message);
                return false;
            }
        }
    });
});



function CheckCheckups(){
    var name = $("#checkup_name").val();
    
    $.ajax({
        type: "POST",
        url: HOME_URL+"Checkups/GetCheckupDetailsbyId/",
        data: {name: name},
        dataType: "JSON",
        success: function (data) {
            if(data.error){
                alert(data.message);
                return false;
            }else{
                $("#ViewDescription").text("");
                $.each(data, function (i, admin) { 
                    $("#ViewDescription").text(admin.description);
                });
                return false;
            }
        }
    });
}


$(document).on("click", ".AddPatientCheckups", function(){
    var id = $("#id").text();
    var checkup_date = $("#checkup").val();
    var checkup_id = $("#checkup_name").val();

    if(checkup == ""){
        alert("Please choose checkup date...");
        $("#appointment").focus();
        return false;
    }

    var CheckupsData = {
        id: id,
        checkup_date: checkup_date,
        checkup_id: checkup_id
    };

    // alert(address);
    // return false;
    $.ajax({
        type: "POST",
        url: HOME_URL+"Checkups/AddPatientCheckups",
        data: CheckupsData,
        dataType: "JSON",
        success: function (data) {
            if(data.error){
                alert(data.message);
                return false;
            }else{
                alert(data.message);
                return false;
            }
        }
    });
});