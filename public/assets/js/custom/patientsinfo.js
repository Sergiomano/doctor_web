HOME_URL = "/";

$(document).on("click", ".AddExamination", function(){
    var name = $("#name").val();
    var description = CKEDITOR.instances['description'].getData();

    // console.log(description);
    // return false;
    var CompliantsData = {
        name: name,
        description: description
    };

    $.ajax({
        type: "POST",
        url: HOME_URL+"Patient_info/add_examination",
        data: CompliantsData,
        dataType: "JSON",
        success: function (data) {
            if(data.error){
                alert(data.message);
location.reload();
                return false;
            }else{
				alert(data.message);
location.reload();
                // window.location.href=data.redirect;
                return false;
            }
        }
    });
});



function EditExamination(id){
    $.ajax({
        type: "POST",
        url: HOME_URL+"Super_admin/GetPatientsInfobyId/"+id,
        data: {id: id},
        dataType: "JSON",
        success: function (data) {
            if(data.error){
                alert(data.message);
location.reload();
                return false;
            }else{
                $.each(data, function (i, admin) { 
                    $("#eid").val(admin.id);
                    $("#Editexaminationname").val(admin.compliant_name);
                    // $("#Editdescription").val(admin.details);
                    CKEDITOR.instances['Editexaminationdescription'].setData(admin.description);
                    // $("#Editexaminationdescription").val(admin.details);
                });
                $("#EditExamination").modal("show");
            }
        }
    });
}

$(document).on("click", "#UpdateExamination", function(){
    var id = $("#eid").val();
    var name = $("#Editexaminationname").val();
    var description = CKEDITOR.instances['Editexaminationdescription'].getData();

    var CompliantsData = {
        id: id,
        name: name,
        description: description
    };

    // alert(address);
    // return false;
    $.ajax({
        type: "POST",
        url: HOME_URL+"Super_admin/update_compliantsInfo",
        data: CompliantsData,
        dataType: "JSON",
        success: function (data) {
            if(data.error){
                alert(data.message);
location.reload();
                return false;
            }else{
                alert(data.message);
location.reload();
                return false;
            }
        }
    });
});





// Investigation

$(document).on("click", ".AddInvestigation", function(){
    var name = $("#name").val();
    var description = CKEDITOR.instances['description'].getData();

    // console.log(description);
    // return false;
    var CompliantsData = {
        name: name,
        description: description
    };

    $.ajax({
        type: "POST",
        url: HOME_URL+"Patient_info/add_investigation",
        data: CompliantsData,
        dataType: "JSON",
        success: function (data) {
            if(data.error){
                alert(data.message);
location.reload();
                return false;
            }else{
                alert(data.message);
location.reload();
                // window.location.href=data.redirect;
                return false;
            }
        }
    });
});



function EditInvestigation(id){
    $.ajax({
        type: "POST",
        url: HOME_URL+"Super_admin/GetPatientsInfobyId/"+id,
        data: {id: id},
        dataType: "JSON",
        success: function (data) {
            if(data.error){
                alert(data.message);
location.reload();
                return false;
            }else{
                $.each(data, function (i, admin) { 
                    $("#id").val(admin.id);
                    $("#Editname").val(admin.compliant_name);
                    // $("#Editdescription").val(admin.details);
                    CKEDITOR.instances['Editdescription'].setData(admin.description)
                });
                $("#EditCompliants").modal("show");
            }
        }
    });
}

function EditSurgery(id){
    $.ajax({
        type: "POST",
        url: HOME_URL+"Super_admin/GetSurgerybyId/"+id,
        data: {id: id},
        dataType: "JSON",
        success: function (data) {
            if(data.error){
                alert(data.message);
location.reload();
                return false;
            }else{
                $.each(data, function (i, admin) { 
                    $("#Sid").val(admin.id);
                    $("#EditSurgeryname").val(admin.surgery_name);
                    // $("#Editdescription").val(admin.details);
                    CKEDITOR.instances['EditSurgerydescription'].setData(admin.details)
                });
                $("#EditSurgeryModal").modal("show");
            }
        }
    });
}

$(document).on("click", "#UpdateInvestigation", function(){
    var id = $("#id").val();
    var name = $("#Editname").val();
    var description = CKEDITOR.instances['Editdescription'].getData();

    var CompliantsData = {
        id: id,
        name: name,
        description: description
    };

    // alert(address);
    // return false;
    $.ajax({
        type: "POST",
        url: HOME_URL+"Super_admin/update_compliantsInfo",
        data: CompliantsData,
        dataType: "JSON",
        success: function (data) {
            if(data.error){
                alert(data.message);
location.reload();
                return false;
            }else{
                alert(data.message);
location.reload();
                return false;
            }
        }
    });
});




// Treatments

$(document).on("click", "#AddTreatments", function(){
	var drug = $("#drug").val();
	var patient_id = $("#pid").val();
	var dose = $("#dose").val();
	var morning = $("#morning").val();
	var afternoon = $("#afternoon").val();
	var evening = $("#evening").val();
	var night = $("#night").val();
	var after_food = $("#after_food").val();
	var before_food = $(".before_food").val();
	var sos = $("#sos").val();

    // console.log(description);
    // return false;
    var CompliantsData = {
		patient_id:patient_id,
        drug: drug,
		dose: dose,
		morning: morning,
		afternoon: afternoon,
		evening: evening,
		night: night,
		after_food: after_food,
		before_food: before_food,
		sos: sos
    };

    $.ajax({
        type: "POST",
        url: HOME_URL+"Patient_info/add_treatments",
        data: CompliantsData,
        dataType: "JSON",
        success: function (data) {
            if(data.error){
                alert(data.message);
location.reload();
                return false;
            }else{
				alert(data.message);
// location.reload();
				$("#TreatmentDetails").load(location.href + " #TreatmentDetails");
                // window.location.href=data.redirect;
                return false;
            }
        }
    });
});



function EditTreatments(id){
    $.ajax({
        type: "POST",
        url: HOME_URL+"Patient_info/GetTreatmentsbyId/"+id,
        data: {id: id},
        dataType: "JSON",
        success: function (data) {
            if(data.error){
                alert(data.message);
location.reload();
                return false;
            }else{

                $.each(data, function (i, admin) { 
                    $("#Tid").val(admin.id);
					$("#Edit_drug").val(admin.drug);
					$("#Edit_dose").val(admin.dose);
					$("#Edit_morning").val(admin.morning);
					$("#Edit_afternoon").val(admin.afternoon);
					$("#Edit_evening").val(admin.evening);
					$("#Edit_night").val(admin.night);
					$("#Edit_sos").val(admin.sos);
					$("#Edit_af_food").val(admin.after_food);
					$("#Edit_bf_food").val(admin.before_food);
                    // $("#Editdescription").val(admin.details);
                });
                $("#EditTreatments").modal("show");
            }
        }
    });
}

$(document).on("click", "#UpdateTreatments", function(){
    var id = $("#Tid").val();
    var drug = $("#Edit_drug").val();
	var dose = $("#Edit_dose").val();
	var morning = $("#Edit_morning").val();
	var afternoon = $("#Edit_afternoon").val();
	var evening = $("#Edit_evening").val();
	var night = $("#Edit_night").val();
	var after_food = $("#Edit_after_food").val();
	var before_food = $("#Edit_before_food").val();
	var sos = $("#Edit_sos").val();

    var CompliantsData = {
        id: id,
        drug: drug,
		dose: dose,
		morning: morning,
		afternoon: afternoon,
		evening: evening,
		night: night,
		after_food: after_food,
		before_food: before_food,
		sos: sos
    };

    // alert(address);
    // return false;
    $.ajax({
        type: "POST",
        url: HOME_URL+"Super_admin/update_TreatmentsInfo",
        data: CompliantsData,
        dataType: "JSON",
        success: function (data) {
            if(data.error){
                alert(data.message);
location.reload();
                return false;
            }else{
                alert(data.message);
location.reload();
                return false;
            }
        }
    });
});


$(document).on("click", "#UpdateSurgery", function(){
    var id = $("#Sid").val();
    var name = $("#EditSurgeryname").val();
    var description = CKEDITOR.instances['EditSurgerydescription'].getData();

    var CompliantsData = {
        id: id,
        name: name,
        description: description
    };

    // alert(address);
    // return false;
    $.ajax({
        type: "POST",
        url: HOME_URL+"Super_admin/update_surgery",
        data: CompliantsData,
        dataType: "JSON",
        success: function (data) {
            if(data.error){
                alert(data.message);
location.reload();
                return false;
            }else{
                alert(data.message);
location.reload();
                return false;
            }
        }
    });
});



function CheckExamination(){
    var cname = $("#examination").val();
    
    $.ajax({
        type: "POST",
        url: HOME_URL+"Patients/GetCompliantsbyId/",
        data: {cname: cname},
        dataType: "JSON",
        success: function (data) {
            if(data.error){
                alert(data.message);
location.reload();
                return false;
            }else{
                $("#examination_description").text("");
                $.each(data, function (i, admin) { 
                    // var content = $("#Getdescription").text(admin.details);
                    CKEDITOR.instances['examination_description'].setData(admin.details)
                    // $("#Getdescription").text(admin.details);
                });
                return false;
            }
        }
    });
}

function CheckInvestigation(){
    var cname = $("#investigation").val();
    
    $.ajax({
        type: "POST",
        url: HOME_URL+"Patients/GetCompliantsbyId/",
        data: {cname: cname},
        dataType: "JSON",
        success: function (data) {
            if(data.error){
                alert(data.message);
location.reload();
                return false;
            }else{
                $("#investigation_description").text("");
                $.each(data, function (i, admin) { 
                    // var content = $("#Getdescription").text(admin.details);
                    CKEDITOR.instances['investigation_description'].setData(admin.details)
                    // $("#Getdescription").text(admin.details);
                });
                return false;
            }
        }
    });
}

function CheckTreatments(){
    var cname = $("#treatments").val();
    
    $.ajax({
        type: "POST",
        url: HOME_URL+"Patients/GetCompliantsbyId/",
        data: {cname: cname},
        dataType: "JSON",
        success: function (data) {
            if(data.error){
                alert(data.message);
location.reload();
                return false;
            }else{
                $("#treatments_description").text("");
                $.each(data, function (i, admin) { 
                    // var content = $("#Getdescription").text(admin.details);
                    CKEDITOR.instances['treatments_description'].setData(admin.details)
                    // $("#Getdescription").text(admin.details);
                });
                return false;
            }
        }
    });
}





// Patients Info
$(document).on("click", ".AddCompliantsInfo", function(){
    var cid = $("#compliants").val();
    var patient_id = $("#pid").val();
    var description = CKEDITOR.instances['description'].getData();


    var CompliantsData = {
        cid: cid,
        description: description,
        patient_id: patient_id
    };

    // alert(address);
    // return false;
    $.ajax({
        type: "POST",
        url: HOME_URL+"Patient_info/add_compliants_info",
        data: CompliantsData,
        dataType: "JSON",
        success: function (data) {
            if(data.error){
                alert(data.message);
location.reload();
                return false;
            }else{
                alert(data.message);
// location.reload();
                $("#ComplaintsDetails").load(location.href + " #ComplaintsDetails");
                return false;
            }
        }
    });
});



$(document).on("click", ".AddDaignoticsInfo", function(){
    var cid = $("#daignotics").val();
    var patient_id = $("#pid").val();
    var description = CKEDITOR.instances['daignotics_description'].getData();


    var CompliantsData = {
        cid: cid,
        description: description,
        patient_id: patient_id
    };

    // alert(address);
    // return false;
    $.ajax({
        type: "POST",
        url: HOME_URL+"Patient_info/add_compliants_info",
        data: CompliantsData,
        dataType: "JSON",
        success: function (data) {
            if(data.error){
                alert(data.message);
location.reload();
                return false;
            }else{
                alert(data.message);
// location.reload();
                $("#DaignoticsDetails").load(location.href + " #DaignoticsDetails");
                return false;
            }
        }
    });
});



$(document).on("click", ".AddExaminationInfo", function(){
    var eid = $("#examination").val();
    var patient_id = $("#pid").val();
    var examination_description = CKEDITOR.instances['examination_description'].getData();
    // var examination_description = $("#examination_description").val();


    var CompliantsData = {
        eid: eid,
        examination_description: examination_description,
        patient_id: patient_id
    };

    // alert(address);
    // return false;
    $.ajax({
        type: "POST",
        url: HOME_URL+"Patient_info/add_examination_info",
        data: CompliantsData,
        dataType: "JSON",
        success: function (data) {
            if(data.error){
                alert(data.message);
location.reload();
                return false;
            }else{
                alert(data.message);
// location.reload();
                $("#ExaminationDetails").load(location.href + " #ExaminationDetails");
                return false;
            }
        }
    });
});



$(document).on("click", ".AddInvestigationInfo", function(){
    var Iid = $("#investigation").val();
    var patient_id = $("#pid").val();
    var investigation_description = CKEDITOR.instances['investigation_description'].getData();


    var CompliantsData = {
        Iid: Iid,
        investigation_description: investigation_description,
        patient_id: patient_id
    };

    // alert(address);
    // return false;
    $.ajax({
        type: "POST",
        url: HOME_URL+"Patient_info/add_investigation_info",
        data: CompliantsData,
        dataType: "JSON",
        success: function (data) {
            if(data.error){
                alert(data.message);
location.reload();
                return false;
            }else{
                alert(data.message);
// location.reload();
                $("#InvestigationDetails").load(location.href + " #InvestigationDetails");
                return false;
            }
        }
    });
});


$(document).on("click", ".AddTreatmentsInfo", function(){
    var Tid = $("#treatments").val();
    var patient_id = $("#pid").val();
    var treatments_description = CKEDITOR.instances['treatments_description'].getData();


    var CompliantsData = {
        Tid: Tid,
        treatments_description: treatments_description,
        patient_id: patient_id
    };

    // alert(address);
    // return false;
    $.ajax({
        type: "POST",
        url: HOME_URL+"Patient_info/add_treatments_info",
        data: CompliantsData,
        dataType: "JSON",
        success: function (data) {
            if(data.error){
                alert(data.message);
location.reload();
                return false;
            }else{
                alert(data.message);
// location.reload();
                $("#TreatmentDetails").load(location.href + " #TreatmentDetails");
                return false;
            }
        }
    });
});


// Surgery

$(document).on("click", ".AddSurgery", function(){
    var patient_id = $("#pid").val();
	var surgery = $("#surgery").val();
	var get_date = $("#get_date").val();
	var surgery_description = $("#surgery_description").val();

    // var surgery_description = CKEDITOR.instances['surgery_description'].getData();

    // console.log(description);
    // return false;
    var SurgeryData = {
        patient_id: patient_id,
		surgery: surgery,
		get_date: get_date,
        surgery_description: surgery_description
    };

    $.ajax({
        type: "POST",
        url: HOME_URL+"Patient_info/add_surgery",
        data: SurgeryData,
        dataType: "JSON",
        success: function (data) {
            if(data.error){
                alert(data.message);
location.reload();
                return false;
            }else{
                alert(data.message);
                // window.location.href=data.redirect;
                $("#SurgeryDetails").load(location.href + " #SurgeryDetails");
                return false;
            }
        }
    });
});











// $(document).on("change", ".ChangeStatus", function(){
function ChangeStatus(id){

    $.ajax({
        type: "POST",
        url: "/Patients/UpdatePatientStatus/"+id,
        // data: {id: id},
        dataType: "JSON",
        success: function (data) {
            if(data.error){
                alert(data.message);
                return false;
            }else{
                // $("#JobsCount").text('0');
                alert(data.message);
                return false;
            }
        }
	});
}
